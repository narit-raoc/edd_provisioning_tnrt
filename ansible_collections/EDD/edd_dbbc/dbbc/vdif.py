"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

The dbbc..vdif module provides utilities for the VDIF format
- packing and unpacking of VDIF data
- calculations for determining VDIF packet sizes
- Classes for representing VDIF headers and payloads
- A class for Reading VDIF data from a stream (e.g. io.BufferedReader)
"""
import io
import struct
import json
import datetime
import numpy as np

from mpikat.core import logger as logging

_LOG = logging.getLogger("mpikat.dbbc..vdif")

HEADER_SIZE = 32 # Size if a VDIF header

vlbi_bands = [1e6 * 2**i for i in range(0,10)]

def frame_size(sample_rate: float) -> tuple:
    """Calculate vdif frame properties. The algorithm is extraced from Jan Wagner's octave script

    Args:
        sample_rate (float): The sample rate to evaluate the frame properties for (in Hz)

    Returns:
        tuple(list, list, list): A tuple containing three lists for payload size, fps and frame
            time. The last items in the lists refer to the largest possible frame size.
    """
    if sample_rate not in vlbi_bands:
        _LOG.error("Samplerate %f is not VLBI compatible")
    bandwidth = sample_rate / 2e6
    rate_msps = bandwidth * 2
    rate_mbps = 2 * rate_msps # % 2-bit
    _LOG.debug('Bandwidth {:.3f} MHz --> {:.3f} Msps --> {:.3f} Mbit/sec'\
        .format(bandwidth, rate_msps, rate_mbps))

    granularity = 8 # % VDIF specs, granularity of payload size

    num = np.arange(1024, 9001, granularity) * 8*1000 # bits per frame, various payload sizes
    den = rate_mbps # datarate bits per sec
    fitting_payloads = num[num % den == 0]/(8*1000) # corresponding frame payloads in byte

    bytes_per_sec = rate_mbps*1e6/8 #;
    payload_sizes = fitting_payloads[bytes_per_sec % fitting_payloads == 0]
    fps = bytes_per_sec / payload_sizes
    frame_ns = payload_sizes*4*1e3 / rate_msps
    _LOG.debug('Possible frame payload sizes (add 32 for framesize):')
    for k in range(payload_sizes.size):
        _LOG.debug(' {:5.0f} byte  {:8.0f} frames per sec  {:6.3f} nsec/frame'\
            .format(payload_sizes[k], fps[k], frame_ns[k]))
    return payload_sizes, fps, frame_ns

def payload_size(sample_rate: float) -> int:
    """The largest possible payload size for the given sample rate

    Args:
        sample_rate (float): The sample rate to evaluate the payload size for (in Hz)

    Returns:
        int: The payload size
    """
    return int(frame_size(sample_rate)[0][-1])

def frames_per_second(sample_rate: float, nchannel: int=1) -> int:
    """The frames per second for the largest possible payload size

    Args:
        sample_rate (float): The sample rate to evaluate the fps for (in Hz)
        nchannel (int, optional): The number of channels in the frame. Defaults to 1.

    Returns:
        int: The frames per second
    """
    return int(frame_size(sample_rate)[1][-1] * nchannel)


def volume(sample_rate: int, duration: int, nchannel: int=1) -> int:
    """Computes the the data volume in bytes for a given duration

    Args:
        sample_rate (int): Sample rate
        duration (int): Duration in seconds
        nchannel (int, optional): Number of channels. Defaults to 1.
    """
    return int(duration
               * frames_per_second(sample_rate, nchannel)
               * (payload_size(sample_rate) + HEADER_SIZE))


def frame_time_ns(sample_rate: float) -> int:
    """The duration of a single frame in nano seconds

    Args:
        sample_rate (float): The sample rate to evaluate the frame duration for (in Hz)

    Returns:
        int: The frame duration
    """
    return int(frame_size(sample_rate)[2][-1])

# SLOW IMPLEMENTATION
# def unpack2bit(data: bytes, nchannel: int=1, dtype: np.dtype=np.int8) -> np.ndarray:
#     """Unpacks a bytes-object containing VDIF payload data to a numpy array

#     Args:
#         bytes (str): bytes-object containing the VDIF data
#         nchannel (int, optional): number of channels, samples with the same timestamp of different
#             channels are adjacent. Defaults to 1.
#         dtype (np.dtype, optional): The data type to convert to. Defaults to np.int8.

#     Returns:
#         np.ndarray: The numpy array containg the unpacked data (shape: nchannel, nsamples)
#     """
#     data = np.frombuffer(io.BytesIO(data).getbuffer(), dtype=np.uint8)
#     data = np.packbits(np.unpackbits(data, bitorder="little").reshape(-1, nchannel, 2),
#                        axis=-1, bitorder="little")
#     return data.squeeze(axis=-1).transpose(1,0).astype(dtype)

def unpack2bit(raw: bytes, nchannel: int = 1, dtype: np.dtype = np.int8) -> np.ndarray:
    """Unpacks a bytes-object containing VDIF payload data to a numpy array.

    Args:
        data (bytes): Bytes-object containing the VDIF data.
        nchannel (int, optional): Number of channels, samples with the same timestamp of different
            channels are adjacent. Defaults to 1.
        dtype (np.dtype, optional): The data type to convert to. Defaults to np.int8.

    Returns:
        np.ndarray: The numpy array containing the unpacked data (shape: nchannel, nsamples).
    """
    # Convert the bytes data to a numpy array of uint8
    data = np.frombuffer(raw, dtype=np.uint8)
    nsamples = len(raw) * 4 // nchannel
    # Prepare an array to store the unpacked data
    unpacked_data = np.empty((nchannel, nsamples), dtype=dtype)
    if nchannel > 4:
        niter = nchannel // 4
    else:
        niter = 1
    # Unpack the data
    step = 4 // nchannel
    if step == 0:
        step = 1
    for i in range(niter):
        ii = i * 4
        channel_data = data[i::niter]
        unpacked_data[(ii+0) % nchannel, 0//nchannel::step] = (channel_data & 0x03)       # First 2 bits
        unpacked_data[(ii+1) % nchannel, 1//nchannel::step] = (channel_data >> 2) & 0x03  # Second 2 bits
        unpacked_data[(ii+2) % nchannel, 2//nchannel::step] = (channel_data >> 4) & 0x03  # Third 2 bits
        unpacked_data[(ii+3) % nchannel, 3//nchannel::step] = (channel_data >> 6) & 0x03  # Fourth 2 bits

    return unpacked_data

def pack2bit(array: np.ndarray) -> bytes:
    """Packs a numpy uint8 array into 2 bit data. All values greater than 3 are discarded

    Args:
        array (np.ndarry): The array to pack
    Returns:
        bytes: a byte string containing the packed data
    """
    array = array.astype(np.uint8)
    out = np.zeros(array.size // 4, dtype=np.uint8)
    data = np.unpackbits(array, bitorder="little")\
        .reshape(array.shape[0], array.shape[1], 8)[...,:2]
    data = np.packbits(data, bitorder="little", axis=-1).flatten()
    for i in range(array.size // 4):
        out[i] = (data[i*4] & 0b00000011)
        out[i] += (data[i*4+1] & 0b00000011) << 2
        out[i] += (data[i*4+2] & 0b00000011) << 4
        out[i] += (data[i*4+3] & 0b00000011) << 6
    return out.tobytes()

class Header:
    """
    Class to store the header information of a VDIF frame
    """
    def __init__(self):
        """Constructor, setting all attributes to default arguments
        """
        self.update_cnt = 0
        self.invalid = 0
        self.legacy_mode = 0
        self.seconds_from_epoch = 0
        self.unassigned = 0
        self.ref_epoch = 46
        self.frame_id = 0
        self.version = 0
        self.log2channels = 0
        self.data_frame_len = 1004
        self.complex = 0
        self.bit_sample = 1
        self.thread_id = 0
        self.station_id = 0
        self.edv = 0
        self.word4_user = 0
        self.word5_user = 0
        self.word6_user = 0
        self.word7_user = 0

    def __str__(self) -> str:
        """string representation of header

        Returns:
            str: The header information as string
        """
        s = f"Header {self.update_cnt}\n"
        s += json.dumps(self.to_dict(), indent=4)
        return s

    def update(self, raw: bytes):
        """
        Updates the object attributes by the given bytes object.

        Args:
            raw (bytes): bytes object must be 32 bytes long.
        """
        assert(len(raw) == HEADER_SIZE), f"VDIF header.update() expects 32 bytes, got {len(raw)}"
        words = []
        stream = io.BytesIO(raw)
        for __ in range(0,8):
            words.append(struct.unpack("<I", stream.read(4))[0])
        self.invalid = (words[0] & 0x80000000) >> 31
        self.legacy_mode = (words[0] & 0x40000000) >> 30
        self.seconds_from_epoch = (words[0] & 0x3FFFFFFF)
        self.unassigned = (words[1] & 0xC0000000) >> 30
        self.ref_epoch = (words[1] & 0x3F000000) >> 24
        self.frame_id = words[1] & 0x00FFFFFF
        self.version = (words[2] & 0xE0000000) >> 29
        self.log2channels = (words[2] & 0x1F000000) >> 24
        self.data_frame_len = words[2] & 0x00FFFFFF
        self.complex = (words[3] & 0x80000000) >> 31
        self.bit_sample = (words[3] & 0x7C000000) >> 26
        self.thread_id = (words[3] & 0x03FF0000) >> 16
        self.station_id = (words[3] & 0x0000FFFF)
        self.edv = (words[4] & 0xFF000000) >> 24
        self.word4_user = words[4] & 0x00FFFFFF
        self.word5_user = words[5] & 0xFFFFFFFF
        self.word6_user = words[6] & 0xFFFFFFFF
        self.word7_user = words[7] & 0xFFFFFFFF
        self.update_cnt += 1

    def to_bytes(self) -> bytes:
        """Converts the header into a bytes object

        Returns:
            bytes: The header represented as bytes
        """
        words = [
            (self.invalid << 31) | (self.legacy_mode << 30) | self.seconds_from_epoch,
            (self.unassigned << 30) | (self.ref_epoch << 24) | self.frame_id,
            (self.version << 29) | (self.log2channels << 24) | self.data_frame_len,
            (self.complex << 31) | (self.bit_sample << 26) | (self.thread_id << 16) | self.station_id,
            (self.edv << 24) | self.word4_user,
            self.word5_user,
            self.word6_user,
            self.word7_user,
        ]
        stream = io.BytesIO()
        for word in words:
            stream.write(struct.pack("<I", word))
        return stream.getvalue()

    def to_dict(self) -> dict:
        """dictionary representation of the header

        Returns:
            dict: dictionary containing header data
        """
        d = {"word0":{},"word1":{},"word2":{},"word3":{},
             "word4":{},"word5":{},"word6":{},"word7":{}
        }
        d["word0"]["i"] = self.invalid
        d["word0"]["l"] = self.legacy_mode
        d["word0"]["seconds_from_epoch"] = self.seconds_from_epoch
        d["word1"]["unassigned"] = self.unassigned
        d["word1"]["ref_epoch"] = self.ref_epoch
        d["word1"]["frame_id"] = self.frame_id
        d["word2"]["v"] = self.version
        d["word2"]["log2chns"] = self.log2channels
        d["word2"]["data_frame_len"] = self.data_frame_len
        d["word3"]["c1"] = self.complex
        d["word3"]["bit_sample"] = self.bit_sample
        d["word3"]["thread_id"] = self.thread_id
        d["word3"]["station_id"] = self.station_id
        d["word4"]["edv"] = self.edv
        d["word4"]["user"] = self.word4_user
        d["word5"]["user"] = self.word5_user
        d["word6"]["user"] = self.word6_user
        d["word7"]["user"] = self.word7_user
        return d

    @property
    def nchannel(self) -> int:
        """The number of channels within one VDIF frame

        Returns:
            int: number of channels
        """
        return 2**self.log2channels

    @property
    def psize(self) -> int:
        """The payload size of a frame

        Returns:
            int: The payload size
        """
        return int(self.data_frame_len * 8 - HEADER_SIZE)

    @property
    def nsample(self) -> int:
        """The number of samples per channel within a VDIF frame

        Returns:
            int: The number of samples
        """
        return int(self.psize * 8 // ((self.bit_sample+1) * self.nchannel))

    @property
    def timestamp(self) -> int:
        """Computes the POSIX timestamp of the first sample in the data frame

        Returns:
            int: the timestamp of the first sample in the given second
        """
        ref_datetime = datetime.datetime(
            2000 + self.ref_epoch // 2, 1 + (self.ref_epoch % 2) * 6,
            1, tzinfo=datetime.timezone.utc)
        delta = datetime.timedelta(seconds=self.seconds_from_epoch)
        dt = ref_datetime + delta
        return int(dt.timestamp())


class Payload():
    """
    Class to store the payload data of a VDIF frame
    """
    def __init__(self, nsample: int, nchannel: int=0, dtype=np.int8):
        self.update_cnt = 0
        self.nsample = nsample
        self.nchannel = nchannel
        self.dtype = dtype
        self.sample_cnt = 0
        self._data = np.zeros((self.nchannel, self.nsample), dtype=self.dtype)
        self._histogram = np.zeros((self.nchannel, 4), dtype=np.int64)

    def update(self, raw: bytes) -> None:
        """Updates the data by the given bytes object.

        Args:
            raw (bytes): bytes object must be 32 bytes long.
        """
        self._data[...] = unpack2bit(raw, self.nchannel, np.int8)
        self.update_cnt += 1

    def to_bytes(self) -> bytes:
        """The bytes representation of the payload data

        Returns:
            bytes: The payload in a bytes-object
        """
        return pack2bit(self.data.transpose(1,0))

    @property
    def data(self) -> np.ndarray:
        """The payload data represented in a numpy array. The array shape is channels, samples

        Returns:
            np.ndarray: The payload data as numpy array
        """
        return self._data

    @property
    def histogram(self) -> np.ndarray:
        """Calculates the histogram of the payload represented as a numpy array.
            The shape is channels, 4

        Returns:
            np.ndarray: The histogram as numpy array
        """
        for chan in range(self.nchannel):
            for i in range(4):
                self._histogram[chan][i] = np.count_nonzero(self._data[chan] == i)
        return self._histogram

    @property
    def samples(self) -> int:
        """The total number of samples processed with this Payload-object

        Returns:
            int: The total number of samples
        """
        return self.nsample * self.update_cnt



class Reader(io.BufferedReader):
    """Class to process a VDIF byte stream
    """
    def __init__(self, stream: io.BufferedReader, offset: int=0, frames=-1):
        """Constructor

        Args:
            stream (io.BufferedReader): The IO stream to process
            offset (int, optional): The start offset when the processing starts. Defaults to 0.
            frames (int, optional): The total number of frames to process, when set to -1 it
                processes the whole streamm. Defaults to -1.
        """
        super().__init__(stream)
        self.seek(0, 2)
        self.filesize = self.tell()
        self.offset = int(offset)
        self.seek(self.offset)
        self.header = Header()
        self.header.update(self.read(HEADER_SIZE))
        self.payload = Payload(self.header.nsample, self.header.nchannel)
        self.seek(self.offset, 0)
        self.call_cnt = 0
        self.header.update_cnt = 0
        self.payload.update_cnt = 0
        self.fs = None
        self.fps = None
        self.frames = frames
        self.tot_frames = (self.filesize - self.offset) // (self.header.data_frame_len * 8)
        self.tot_samples = self.tot_frames * self.nsample
        # Read all frames / entire file
        if self.frames == -1:
            self.frames = (self.filesize - self.offset) // (self.header.data_frame_len * 8)



    def __next__(self) -> tuple:
        """Iterator function which reads the next frame. It returns a Header and Payload object

        Raises:
            StopIteration: When the EOF is reached, it raises StopIteration

        Returns:
            tuple(Header, Payload):
        """
        if self.frames == self.call_cnt:
            raise StopIteration
        self.call_cnt += 1
        self.header.update(self.read(HEADER_SIZE))
        self.payload.update(self.read(self.header.data_frame_len*8-HEADER_SIZE))
        return self.header, self.payload

    def reset(self):
        """Resets the file pointer to the start of the file
        """
        self.seek(self.offset, 0)

    @property
    def psize(self) -> int:
        """The payload size

        Returns:
            int: the payload size in bytes
        """
        return self.header.psize


    @property
    def nchannel(self) -> int:
        """The number of channels within one VDIF frame

        Returns:
            int: number of channels
        """
        return self.header.nchannel

    @property
    def nsample(self) -> int:
        """The number of samples per channel within a VDIF frame

        Returns:
            int: The number of samples
        """
        return self.header.nsample

    @property
    def frame_size(self) -> int:
        """The size of a VDIF frame in bytes

        Returns:
            int: The size
        """
        return self.header.psize + HEADER_SIZE

    @property
    def frames_per_second(self) -> int:
        """The estimated frames per second from the byte stream

        Returns:
            float: The frames per second
        """
        pointer = self.tell()
        cur_epoch = self.header.seconds_from_epoch
        while self.header.seconds_from_epoch == cur_epoch:
            self.get_header()
        self.seek(int(self.tell() - self.header.data_frame_len*8*2))
        self.get_header()
        self.fps = self.header.frame_id + 1
        self.seek(pointer)
        return self.fps

    @property
    def sample_rate(self) -> float:
        """The estimated sample rate from the byte stream

        Returns:
            float: The sample rate
        """
        if self.fs is not None:
            return self.fs
        self.fs = self.frames_per_second * self.header.nsample
        return self.fs

    def seek_frame(self, seconds_from_epoch: int, frame_id: int=0):
        """Seek to a specific data frame with the given seconds_from_epoch and frame_id.

        Args:
            seconds_from_epoch (int): The seconds from the epoch of the desired frame.
            frame_id (int, optional): The frame ID of the desired frame. Defaults to 0.
        ToDo: Cath backwards seeking not implemented yet
        """
        pointer = self.tell()
        cur_frame_id = self.header.frame_id
        cur_sec_epoch = self.header.seconds_from_epoch
        delta_seconds = seconds_from_epoch - cur_sec_epoch
        delta_frames = frame_id - cur_frame_id

        # # adjust delta values to be negative if seeking backwards
        # if delta_seconds < 0:
        #     delta_frames -= (abs(delta_seconds) * self.frames_per_second)
        # if delta_frames < 0:
        #     delta_seconds -= (abs(delta_frames) // self.frames_per_second) + 1

        new_pointer = pointer + (delta_seconds * self.frames_per_second + delta_frames) \
            * self.header.data_frame_len * 8
        if new_pointer >= self.filesize or new_pointer < self.offset:
            raise RuntimeError("Frame does not exists in this stream")
        self.seek(new_pointer)


    def _seek_next_frame(self) -> bool:
        """Sets the stream pointer to the beginning of the next frame

        Returns:
            bool: Return True if it could set the pointer, False otherwise
        """
        current_frame = (self.tell() - self.offset) / (self.header.data_frame_len * 8)
        if current_frame % 1:
            self.seek(int(current_frame + 1) * (self.header.data_frame_len * 8) + self.offset)
        # If end of file reached
        if self.tell() + self.header.data_frame_len * 8 >= self.filesize:
            return False
        return True

    def get_payload(self) -> Payload:
        """Reads the payload and updates the Payload-object with new data of the next frame.

        Returns:
            Payload: the updated Payload object
        """
        if not self._seek_next_frame():
            return False
        self.seek(self.tell() + HEADER_SIZE)
        self.payload.update(self.read(self.header.psize))
        return self.payload

    def get_header(self):
        """Reads the header and updates the Header-object with new data of the next frame.

        Returns:
            Header: the updated Header object
        """
        if not self._seek_next_frame():
            return False
        self.header.update(self.read(HEADER_SIZE))
        return self.header

    # def read_data(self, nframes: int=-1, dtype: np.dtype=np.int8) -> np.ndarray:
    #     """Reads nframes and returns the payload data as numpy array. The file pointer
    #         stops at the position of the last frame. When calling this function again
    #         it continues to read data, except when the file pointer was manipulated (e.g. reset, __next__)

    #     Args:
    #         nframes (int): The number of frames to read

    #     Returns:

    #     """
    #     if nframes > self.tot_frames:
    #         nframes = self.tot_frames
    #         _LOG.warning("More frames to read than file contains, setting nframes to %s", nframes)
    #     if nframes == -1:
    #         nframes = self.tot_frames
    #     raw = self.read(nframes * self.frame_size)
    #     data = np.zeros((self.nchannel, self.nsample*nframes), dtype=dtype)
    #     for i in range(nframes):
    #         rstart = HEADER_SIZE + i * self.frame_size
    #         dstart = i * self.nsample
    #         rstop = rstart + self.psize
    #         dstop = dstart + self.nsample
    #         data[:, dstart:dstop] = unpack2bit(raw[rstart:rstop], self.nchannel, dtype)
    #     return data

    def read_data(self, nframes: int=-1, dtype: np.dtype=np.int8) -> np.ndarray:
        """Reads nframes and returns the payload data as numpy array. The file pointer
            stops at the position of the last frame. When calling this function again
            it continues to read data, except when the file pointer was manipulated (e.g. reset, __next__)

        Args:
            nframes (int): The number of frames to read

        Returns:

        """
        if nframes > self.tot_frames:
            nframes = self.tot_frames
            _LOG.warning("More frames to read than file contains, setting nframes to %s", nframes)
        if nframes == -1:
            nframes = self.tot_frames
        raw = (np.frombuffer(self.read(nframes*self.frame_size), dtype=np.int8).reshape(-1, self.frame_size)[:, HEADER_SIZE:]).tobytes()
        return unpack2bit(raw, self.nchannel)