#pragma once

#include <cuda.h>


namespace edd_dbbc {
namespace ddc {
namespace signal {
namespace kernel {

__global__ void chirp(
    float* data,
    float fs,
    float f0,
    float f1,
    float gain,
    float t1,
    int width)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    float duration = width / fs;
    float beta = (f1 - f0) / duration, phase, t;
    if(tid < width)
    {
        t = tid / fs;
        phase = 2 * M_PI * (f0 * t + 0.5 * beta * t * t);
        data[tid] = gain * cosf(phase);
    }
}


__global__ void phase(
    float2* phases,
    float* freq,
    std::size_t fs,
    std::size_t nsamples,
    double precision)
{
    int tidx = threadIdx.x;
    double phase = -2.0 * M_PI * freq[tidx] * nsamples / static_cast<double>(fs);
    phases[tidx].x = cos(phase);
    phases[tidx].y = sin(phase);
    if(phases[tidx].x >= -precision && phases[tidx].x <= precision){
        phases[tidx].x = 0;
    }
    if(phases[tidx].y >= -precision && phases[tidx].y <= precision){
        phases[tidx].y = 0;
    }
}

template<typename T>
__global__ void oscillate(
    float2* data,
    float* lo,
    float fs,
    int width,
    int height,
    int offset)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    int tidy = blockIdx.y;
    double fs_1 = 1/static_cast<double>(fs);
    if(tidx < width && tidy < height)
    {
        T phase = -2.0 * lo[tidy] * ((tidx+offset) * fs_1);
        if constexpr(std::is_same<float, T>::value)
        {
            data[tidy * width + tidx].x = cospif(phase);
            data[tidy * width + tidx].y = sinpif(phase);
        }
        else
        {
            data[tidy * width + tidx].x = cospi(phase);
            data[tidy * width + tidx].y = sinpi(phase);
        }
    }
}


}
}
}
}