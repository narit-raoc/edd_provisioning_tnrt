import asyncio
import json
import os
import numpy as np
import matplotlib.pyplot as plt
from mpikat.core import logger
from mpikat.utils.testing.provision_tests import register, Stage, EDDTestContext
from mpikat.pipelines.gated_spectrometer.plotter import encodeb64_figure

from dbbc.vdif import Reader, HEADER_SIZE

_LOG = logger.getLogger('mpikat.dbbc.testing.provision_tests.vdif_packer')

@register(stage=Stage.measurement_start, class_name='VDIFPacker')
async def test_is_receiving_data(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    # Check number of expected polarizations
    _LOG.debug('Checking input data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))
    active_polarizations = set([i['polarization'] for i in cfg['input_data_streams']])
    _LOG.debug('Active polarizations: %s', active_polarizations)

    await asyncio.sleep(cfg["samples_per_block"] / cfg["input_data_streams"][0]["sample_rate"] * 8)

    i1 = {}
    for p in active_polarizations:
        _LOG.debug("Requesting sensor %s", f'input-buffer-total-write-polarization_{p}')
        _, results = await context.pipeline.request("sensor-value", f'input-buffer-total-write-polarization_{p}')
        _LOG.debug("Results %s", results)
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _LOG.debug('Received input buffer reads: %s', i1)

    for p in i1:
        assert i1[p] > 0, f"No input buffers processed for polarization {p}, got {i1[p]}  and {i1[p]} buffers "

@register(stage=Stage.measurement_start, class_name='VDIFPacker')
async def test_create_file(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))

    await asyncio.sleep(cfg["file_duration"] * 3)

@register(stage=Stage.measurement_stop, class_name='VDIFPacker', tbc=True)
async def test_folding_noise_diode(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg: dict = json.loads(result[0].arguments[4].decode('ascii'))
    resp: dict = context.stage_response
    subdirs: list = []
    for path, meta in resp.items():
        if cfg["id"] == meta["pipeline_id"]:
            subdirs.append(path)
    _LOG.info("Stage results %s", context.stage_response)
    _LOG.info("subdirs %s, ostreams: %d", str(subdirs), len(cfg["output_data_streams"]))
    assert(len(subdirs) == len(cfg["output_data_streams"])), "Number of output data streams does not match number of subdirectories"

    duration = int(cfg["file_duration"])
    for dir in subdirs:
        fname = os.listdir(dir)[0]
        fsize = os.path.getsize(fname)
        _LOG.info("Filename %s, size: %d", fname, fsize)
        with open(fname, "rb") as f:
            reader = Reader(f, 4096)
            frames_per_file = reader.frames_per_second * duration
            expected_size = frames_per_file * reader.frame_size + reader.offset
            _LOG.info("Expected size: %d", expected_size)
            assert(fsize == expected_size), f"Expected size {expected_size} not equal to actual size {fsize}"
            tscrunch = reader.sample_rate // 1000
            _LOG.info("Start processing of file ", fname)
            for seconds in range(duration):
                idata = np.zeros((reader.nchannel, reader.sample_rate), reader.payload.dtype)
                odata = np.zeros((reader.nchannel, duration, 1000), reader.payload.dtype)
                for i, __, payload in enumerate(reader):
                    idata[:, i*reader.nsample: (i+1)*reader.nsample] = payload.data
                odata[:, seconds] = idata.reshape(reader.nchannel, 1000, tscrunch).sum(axis=2)
            _LOG.info("Processed %s", fname)
        fig = plt.figure(num=len(odata), figsize=(8,4))
        for i in range(len(odata)):
            sub = fig.add_subplot(len(odata), 1, i+1)
            sub.imshow(odata[i], aspect="auto", interpolation="nearest")
            sub.set_xlabel('ms')
            sub.set_ylabel('s')
        fig.suptitle(f"{dir.split('/')[0]}")
        context.output.append(encodeb64_figure(fig).decode('ascii'))


@register(stage=Stage.measurement_start, class_name='VDIFPacker')
async def test_is_bottleneck(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    # Check number of expected polarizations
    _LOG.debug('Checking input data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))
    active_polarizations = set([i['polarization'] for i in cfg['input_data_streams']])
    _LOG.debug('Active polarizations: %s', active_polarizations)

    await asyncio.sleep(cfg["samples_per_block"] / cfg["input_data_streams"][0]["sample_rate"] * 8)

    i1 = {}
    for p in active_polarizations:
        _, results = await context.pipeline.request("sensor-value", f'input-buffer-fill-level-polarization_{p}')
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _LOG.debug('Received input fille level: %s', i1)

    for p in i1:
        assert i1[p] < 0.1, f"Buffer fill level higher than allowed for polarization{p}"
