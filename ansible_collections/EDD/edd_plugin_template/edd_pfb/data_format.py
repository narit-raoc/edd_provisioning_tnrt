from mpikat.core.data_stream import DataStream, DataStreamRegistry
class EDDPFBDataFormat(DataStream):
        stream_meta = {
                "type": "EDDPFB:1",
                "ip": "",
                "port": "",
                "description": "Spead stream of channelized data.",
                "central_freq": "",
                "receiver_id": "",
                "band_flip": ""}

DataStreamRegistry.register(EDDPFBDataFormat)
