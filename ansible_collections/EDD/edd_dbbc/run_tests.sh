#!/bin/bash
START=$(date +%s)
LOG_LEVEL=CRITICAL PYTHONASYNCIODEBUG=1 OMPI_MCA_btl=^openib parallel -v  -k 'python3-coverage run -p --source="dbbc" -m unittest' ::: tests/test_*.py
TEST_RESULT=$?
END=$(date +%s)
sleep 1
DIFF=$((END-START))
echo
echo "Ran all tests. Overall test result:"
if [ $TEST_RESULT -ne 0 ]
then
	echo " --> FAIL"
else
	echo " --> OK"
fi
echo "Duration of all tests: ${DIFF} s"
exit $TEST_RESULT

