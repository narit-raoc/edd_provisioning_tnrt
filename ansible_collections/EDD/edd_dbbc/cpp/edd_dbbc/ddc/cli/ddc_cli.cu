#include <boost/program_options.hpp>

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/dada_input_stream.hpp>
#include <psrdada_cpp/dada_null_sink.hpp>
#include <psrdada_cpp/dada_output_stream.hpp>
#include <psrdada_cpp/multilog.hpp>
#include <psrdada_cpp/simple_file_writer.hpp>

#include <ctime>
#include <iostream>
#include <bits/stdc++.h>

#include "edd_dbbc/ddc/pipeline.cuh"

using namespace psrdada_cpp;

const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t SUCCESS = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

template<typename InputType>
void launch_pipeline(edd_dbbc::ddc::pipe_config config)
{
  MultiLog log("edd_dbbc::ddc_processor");
  DadaClientBase client(config.input_key(), log);
  DadaOutputStream sink(config.output_key(), log);
  config.input_size(client.data_buffer_size());
  config.output_size(sink.client().data_buffer_size());
  edd_dbbc::ddc::Pipeline<decltype(sink), InputType> pipeline(config, sink);
  DadaInputStream<decltype(pipeline)> istream(config.input_key(), log, pipeline);
  istream.start();
}


int main(int argc, char **argv) {
  try {
    edd_dbbc::ddc::pipe_config config;
    std::size_t device;
    std::string i_dtype;
    /** Define and parse the program options
    */
    namespace po = boost::program_options;
    po::options_description desc("Options");

    desc.add_options()("help,h", "Print help messages");
    desc.add_options()("input_key,i",
      po::value<std::string>()->required()->notifier(
          [&config](std::string val) { config.input_key(string_to_key(val)); }),
      "The shared memory key for the input dada buffer to connect to (hex string)");
    desc.add_options()("output_key,o",
      po::value<std::string>()->required()->notifier(
          [&config](std::string val) { config.output_key(string_to_key(val)); }),
      "The shared memory key for the output dada buffer to connect to (hex string)");
    desc.add_options()("device,g", po::value<std::size_t>(&device)->default_value(0),
      "The ID of the GPU device");
    desc.add_options()("oscillations,lo",
      po::value<std::string>(&config.ddc.f_lo)->default_value("8000"),
      "The local oscillation frequencies. Seperate by ',' to use multiple");
    desc.add_options()("sample_clock,s",
      po::value<std::size_t>(&config.ddc.fs_in)->default_value(4000000000),
      "The input sample frequency");
    desc.add_options()("output_clock,c",
      po::value<std::size_t>(&config.ddc.fs_dw)->default_value(16000000),
      "The output sample frequency");
    desc.add_options()("mult,m",
      po::value<std::size_t>(&config.ddc.lowpass.mult)->default_value(4),
      "Multiplier for the number of lowpass filter taps.\n\tEquation is: ntaps = max(down, up) * mult,\n\twhere down and up are the sampling factors");
    desc.add_options()("dtype,d",
      po::value<std::string>(&i_dtype)->default_value("int"),
      "Data type of the input data ('int', 'float')");
    desc.add_options()("nbit,b", po::value<std::size_t>()->default_value(8)->notifier(
          [&config](std::size_t val) { config.nbit(val); }),
      "Number of bits of the input data type (8,10,12), ignored when dtype is 'float'");
    desc.add_options()("nsamples_pack,n", po::value<std::size_t>()->default_value(0)->notifier(
          [&config](std::size_t val) { config.batch_size(val); }),
      "Number of consecutive samples of the same channel. Set to 0 for a full batch");

    po::variables_map vm;
    try {
      po::store(po::parse_command_line(argc, argv, desc), vm);
      if (vm.count("help")) {
        std::cout << "DDC Resampler" << std::endl << desc << std::endl;
        return SUCCESS;
      }

      po::notify(vm);

    } catch (po::error &e) {
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << desc << std::endl;
      return ERROR_IN_COMMAND_LINE;
    }
    CUDA_ERROR_CHECK(cudaSetDevice(device));
    if(i_dtype == "int"){
      launch_pipeline<uint64_t>(config);
    }else if(i_dtype == "float"){
      config.nbit(32);
      launch_pipeline<float>(config);
    }else{
      throw std::runtime_error("Unsupported input type");
    }

  } catch (std::exception &e) {
    std::cerr << "Unhandled Exception reached the top of main: " << e.what()
              << ", application will now exit" << std::endl;
    return ERROR_UNHANDLED_EXCEPTION;
  }
  return SUCCESS;
}
