#include <gtest/gtest.h>
#include <cuda.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <psrdada_cpp/testing_tools/tools.cuh>

#include "edd_dbbc/testing_tools.cuh"
#include "edd_dbbc/math_tools.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace tools {
namespace test {

TEST(mean, mean_test_double)
{
  int mean = 27; int sigma = 13;
  thrust::device_vector<double> data = psr_testing::random_vector<double>(mean, sigma, 1000);
  double result = edd_dbbc::tools::mean(data);
  ASSERT_TRUE(result + result * 0.1 >= mean && result - result * 0.1 <= mean)
    << "Expected mean is " << mean << ", but actual value is " << result << std::endl;
}

TEST(mean, mean_test_float)
{
  int mean = -11; int sigma = 3;
  thrust::device_vector<float> data = psr_testing::random_vector<float>(mean, sigma, 1000);
  float result = edd_dbbc::tools::mean(data); // Execute function under test
  ASSERT_TRUE(abs(result + result * 0.1) >= abs(mean) && abs(result - result * 0.1) <= abs(mean))
    << "Expected mean is " << mean << ", but actual value is " << result << std::endl;
}

TEST(mean, mean_test_int)
{
  int mean = 9; int sigma = 7;
  thrust::device_vector<int> data = psr_testing::random_vector<int>(mean, sigma, 1000);
  int result = edd_dbbc::tools::mean(data);
  ASSERT_TRUE(result == mean)
    << "Expected mean is " << mean << ", but actual value is " << result << std::endl;
}

TEST(variance, variance_test_float)
{
  int mean = 5; int sigma = 10;
  thrust::device_vector<float> data = psr_testing::random_vector<float>(mean, sigma, 1000);
  float result = edd_dbbc::tools::variance(data, (float)(mean));
  ASSERT_TRUE(result + result * 0.1 >= sigma*sigma && result - result * 0.1 <= sigma*sigma)
    << "Expected variance is " << sigma*sigma << ", but actual value is " << result << std::endl;
}

TEST(variance, variance_test_double)
{
  int mean = 1000; int sigma = 200;
  thrust::device_vector<double> data = psr_testing::random_vector<double>(mean, sigma, 1000);
  double result = edd_dbbc::tools::variance(data, (double)(mean));
  ASSERT_TRUE(result + result * 0.1 >= sigma*sigma && result - result * 0.1 <= sigma*sigma)
    << "Expected variance is " << sigma*sigma << ", but actual value is " << result << std::endl;
}

TEST(variance, variance_test_int)
{
  int mean = 10; int sigma = 3;
  thrust::device_vector<int> data = psr_testing::random_vector<int>(mean, sigma, 1000);
  int result = edd_dbbc::tools::variance(data, mean) + 1;
  ASSERT_TRUE(result == sigma*sigma)
    << "Expected variance is " << sigma*sigma << ", but actual value is " << result << std::endl;
}

}
}
}