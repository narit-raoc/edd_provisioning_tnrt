#include <gtest/gtest.h>
#include <psrdada_cpp/cli_utils.hpp>

#include "edd_dbbc/ddc/test/test_signal.cuh"
#include "edd_dbbc/ddc/test/test_filter.cuh"
#include "edd_dbbc/ddc/test/test_processing.cuh"
#include "edd_dbbc/ddc/test/test_ddc.cuh"
#include "edd_dbbc/ddc/test/test_pipeline.cuh"

int main(int argc, char **argv) {

  char * val = getenv("LOG_LEVEL");
  if ( val )
  {
      psrdada_cpp::set_log_level(val);
  }

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
