"""
The PFB Analysis receives data in EDD packetizer format and channelizes it using a PFB.
"""


import asyncio
import json
import tempfile
import time
import copy
import os

from aiokatcp import Sensor, FailReply

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change

import mpikat.pipelines.digpack_controller  # Import to register data stream. pylint: disable=unused-import
from mpikat.utils.process_tools import ManagedProcess, command_watcher
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog
from mpikat.utils.core_manager import CoreManager
import mpikat.utils.dada_tools as dada
from mpikat.utils.mk_tools import MkrecvSensors
from mpikat.utils.ip_utils import ipstring_to_list
import mpikat.utils.numa as numa
import mpikat.core.logger

from ..data_format import EDDPFBDataFormat


_log = mpikat.core.logger.getLogger("mpikat.edd_pfb.CriticalPFBPipeline")

_DEFAULT_CONFIG = {
        "id": "PFB",
        "type": "PFBAnalysis",
        "supported_input_formats": {"MPIFR_EDD_Packetizer": [1]},      # supported input formats name:version
        "samples_per_block": 64 * 1024 * 1024,              # sampels per buffer block
        "nslots": 64,                                       # Slots to allocat in dada input buffer
        "numa_node": "auto",                                # Numa node to run on
        "input_data_streams":
        [
            {
                "format": "MPIFR_EDD_Packetizer:1",         # Format has version separated via colon
                "ip": "225.0.0.140+3",
                "port": "7148",
                "bit_depth" : 8,
                "sample_rate": 1300000000,
                "samples_per_heap": 4096,
                "polarization":0
            }
        ],
        "nchannels": 128,
        "ntaps": 4,
        "output_bit_depth" : 8,                             # Output bit-depth (2,4,8,16,32)
        "output_type": 'network',                              # ['network', 'disk', 'null'] 
        "dummy_input": False,                               # Use dummy input instead of mkrecv process.
        "log_level": "debug",

        "output_rate_factor": 1.10,                         # True output date rate is multiplied by this factor for sending.

        "output_data_streams":
        {
            "polarization_0" :
            {
                "format": "EDDPFB:1",
                "ip": "225.0.0.161",
                "port": "7152",
            },
             "polarization_1" :
            {
                "format": "EDDPFB:1",
                "ip": "225.0.0.162",
                "port": "7152",
            }
        },
        "idx1_modulo": "auto",
        "computing_cores": {                                # If auto, cores are selected automatically for numa node. otherwise, a list pf cores is selected and directly used
                "mkrecv": "auto",
                "mksend": "auto",
                "pfb": "auto"
                },


    }

# static configuration for mkrec. all items that can be configured are passed
# via cmdline
_mkrecv_header = """
## Dada header configuration
HEADER          DADA
HDR_VERSION     1.0
HDR_SIZE        4096
DADA_VERSION    1.0

## MKRECV configuration
PACKET_SIZE         8400
IBV_VECTOR          -1          # IBV forced into polling mode
IBV_MAX_POLL        10
BUFFER_SIZE         128000000

DADA_MODE           4    # The mode, 4=full dada functionality

SAMPLE_CLOCK_START  0 # This is updated with the sync-time of the packetiser to allow for UTC conversion from the sample clock

NTHREADS            32
NHEAPS              64
NGROUPS_TEMP        65536

#SPEAD specifcation for EDD packetiser data stream
NINDICES            1      # Although there is more than one index, we are only receiving one polarisation so only need to specify the time index

# The first index item is the running timestamp
IDX1_ITEM           0      # First item of a SPEAD heap

"""

# static configuration for mksend. all items that can be configured are passed
# via cmdline
_mksend_header = """
HEADER          DADA
HDR_VERSION     1.0
HDR_SIZE        4096
DADA_VERSION    1.0
BUFFER_SIZE         128000000

# MKSEND CONFIG
NETWORK_MODE  1
PACKET_SIZE 8400
IBV_VECTOR   -1          # IBV forced into polling mode
IBV_MAX_POLL 10

SYNC_TIME           unset  # Default value from mksend manual
UTC_START           unset  # Default value from mksend manual

HEAP_COUNT 1
HEAP_ID_START   1
HEAP_ID_OFFSET  1
HEAP_ID_STEP    13

NITEMS          7
ITEM1_ID        5632    # timestamp, slowest index

ITEM2_ID        5633    # polarization

ITEM3_ID        5634    # fft_length
ITEM4_ID        5635    # n_taps

ITEM5_ID        5636    # sync_time

ITEM6_ID        5637    # sampling rate

ITEM7_ID        5638    # payload item (empty step, list, index and sci)
"""



class EDDPFBAnalysis(EDDPipeline):
    VERSION_INFO = ("mpikat-edd-api", 0, 1)
    BUILD_INFO = ("mpikat-edd-implementation", 0, 1, "rc1")

    DATA_FORMATS = [EDDPFBDataFormat.stream_meta]
    def __init__(self, ip, port, loop=None):
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)

        self.dada_buffers = {}
        self.watchdogs = []

    def setup_sensors(self):
        """
        Setup monitoring sensors
        """
        super().setup_sensors()
        self._output_rate_status = Sensor(
            float, "output-rate",
            description="Output data rate [Gbyte/s]",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._output_rate_status)

        self._mkrecv_sensors = MkrecvSensors("")

        for s in self._mkrecv_sensors.sensors.values():
            self.sensors.add(s)


        self._input_buffer_fill_level = Sensor(
            float, "input-buffer-fill-level",
            description="Fill level of the input buffer"
        )
        self.sensors.add(self._input_buffer_fill_level)

        self._input_buffer_total_write = Sensor(
            float, "input-buffer-total-write",
            description="Total write into input buffer"
        )
        self.sensors.add(self._input_buffer_total_write)

        self._output_buffer_fill_level = Sensor(
            float, "output-buffer-fill-level",
            description="Fill level of the output buffer"
        )
        self.sensors.add(self._output_buffer_fill_level)
        self._output_buffer_total_read = Sensor(
            float, "output-buffer-total-read",
            description="Total read from output buffer"
        )
        self.sensors.add(self._output_buffer_total_read)



    def add_input_stream_sensor(self, streamid):
        """
        Add sensors for i/o buffers for an input stream with given stream-id.
        """
        self._polarization_sensors[streamid] = {}
        self._polarization_sensors[streamid]["mkrecv_sensors"] = mk.MkrecvSensors(streamid)
        for s in self._polarization_sensors[streamid]["mkrecv_sensors"].sensors.values():
            self.sensors.add(s)
        self._polarization_sensors[streamid]["input-buffer-fill-level"] = Sensor(
            float, "input-buffer-fill-level-{}".format(streamid),
            description="Fill level of the input buffer for polarization{}".format(streamid),
            initial_status=Sensor.Status.UNKNOWN,
            # params=[0, 1] # not supported by aiokatcp
        )
        self.sensors.add(self._polarization_sensors[streamid]["input-buffer-fill-level"])
        self._polarization_sensors[streamid]["input-buffer-total-write"] = Sensor(
            float, "input-buffer-total-write-{}".format(streamid),
            description="Total write into input buffer for polarization {}".format(streamid),
            initial_status=Sensor.Status.UNKNOWN,
            # params=[0, 1] # not supported by aiokatcp
        )
        self.sensors.add(self._polarization_sensors[streamid]["input-buffer-total-write"])
        self._polarization_sensors[streamid]["output-buffer-fill-level"] = Sensor(
            float, "output-buffer-fill-level-{}".format(streamid),
            description="Fill level of the output buffer for polarization {}".format(streamid),
            initial_status=Sensor.Status.UNKNOWN
        )
        self._polarization_sensors[streamid]["output-buffer-total-read"] = Sensor(
            float, "output-buffer-total-read-{}".format(streamid),
            description="Total read from output buffer for polarization {}".format(streamid),
            initial_status=Sensor.Status.UNKNOWN
        )
        self.sensors.add(self._polarization_sensors[streamid]["output-buffer-total-read"])
        self.sensors.add(self._polarization_sensors[streamid]["output-buffer-fill-level"])
        self.mass_inform('interface-changed')



#    def _buffer_status_handle(self, status):
#        """
#        Process a change in the buffer status.
#        """
#        timestamp = time.time()
#        for i, stream_description in enumerate(self._config["input_data_streams"]):
#            streamid = "polarization_{}".format(stream_description['polarization'])
#            if status['key'] == self._dada_buffers[i*2].key: # Multiply by 2 to access only input buffers
#                conditional_update(self._polarization_sensors[streamid]["input-buffer-total-write"], status['written'], timestamp=timestamp)
#                self._polarization_sensors[streamid]["input-buffer-fill-level"].set_value(status['fraction-full'], timestamp=timestamp)
#        for i, stream_description in enumerate(self._config["input_data_streams"]):
#            streamid = "polarization_{}".format(stream_description['polarization'])
#            if status['key'] == self._dada_buffers[i*2+1].key: # Multiply by 2 and add 1 to access only output buffers
#                self._polarization_sensors[streamid]["output-buffer-fill-level"].set_value(status['fraction-full'], timestamp=timestamp)
#                conditional_update(self._polarization_sensors[streamid]["output-buffer-total-read"], status['read'], timestamp=timestamp)


    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the EDD CCritical PFB
        """
        _log.info("Configuring EDD backend for processing")
        _log.info("Final configuration:\n%s", json.dumps(self._config, indent=4))
        if len(self._config['input_data_streams']) != 1:
            raise FailReply('Only one input data stream is supported')

        if self._config['numa_node'] == "auto":
            # remove numa nodes with missing capabilities
            for node in numa.getInfo():
                if len(numa.getInfo()[node]['gpus']) < 1:
                    _log.debug("Not enough gpus on numa node %s", node)
                    continue
                elif len(numa.getInfo()[node]['net_devices']) < 1:
                    _log.debug("Not enough nics on numa node %s", node)
                    continue
                self._config['numa_node'] = node

        if self._config['numa_node'] == "auto":
            raise FailReply("Not enough numa nodes to process data!")
        _log.debug("Running on numa node %s", self._config['numa_node'])

        stream_description = self._config["input_data_streams"][0]

        input_heapSize = stream_description ["samples_per_heap"] * stream_description['bit_depth'] / 8
        nHeaps = self._config["samples_per_block"] / stream_description["samples_per_heap"]
        input_bufferSize = nHeaps * input_heapSize

        _log.info('Input dada parameters created from configuration:\n\
                heap size:        %i byte\n\
                heaps per block:  %i\n\
                buffer size:      %i byte', input_heapSize, nHeaps, input_bufferSize)

        self.dada_buffers['input'] = dada.DadaBuffer(input_bufferSize, n_slots=self._config['nslots'],
                                                      node_id=self._config['numa_node'], pin=True, lock=True)


        # calculate output buffer parameters
        nSlices = max(self._config["samples_per_block"] / self._config['nchannels'] / 2, 1)

        # on / off spectrum  + one side channel item per spectrum
        output_buffer_size = nSlices * 2 * self._config['nchannels'] * self._config['output_bit_depth'] / 8


        self.dada_buffers['output'] = dada.DadaBuffer(output_buffer_size, n_slots=self._config['nslots'],
                                                      node_id=self._config['numa_node'], pin=True, lock=True)


        ## specify all subprocesses
        self.__coreManager = CoreManager(self._config['numa_node'], self._config['computing_cores'])
        self.__coreManager.add_task("pfb", 1)

        N_inputips = 0
        for p in self._config['input_data_streams'][0]["ip"].split(','):
            N_inputips+= len(ipstring_to_list(p))
        _log.debug("Found {} input ips".format(N_inputips))

        if not self._config["dummy_input"]:
            self.__coreManager.add_task("mkrecv", N_inputips + 1, prefere_isolated=True)

        if self._config["output_type"] == "network":
            self.__coreManager.add_task("mksend", 2)


        self._subprocessMonitor = SubprocessMonitor()
        # Configure + launch

        cmd = "taskset {physcpu} pfb --input_key={dada_key} --inputbitdepth={input_bit_depth} --fft_length={fft_length} --ntaps={ntaps}   -o {ofname} --log_level={log_level} --outputbitdepth={output_bit_depth} --output_type=dada".format(dada_key=self.dada_buffers['input'].key,
                                                                                                                                                                                                                                                 input_bit_depth=stream_description['bit_depth'],
                                                                                                                                                                                                                                                 fft_length=2*self._config['nchannels'],
                                                                                                                                                                                                                                                 ofname=self.dada_buffers['output'].key,
                                                                                                                                                                                                                                                 heapSize=input_heapSize, physcpu=self.__coreManager.get_coresstr('pfb'), **self._config)
#
#
#        cmd = "taskset -c {physcpu} gated_spectrometer {disable_gate_opt} --nsidechannelitems=1 --input_key={dada_key} --speadheap_size={heapSize} --selected_sidechannel=0 --nbits={bit_depth} --fft_length={fft_length} --naccumulate={naccumulate} -o {ofname} --log_level={log_level} --output_format=Stokes  --input_polarizations=Dual --output_type=dada".format(dada_key=self._dada_buffers[-2].key, ofname=self._dada_buffers[-1].key, heapSize=self.input_heapSize, numa_node=numa_node, bit_depth=self.stream_description['bit_depth'], physcpu=self.__coreManager.get_coresstr('gated_spectrometer'), disable_gate_opt=disable_gate_opt, **self._config)
#        _log.debug("Command to run: {}".format(cmd))
#
        cudaDevice = numa.getInfo()[self._config['numa_node']]['gpus'][0]
        pfb_cli = ManagedProcess(cmd, env={"CUDA_VISIBLE_DEVICES": cudaDevice})
        _log.debug("Visble Cuda Device: {}".format(cudaDevice))
        self._subprocessMonitor.add(pfb_cli, self._subprocess_error)
        self._subprocesses.append(pfb_cli)

        ip_range = []
        port = set()
        for key in self._config["output_data_streams"]:
            ip_range.append(self._config["output_data_streams"][key]['ip'])
            port.add(self._config["output_data_streams"][key]['port'])
        if len(port)!=1:
            raise FailReply("Output data has to be on the same port! ")

        if self._config["output_type"] == 'network':
            mksend_header_file = tempfile.NamedTemporaryFile(delete=False, mode='w')
            mksend_header_file.write(_mksend_header)
            mksend_header_file.close()

            sample_rate = self._config['input_data_streams'][0]['sample_rate']
            sync_time = self._config['input_data_streams'][0]['sync_time']

            nhops = len(ip_range)

            rate = output_buffer_size / nSlices / ( 2 * self._config['nchannels'] / sample_rate)
            rate *= self._config["output_rate_factor"]        # set rate to (100+X)% of expected rate

            timestep = self._config['nchannels'] * 2 
            #select network interface
            fastest_nic, nic_params = numa.getFastestNic(self._config['numa_node'])
            heap_id_start = 0 #2 * i    # two output spectra per pol

            _log.info("Sending data on NIC {} [ {} ] @ {} Mbit/s".format(fastest_nic, nic_params['ip'], nic_params['speed']))
            cmd = "taskset -c {physcpu} mksend --header {mksend_header} --heap-id-start {heap_id_start} --dada-key {ofname} --ibv-if {ibv_if} --port {port_tx} --sync-epoch {sync_time} --sample-clock {sample_rate} --item1-step {timestep} --item4-list {fft_length} --item6-list {sync_time} --item7-list {sample_rate} --rate {rate} --item11-list 1 --heap-size {heap_size} --nhops {nhops} {mcast_dest}".format(mksend_header=mksend_header_file.name, heap_id_start=heap_id_start, sync_time=sync_time,
                                                                                                                                                                                                                                                                                                                                                                                                                                                 timestep=timestep, sample_rate=sample_rate,
                        ofname=self.dada_buffers['output'].key, fft_length=2*self._config['nchannels'], physcpu=self.__coreManager.get_coresstr('mksend'), 
                                                                                                                                                                                                                                                                                                                                                                                                                                                 rate=rate, nhops=nhops, heap_size=output_buffer_size, ibv_if=nic_params['ip'],
                        mcast_dest=" ".join(ip_range),
                        port_tx=port.pop(), **self._config)

            _log.debug("Command to run: {}".format(cmd))
            mks = ManagedProcess(cmd, env={"CUDA_VISIBLE_DEVICES": cudaDevice})

        elif self._config["output_type"] == 'disk':
            ofpath = os.path.join(self._config["output_directory"], self.dada_buffers['output'].key)
            _log.debug("Writing output to {}".format(ofpath))

            if not os.path.isdir(ofpath):
                os.makedirs(ofpath)
            mks = dada.DbDisk(self.dada_buffers['output'].key, directory=ofpath).start()
        else:
            _log.warning("Selected null output. Not sending data!")
            mks = dada.DbNull(self._dada_buffers[-1].key, zerocopy=True).start()

        self._subprocessMonitor.add(mks, self._subprocess_error)
        self._subprocesses.append(mks)

        self._subprocessMonitor.start()


    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self, config_json=""):
        """
        start streaming of spectrometer output.
        """
        _log.info("Starting capturing")
        self.input_buffer_last_good = time.time()
        stream_description = self._config["input_data_streams"][0]
        try:
            input_heapSize = stream_description["samples_per_heap"] * stream_description['bit_depth'] // 8
            mkrecvheader_file = tempfile.NamedTemporaryFile(delete=False, mode="w")
            _log.debug("Creating mkrec header file: {}".format(mkrecvheader_file.name))
            mkrecvheader_file.write(_mkrecv_header)
            # DADA may need this
            # ToDo: Check for input stream definitions
            mkrecvheader_file.write("NBIT {}\n".format(stream_description["bit_depth"]))
            mkrecvheader_file.write("HEAP_SIZE {}\n".format(input_heapSize))


            mkrecvheader_file.write("\n#OTHER PARAMETERS\n")
            mkrecvheader_file.write("samples_per_block {}\n".format(self._config["samples_per_block"]))

            mkrecvheader_file.write("\n#PARAMETERS ADDED AUTOMATICALLY BY MKRECV\n")
            mkrecvheader_file.close()

            if not self._config['dummy_input']:
                fastest_nic, nic_params = numa.getFastestNic(self._config['numa_node'])
                _log.info("Receiving data on NIC {} [ {} ] @ {} Mbit/s".format(fastest_nic, nic_params['ip'], nic_params['speed']))

                if self._config['idx1_modulo'] == 'auto': # Align along output ranges
                    idx1modulo = self._config['nchannels'] * 2
                else:
                    idx1modulo = self._config['idx1_modulo']

                cmd = "taskset -c {physcpu} mkrecv_v4 --quiet --dnswtwr --lst --header {mkrecv_header} --idx1-step {samples_per_heap} --heap-size {input_heap_size} --idx1-modulo {idx1modulo}  --nthreads {nthreads}\
                --dada-key {dada_key} --sync-epoch {sync_time} --sample-clock {sample_rate} \
                --ibv-if {ibv_if} --port {port} {ip}".format(mkrecv_header=mkrecvheader_file.name, dada_key=self.dada_buffers['input'].key, 
                                                             physcpu=self.__coreManager.get_coresstr('mkrecv'), ibv_if=nic_params['ip'], input_heap_size=input_heapSize, idx1modulo=idx1modulo, nthreads=len(self.__coreManager.get_cores('mkrecv')) - 1, **stream_description)
                mk = ManagedProcess(cmd, stdout_handler=self._mkrecv_sensors.stdout_handler)
            else:
                _log.warning("Creating Dummy input instead of listening to network!")
                mk = dada.JunkDb(self._dada_buffers[-2].key, header=mkrecvheader_file.name, data_rate=1e9, duration=3600).start()

            self._subprocessMonitor.add(mk, self._subprocess_error)
            self._subprocesses.append(mk)

        except Exception as E:
            _log.error("Error starting pipeline: {}".format(E))
            raise E
        else:
            wd = SensorWatchdog(self._input_buffer_total_write, 10, self.watchdog_error)
            wd.start()
            self.watchdogs.append(wd)


    @state_change(target="idle", allowed=["streaming"], intermediate="capture_stopping")
    async def capture_stop(self):
        _log.info("Stoping EDD backend")
        await self.deconfigure()


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        Deconfigure the gated spectrometer pipeline.
        """
        _log.info("Deconfiguring EDD backend")
        for wd in self.__watchdogs:
            wd.stop_event.set()
        if self._subprocessMonitor is not None:
            self._subprocessMonitor.stop()
        # stop mkrec process
        _log.debug("Stopping mkrecv processes ...")
        for proc in self._mkrec_proc:
            proc.terminate()
        # This will terminate also the gated spectromenter automatically
        for proc in self._subprocesses:
            proc.terminate()

        _log.debug("Destroying dada buffers")

        tasks = []
        for buf in self._dada_buffers:
            tasks.append(asyncio.create_task(buf.destroy()))
        await asyncio.gather(*tasks)
        self._dada_buffers = []
        self.__coreManager = None





def main():
    """The main function wrapper
    """
    asyncio.run(launchPipelineServer(EDDPFBAnalysis))

if __name__ == "__main__":
    main()

