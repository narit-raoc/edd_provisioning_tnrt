
import threading
import time

class DummyCapture(threading.Thread):
    """A dummy class for emulating network capturing
    """
    def __init__(self, callback: callable=None, items: list=None):
        """Constructor of the class

        Args:
            callback (callable, optional): A callable object, which is called on every item in the list
            items (list, optional): List containing items which are passed to the callable
        """
        super().__init__()
        self._callback = callback
        self._items = items
        self._quit = threading.Event()

    def run(self):
        """Worker function, runs in a thread
        """
        if self._items and callable(self._callback):
            for item in self._items:
                self._callback(item)
        else:
            while not self._quit.is_set():
                time.sleep(1)

    @property
    def items(self) -> list:
        """Return a list of items

        Returns:
            list: list of items
        """
        return self._items

    @items.setter
    def items(self, items: list) -> None:
        """Sets the list of items

        Args:
            items (list): list of items
        """
        self._items = items

    def stop(self):
        """Stops the worker thread
        """
        self._quit.set()
