
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <gtest/gtest.h>
#include <chrono>

#include "edd_dbbc/testing_tools.cuh"
#include "edd_dbbc/ddc/signal.h"
#include "edd_dbbc/ddc/filter.h"
#include "edd_dbbc/ddc/processing.h"


namespace edd_dbbc {
namespace ddc{
namespace processing{
namespace test{

template<typename T>
void compare(
  thrust::host_vector<T> a,
  thrust::host_vector<T> b,
  std::size_t edge = 0,
  float abs=0.005,
  float rel=0.01)
{
    ASSERT_EQ(a.size(), b.size());
    if constexpr (std::is_same<float2, T>::value){
        for(std::size_t i = edge; i < a.size() - edge; i++){
            ASSERT_NEAR(a[i].x, b[i].x, abs + std::fabs(a[i].x) * rel) << "Position: " << i;
            ASSERT_NEAR(a[i].y, b[i].y, abs + std::fabs(a[i].y) * rel) << "Position: " << i;
        }
    }else{
        for(std::size_t i = edge; i < a.size() - edge; i++){
            ASSERT_NEAR(a[i], b[i], abs + std::fabs(a[i]) * rel) << "Position: " << i;
        }
    }
}


/*-----------------------------*/
/*  Generalized test functions */
/*-----------------------------*/

/**
 * @brief Tests if the upsample function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param length Length of the signal
 * @param up The up factor
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
template<typename T>
void test_upsample(
  std::size_t length,
  std::size_t up, float
  mean = 0,
  float sigma=10)
{
    thrust::host_vector<T> h_idata(length);
    thrust::host_vector<T> h_odata(length*up);
    signal::random(h_idata, mean, sigma);
    thrust::device_vector<T> idata = h_idata;
    thrust::device_vector<T> odata(length*up);
    processing::upsample(h_idata, h_odata, up);
    processing::upsample(idata, odata, up);
    thrust::host_vector<T> gpu = odata;
    compare(gpu, h_odata);
}
// Upsample tests
TEST(upsample, test_upsample_real_against_reference){
    test_upsample<float>(2049, 16);
}
TEST(upsample, test_upsample_cplx_against_reference){
    test_upsample<float2>(123456, 16);
}


/**
 * @brief Tests if the downsample function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param length Length of the signal
 * @param down The down factor
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
template<typename T>
void test_downsample(
  std::size_t length,
  std::size_t down,
  float mean = 0,
  float sigma=10)
{
    thrust::host_vector<T> h_idata(length);
    thrust::host_vector<T> h_odata(length / down);
    signal::random(h_idata, mean, sigma);
    thrust::device_vector<T> idata = h_idata;
    thrust::device_vector<T> odata(length / down);
    processing::downsample(h_idata, h_odata, down);
    processing::downsample(idata, odata, down);
    thrust::host_vector<T> gpu = odata;
    compare(gpu, h_odata);
}
// Downsample tests
TEST(downsample, test_downsample_real_against_reference){
    test_downsample<float>(16000, 16);
}
TEST(downsample, test_downsample_cplx_against_reference){
    test_downsample<float2>(32000, 16);
}

/**
 * @brief Tests if the convolve function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param signal_length Length of the signal
 * @param filter_length Length of the FIR filter
 * @param n_signals Number of signals to convolve
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
template<typename T>
void test_convolve(
  std::size_t signal_length,
  std::size_t filter_length,
  std::size_t n_signals=1,
  float mean = 0,
  float sigma=10)
{
    thrust::host_vector<T> h_idata(signal_length * n_signals);
    thrust::host_vector<T> h_odata(signal_length * n_signals);
    thrust::host_vector<float> h_filter(filter_length);
    signal::random( h_idata, mean, sigma );
    filter::firwin( h_filter );
    thrust::device_vector<T> idata = h_idata;
    thrust::device_vector<T> odata(signal_length * n_signals);
    thrust::device_vector<float> filt = h_filter;

    processing::convolve(idata, filt, odata, n_signals);
    processing::convolve(h_idata, h_filter, h_odata, n_signals);

    thrust::host_vector<T> gpu = odata;
    compare(gpu, h_odata);
}
// Convolve tests
TEST(convolve, test_convolve_real_against_reference){
    test_convolve<float>(123456, 1234);
}
TEST(convolve, test_convolve_cplx_against_reference){
    test_convolve<float2>(123456, 1234);
}
TEST(convolve, test_convolve_real_multiple_signals_against_reference){
    test_convolve<float>(12345, 1234, 3);
}
TEST(convolve, test_convolve_cplx_multiple_signals_against_reference){
    test_convolve<float2>(12345, 1234, 3);
}

/**
 * @brief Tests if the convolve function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param signal_length Length of the signal
 * @param filter_length Length of the FIR filter
 * @param n_signals Number of signals to convolve
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
void test_convolve_hilbert(
  std::size_t signal_length,
  std::size_t filter_length,
  std::size_t n_signals=1,
  float mean = 0,
  float sigma=10)
{
    thrust::host_vector<float2> h_idata(signal_length * n_signals);
    thrust::host_vector<float> h_odata(signal_length * n_signals * 2);
    thrust::host_vector<float> h_filter(filter_length);
    signal::random( h_idata, mean, sigma );
    filter::firhilbert( h_filter );
    thrust::device_vector<float2> idata = h_idata;
    thrust::device_vector<float> odata(signal_length * n_signals * 2);
    thrust::device_vector<float> filt = h_filter;

    processing::convolve_hilbert(idata, filt, odata, n_signals);
    processing::convolve_hilbert(h_idata, h_filter, h_odata, n_signals);

    thrust::host_vector<float> gpu = odata;
    compare(gpu, h_odata, filter_length);
}
// Convolve tests
TEST(convolve, test_convolve_hilbert_against_reference){
    test_convolve_hilbert(123456, 301);
}
TEST(convolve, test_convolve_hilbert_multiple_signals_against_reference){
    test_convolve_hilbert(12345, 301, 3);
}

/**
 * @brief Tests if the mat_vec_mult function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param length Length of the matrix
 * @param height Height of the matrix
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
template<typename T>
void test_mat_vec_mult(
  std::size_t length,
  std::size_t height,
  float mean = 0,
  float sigma=10)
{
    // Allocate
    thrust::host_vector<float> h_vector(length);
    thrust::host_vector<T> h_matrix(length * height);
    thrust::host_vector<T> h_odata(length * height);
    thrust::host_vector<T> alpha(height, {1,1});
    signal::random(h_vector, mean, sigma);
    signal::random(h_matrix, mean, sigma);
    thrust::device_vector<float> vector = h_vector;
    thrust::device_vector<T> d_alpha = alpha;
    thrust::device_vector<T> matrix = h_matrix;
    thrust::device_vector<T> odata(length * height);

    processing::mat_vec_mult( matrix, vector, odata, d_alpha );
    processing::mat_vec_mult( h_matrix, h_vector, h_odata, alpha );

    thrust::host_vector<float2> gpu = odata;
    compare(gpu, h_odata);
}
// Elementwise matrix-vector multiplication tests
// TEST(mat_vec_mult, test_mat_vec_mult_real_against_reference){
//     test_mat_vec_mult<float>(12345, 12);
// }
// TEST(mat_vec_mult, test_mat_vec_mult_cplx_against_reference){
//     test_mat_vec_mult<float2>(12345, 12);
// }


/**
 * @brief Testing configuration (only used for ResampleTester)
 */
struct resample_t{
    std::size_t fs_in;
    std::size_t fs_out;
    std::size_t nsamples;
    std::size_t fir_mult;
};

// std::size_t test_run = 0;
/**
 * @brief Class for parameterized testing of the resampler implementations
 */
class ResampleTester : public ::testing::TestWithParam<resample_t>
{
protected:
  void SetUp(){};
  void TearDown(){};
public:
    ResampleTester() : ::testing::TestWithParam<resample_t>(),
        _conf(GetParam()),
        _fs_in(_conf.fs_in),
        _fs_out(_conf.fs_out),
        _nsamples(_conf.nsamples),
        _fir_mult(_conf.fir_mult)
    {
        std::size_t lcm = std::lcm(_fs_in, _fs_out);
        _up = lcm / _fs_in;
        _down = lcm / _fs_out;
        std::size_t max_rate = (_up > _down) ? _up : _down;
        lp.f_cutoff = 1.0 / max_rate;
        lp.ntaps = max_rate * _fir_mult;
        if(lp.ntaps % 2 == 0){lp.ntaps--;}
        std::cout << "up: " << _up << " down: " << _down << " Ftaps: " << lp.ntaps << std::endl;
    }

    /**
     * @brief Test the polyphase resampler against the conventional resampler on CPU
     *
     * @tparam VectorType Type of in- and output
     * @tparam FilterType Type of the FIR filter
     */
    template<typename VectorType, typename FilterType>
    void test_poly_resample_against_resample()
    {
        // Code duplication due to GTest limitation - type and value parameterized tests
        thrust::host_vector<VectorType> idata(_nsamples);
        thrust::host_vector<FilterType> filter(lp.ntaps);
        thrust::host_vector<VectorType> odata_gold(_nsamples * _up / _down);
        thrust::host_vector<VectorType> odata_test(_nsamples * _up / _down);
        signal::random<VectorType>(idata, 0, 10);
        filter::firwin( filter , lp);
        // Run the polyphase resampler on the CPU
        processing::poly_resample(idata, filter, odata_test, _up, _down);
        // Run the naive resmapling implementation
        thrust::host_vector<VectorType> up_buffer(_nsamples * _up);
        thrust::host_vector<VectorType> fl_buffer(_nsamples * _up);
        processing::upsample(idata, up_buffer, _up);
        processing::convolve(up_buffer, filter, fl_buffer);
        processing::downsample(fl_buffer, odata_gold, _down);
        // processing::resample(idata, filter, odata_gold, _up, _down);
        compare(odata_test, odata_gold, (filter.size()-1) / 2);
    }

    /**
     * @brief Test the polyphase resampler GPU against the conventional resampler on CPU
     *
     * @tparam VectorType Type of in- and output
     * @tparam FilterType Type of the FIR filter
     */
    template<typename VectorType, typename FilterType>
    void test_poly_resample_against_resample_gpu()
    {
        // Code duplication due to GTest limitation - type and value parameterized tests
        thrust::host_vector<VectorType> idata(_nsamples);
        thrust::host_vector<FilterType> filter(lp.ntaps);
        thrust::host_vector<VectorType> odata_gold(_nsamples * _up / _down);
        thrust::host_vector<VectorType> odata_test(_nsamples * _up / _down);
        signal::random<VectorType>(idata, 0, 10);
        filter::firwin( filter , lp);
        // Allocate GPU buffers
        thrust::device_vector<VectorType> d_idata = idata;
        thrust::device_vector<FilterType> d_filter = filter;
        thrust::device_vector<VectorType> d_odata = odata_gold;
        // Run the polyphase resampler on the CPU
        processing::poly_resample(d_idata, d_filter, d_odata, _up, _down);
        // Run the naive resmapling implementation
        processing::resample(idata, filter, odata_gold, _up, _down);
        odata_test = d_odata;
        compare(odata_test, odata_gold, (filter.size()-1) / 2);
    }

    /**
     * @brief Test the conventional resampler GPU against the conventional resampler on GPU
     *
     * @tparam VectorType Type of in- and output
     * @tparam FilterType Type of the FIR filter
     */
    template<typename VectorType, typename FilterType>
    void test_resample_gpu()
    {
        // Code duplication due to GTest limitation - type and value parameterized tests
        thrust::host_vector<VectorType> idata(_nsamples);
        thrust::host_vector<FilterType> filter(lp.ntaps);
        thrust::host_vector<VectorType> odata_gold(_nsamples * _up / _down);
        thrust::host_vector<VectorType> odata_test(_nsamples * _up / _down);
        signal::random<VectorType>(idata, 0, 10);
        filter::firwin( filter , lp);
        // Allocate GPU buffers
        thrust::device_vector<VectorType> d_idata = idata;
        thrust::device_vector<FilterType> d_filter = filter;
        thrust::device_vector<VectorType> d_odata = odata_gold;
        // Run the naive resampler on the GPU
        processing::resample(d_idata, d_filter, d_odata, _up, _down);
        // Run the naive resampler on the CPU
        processing::resample(idata, filter, odata_gold, _up, _down);
        odata_test = d_odata;
        compare(odata_test, odata_gold);
    }

    /**
     * @brief Test the polyphase resampler GPU against the polyphase resampler on CPU
     *
     * @tparam VectorType Type of in- and output
     * @tparam FilterType Type of the FIR filter
     */
    template<typename VectorType, typename FilterType>
    void test_poly_resample_gpu()
    {
        // Code duplication due to GTest limitation - type and value parameterized tests
        thrust::host_vector<VectorType> idata(_nsamples);
        thrust::host_vector<FilterType> filter(lp.ntaps);
        thrust::host_vector<VectorType> odata_gold(_nsamples * _up / _down);
        thrust::host_vector<VectorType> odata_test(_nsamples * _up / _down);
        signal::random<VectorType>(idata, 0, 10);
        filter::firwin( filter , lp);
        // Allocate GPU buffers
        thrust::device_vector<VectorType> d_idata = idata;
        thrust::device_vector<FilterType> d_filter = filter;
        thrust::device_vector<VectorType> d_odata = odata_gold;
        // Run reference implementation on CPU
        processing::poly_resample(idata, filter, odata_gold, _up, _down);
        // Run GPU implementation
        processing::poly_resample(d_idata, d_filter, d_odata, _up, _down);
        // Transfer output from device to host
        odata_test = d_odata;
        compare(odata_test, odata_gold, (filter.size()-1) / 2);
    }

     /**
     * @brief Test the mix_poly_resample on the GPU against the mat_vec + poly_resample on CPU
     */
    void test_mix_poly_resample_gpu(float abs_tol=0.5, float rel_tol=0.01)
    {
        // Create vectors
        std::size_t nsignals = 1;
        thrust::host_vector<float>h_lo(nsignals);
        thrust::device_vector<float> idata(_nsamples);
        thrust::device_vector<float2> d_odata_test(_nsamples * _up / _down * nsignals);
        thrust::host_vector<float> filter(lp.ntaps);
        thrust::host_vector<float2> odata_gold(_nsamples * _up / _down * nsignals);
        thrust::host_vector<float2> lo_buffer(_nsamples * nsignals);
        thrust::host_vector<float2> mx_buffer(_nsamples * nsignals);
        thrust::host_vector<float2> alpha(nsignals, {1,0});
        thrust::host_vector<float2> odata_test(_nsamples * _up / _down * nsignals);
        // thrust::fill(idata.begin(), idata.end(), 1);
        // thrust::fill(filter.begin(), filter.end(), 1);
        signal::random<float>(idata, 0, 2000);
        signal::random<float>(h_lo, 0, _fs_in / 2);
        filter::firwin(filter , lp);

        // Run the naive resampler on the CPU
        thrust::host_vector<float> h_idata = idata;
        std::cout << "Running reference " << std::endl;
        signal::oscillate<double>(lo_buffer, h_lo, _conf.fs_in);
        processing::mat_vec_mult(lo_buffer, h_idata, mx_buffer, alpha);
        processing::poly_resample(mx_buffer, filter, odata_gold, _up, _down);

        // Run the naive resampler on the GPU
        thrust::device_vector<float> dlo = h_lo;
        thrust::device_vector<float2> dlo_buffer = lo_buffer;
        std::cout << "Running mix_poly_resample " << std::endl;
        filter::reshape_poly_fir(filter, _up, _down, WARP_SIZE);
        thrust::device_vector<float> d_filter = filter;
        processing::mix_poly_resample(idata, d_filter, dlo, dlo_buffer,
            d_odata_test, _up, _down, lp.ntaps, 0);
        odata_test = d_odata_test;
        // tools::save_vector<float2>(odata_test, "/beegfsEDD/test"+std::to_string(test_run)+".dat");
        // tools::save_vector<float2>(odata_gold, "/beegfsEDD/gold"+std::to_string(test_run)+".dat");
        // test_run += 1;
        compare(odata_test, odata_gold, 10, abs_tol, rel_tol);
    }


private:
    resample_t _conf;
    filter::lowpass_t lp;
    std::size_t _fs_in;
    std::size_t _fs_out;
    std::size_t _fir_mult;
    std::size_t _nsamples;
    std::size_t _up;
    std::size_t _down;
};


/*---------------------------------------- */
/*  Parameterized tests for ResampleTester */
/*---------------------------------------- */
/**
 * @brief Construct a new TEST_P using the conventional resampler on the GPU
 *        Output products are compared against the same algorithm on the CPU
 */
// TEST_P(ResampleTester, TestConventionalResamplerDeviceFloat2){
//   std::cout << std::endl
//     << "-------------------------------------------------------------------" << std::endl
//     << " Testing conventional resampler against convential on CPU (float2)" << std::endl
//     << "-------------------------------------------------------------------" << std::endl;
//   test_resample_gpu<float2, float>();
// }

/**
 * @brief Construct a new TEST_P using the conventional resampler on the GPU
 *        Output products are compared against the same algorithm on the CPU
 */
// TEST_P(ResampleTester, TestConventionalResamplerDeviceFloat){
//   std::cout << std::endl
//     << "------------------------------------------------------------------" << std::endl
//     << " Testing conventional resampler against convential on CPU (float) " << std::endl
//     << "------------------------------------------------------------------" << std::endl;
//   test_resample_gpu<float, float>();
// }

// /**
//  * @brief Construct a new TEST_P using the polyphase resampler on the CPU
//  *        Output products are compared against the convential algorithm on the CPU
//  */
// TEST_P(ResampleTester, TestPolyResamplerHostFloat2){
//   std::cout << std::endl
//     << "-------------------------------------------------------------------" << std::endl
//     << " Testing polyphase resampler against convential on CPU (float2)" << std::endl
//     << "-------------------------------------------------------------------" << std::endl;
//   test_poly_resample_against_resample<float2, float>();
// }

// /**
//  * @brief Construct a new TEST_P using the polyphase resampler on the CPU
//  *        Output products are compared against the convential algorithm on the CPU
//  */
// TEST_P(ResampleTester, TestPolyResamplerHostFloat){
//   std::cout << std::endl
//     << "---------------------------------------------------------------" << std::endl
//     << " Testing polyphase resampler against convential on CPU (float) " << std::endl
//     << "---------------------------------------------------------------" << std::endl;
//   test_poly_resample_against_resample<float, float>();
// }

// /**
//  * @brief Construct a new TEST_P using the polyphase resampler on the GPU
//  *        Output products are compared against the convential algorithm on the CPU
//  */
// TEST_P(ResampleTester, TestPolyResamplerDeviceAgainstConventionalFloat2){
//   std::cout << std::endl
//     << "-----------------------------------------" << std::endl
//     << " Testing polyphase resampler GPU (float2)" << std::endl
//     << "-----------------------------------------" << std::endl;
//   test_poly_resample_against_resample_gpu<float2, float>();
// }

// /**
//  * @brief Construct a new TEST_P using the polyphase resampler on the GPU
//  *        Output products are compared against the convential algorithm on the CPU
//  */
// TEST_P(ResampleTester, TestPolyResamplerDeviceAgainstConventionalFloat){
//   std::cout << std::endl
//     << "-----------------------------------------" << std::endl
//     << " Testing polyphase resampler GPU (float) " << std::endl
//     << "-----------------------------------------" << std::endl;
//   test_poly_resample_against_resample_gpu<float, float>();
// }

// /**
//  * @brief Construct a new TEST_P using the polyphase resampler on the GPU
//  *        Output products are compared against the polyphase algorithm on the CPU
//  */
// TEST_P(ResampleTester, TestPolyResamplerDeviceFloat2){
//   std::cout << std::endl
//     << "-----------------------------------------------------------" << std::endl
//     << " Testing poly_resample()  GPU against poly_resample()  CPU " << std::endl
//     << "-----------------------------------------------------------" << std::endl;
//   test_poly_resample_gpu<float2, float>();
// }

// /**
//  * @brief Construct a new TEST_P using the polyphase resampler on the GPU
//  *        Output products are compared against the polyphase algorithm on the CPU
//  */
// TEST_P(ResampleTester, TestPolyResamplerDeviceFloat){
//   std::cout << std::endl
//     << "-----------------------------------------------------------" << std::endl
//     << " Testing poly_resample()  GPU against poly_resample()  CPU " << std::endl
//     << "-----------------------------------------------------------" << std::endl;
//   test_poly_resample_gpu<float, float>();
// }

TEST_P(ResampleTester, TestMixPolyResamplerDeviceFloat){
  std::cout << std::endl
    << "---------------------------------------------------------------------------------- " << std::endl
    << " Testing mix_poly_resample() GPU against mat_vec() + poly_resample() CPU (T=float) " << std::endl
    << "---------------------------------------------------------------------------------- " << std::endl;
  test_mix_poly_resample_gpu(0.1, 0.001);
}


INSTANTIATE_TEST_SUITE_P(ResampleTesterInstantiation, ResampleTester, ::testing::Values(
    resample_t{750, 300, 750, 5},
    resample_t{32000, 500, 32000, 2},
    resample_t{7, 4, 280, 3},
    resample_t{11, 6, 660, 4},
    resample_t{7, 5, 350, 5},
    resample_t{7, 5, 350, 3},
    resample_t{9, 4, 360, 3},
    resample_t{4, 3, 24, 7},
    resample_t{3, 2, 24, 4},
    resample_t{550, 100, 2200, 10},
    resample_t{500, 100, 2000, 10},
    resample_t{650, 100, 13000, 5},
    resample_t{40, 33, 1320, 5},
    resample_t{750, 300, 1500, 10},
    /** Reduce number of tests uncomment when developing*/
    // resample_t{40000000, 1600000, 20000000, 10},
    // resample_t{80000000, 1600000, 20000000, 10},
    // resample_t{160000000, 1600000, 20000000, 11},
    // resample_t{320000000, 1600000, 32000000, 10},
    // resample_t{400000000, 1600000, 400000000, 1},
    // resample_t{40000000, 1600000, 20000000, 15},
    // resample_t{100000000, 1600000, 20000000, 10},
    resample_t{32000, 500, 320000, 1},
    resample_t{32000, 500, 320000, 10},
    resample_t{48000, 16000, 300000, 20},
    resample_t{3200, 75,  32000, 10},
    resample_t{3200, 750, 32000, 10}
));

}
}
}
}