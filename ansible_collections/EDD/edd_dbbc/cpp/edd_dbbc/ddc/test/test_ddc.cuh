#include <gtest/gtest.h>
#include <filesystem>
#include <cstdlib>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include "edd_dbbc/testing_tools.cuh"
#include "edd_dbbc/ddc/ddc.cuh"

namespace edd_dbbc {
namespace ddc {
namespace test {
// int test_run = 0;

/**
 * @brief A class for testing the implemented downconverters against
 *        a python implementation. The python implementation is considered
 *        to be the golden sandard. Inherits from ::testing::TestWithParam
 */
class DDCTester : public ::testing::TestWithParam<ddc_t>
{
protected:
  void SetUp(){};
  void TearDown(){};

public:
/**
 * @brief Construct a new DDCTester object
 *
 */
    DDCTester() : ::testing::TestWithParam<ddc_t>(),
        _conf(GetParam())
    {
        h_input.resize(_conf.input_size);
        gold_output.resize(h_input.size()
            * _conf.up()
            / _conf.down()
            * _conf.nsignals() * 2
        );
        test_output.resize(gold_output.size());
        signal::random(h_input, 0, variance);
        // signal::chirp(h_input, _conf.fs_in, 1, _conf.fs_in/2);
    }

    /**
     * @brief Destroy the DDCTester object
     *
     */
    ~DDCTester(){}


    /**
     * @brief Run the python implementation by exchanging the input, filter
     *        and output data.
     */
    void reference()
    {
        std::string script_path = __FILE__;
        script_path.replace(script_path.end()-3, script_path.end(), "py");
        std::filesystem::path current_path = std::filesystem::current_path();
        std::filesystem::path ifile = current_path / "tmp_input.dat";
        std::filesystem::path ofile = current_path / "tmp_output.dat";
        std::filesystem::path lfile = current_path / "tmp_lowpass.dat";

        tools::save_vector(h_input, "tmp_input.dat");
        if (!std::filesystem::exists(script_path) || !std::filesystem::exists(ifile) || !std::filesystem::exists(lfile)) {
            std::cerr << "Python script or input file not found." << std::endl;
        }
        std::string cmd = "python " + script_path
            + " -i " + ifile.string()
            + " -o " + ofile.string()
            + " -l " + lfile.string()
            + " -s " + std::to_string(_conf.fs_in)
            + " -c " + std::to_string(_conf.fs_dw)
            + " -lo " + _conf.f_lo
            + " -n " + std::to_string(_conf.input_size);
        std::cout << "Python command: " << cmd << std::endl;
        int res = std::system(cmd.c_str());

        if(res == 0 && std::filesystem::exists(ofile)){
            tools::load_vector(gold_output, ofile.string());
            std::filesystem::remove(ifile);
            std::filesystem::remove(ofile);
            std::filesystem::remove(lfile);
        }
    }

    /**
     * @brief Run the DDC tests with different ProcessorTypes
     *
     * @tparam ProcessorType defines the used processor
     *         (e.g. DDC, PolyphaseDownConverter, FourierDownConverter)
     * @tparam RealVectorType The input type, either thrust::host_vector<T> or thrust::device_vector<T>
     * @tparam CplxVectorType The output type, either thrust::host_vector<T> or thrust::device_vector<T>
     */
    template<typename ProcessorType, typename RealVectorType, typename CplxVectorType>
    void run_test()
    {
        std::size_t signal_length = gold_output.size() / (_conf.nsignals()*2);
        std::size_t edge = _conf.down()*_conf.lowpass.mult/2;
        std::filesystem::path current_path = std::filesystem::current_path();
        std::filesystem::path lfile = current_path / "tmp_lowpass.dat";
        RealVectorType input = h_input;
        RealVectorType output(test_output.size());
        ProcessorType processor(_conf);

        thrust::host_vector<float> fir_ref(processor.lowpass_conf().ntaps);
        filter::firwin(fir_ref, processor.lowpass_conf());
        tools::save_vector(fir_ref, lfile.string());

        processor.allocate();
        processor.process(input, output);
        test_output = output;
        this->reference();

        // tools::save_vector<float>(gold_output, "/beegfsEDD/gold"+std::to_string(test_run)+".dat");
        // tools::save_vector<float>(test_output, "/beegfsEDD/test"+std::to_string(test_run)+".dat");
        // test_run++;
        for(std::size_t n = 0; n < _conf.nsignals() * 2; n++){
          for(std::size_t i = 0; i < signal_length; i++){
              // Don't compare edge bins, bc the deviation is to large
              if(i < edge || i + edge > signal_length){continue;}
              ASSERT_NEAR(test_output[n * signal_length + i],
                          gold_output[n * signal_length + i],
                          0.1 + std::fabs(gold_output[n * signal_length + i]) * 0.001
              ) << " Position " << n * signal_length + i;
          }
        }
    }

private:
    ddc_t _conf;
    std::size_t variance = 2000;
    thrust::host_vector<float> h_input;
    thrust::host_vector<float> gold_output;
    thrust::host_vector<float> test_output;
};


// Define the different processor types
using DDCHostType = ddc::DDC<thrust::host_vector<float>, thrust::host_vector<float2>>;
using DDCDeviceType = ddc::DDC<thrust::device_vector<float>, thrust::device_vector<float2>>;
using PolyHostType = ddc::PolyphaseDownConverter<thrust::host_vector<float>, thrust::host_vector<float2>>;
using PolyDeviceType = ddc::PolyphaseDownConverter<thrust::device_vector<float>, thrust::device_vector<float2>>;
using FourierHostType = ddc::FourierDownConverter<thrust::host_vector<float>, thrust::host_vector<float2>>;
using FourierDeviceType = ddc::FourierDownConverter<thrust::device_vector<float>, thrust::device_vector<float2>>;


/**
 * @brief Construct a new TEST_P using the DDC on the GPU
 *        Output products are compared against a python implementation
 */
// TEST_P(DDCTester, TestDDCDevice){
//   std::cout << std::endl
//     << "-------------------------------" << std::endl
//     << " Testing convential DDC on GPU " << std::endl
//     << "-------------------------------" << std::endl;
//   run_test<DDCDeviceType, thrust::device_vector<float>, thrust::device_vector<float2>>();
// }
/**
 * @brief Construct a new TEST_P using the PolyphaseDownConverter on the GPU
 *        Output products are compared against a python implementation
 */
TEST_P(DDCTester, TestPolyphaseDownConverterDevice){
  std::cout << std::endl
    << "---------------------------------------" << std::endl
    << " Testing PolyphaseDownConverter on GPU " << std::endl
    << "---------------------------------------" << std::endl;
  run_test<PolyDeviceType, thrust::device_vector<float>, thrust::device_vector<float2>>();
}
/**
 * @brief Construct a new INSTANTIATE_TEST_SUITE_P object
 *        Parameterize the defined TEST_P objects
 */
INSTANTIATE_TEST_SUITE_P(DDCTesterInstantiation, DDCTester, ::testing::Values(
    ddc_t{6000, 750, 500, "300", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                 // 0
    ddc_t{640000, 32000, 750, "1000", false, {301, 0.05, 0.05, 0.45}, {0, 10, "rect", 0}},                // 1
    ddc_t{64000, 3200, 750, "1000", false, {301, 0.05, 0.05, 0.45}, {0, 1, "hamming", 0}},               // 2
    ddc_t{3500, 700, 500, "8000", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},               // 3
    ddc_t{132000, 40, 33, "15", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                   // 4
    ddc_t{32000, 32000, 4000, "8000", false, {301, 0.05, 0.05, 0.45}, {0, 8, "hamming", 0}},             // 5
    ddc_t{110000, 55000, 10000, "4300", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},           // 6
    ddc_t{30000, 250, 150, "75", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                 // 7
    ddc_t{7500,  250, 150, "1234,245,123,222", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},   // 8
    ddc_t{1320, 12, 11, "999", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                  // 9
    ddc_t{32000, 32000, 2000, "0,2000,4000,6000", false, {301, 0.05, 0.05, 0.45}, {0, 7, "hamming", 0}},  // 10
    // ddc_t{40960000, 4096000000, 16000000, "238000000", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},  // 11
    // ddc_t{409600000, 1024000000, 1000000, "256000000", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},  // 11
    ddc_t{1280000, 32000, 2000, "6000,6000", false, {301, 0.05, 0.05, 0.45}, {0, 12, "hamming", 0}}
));

}
}
}