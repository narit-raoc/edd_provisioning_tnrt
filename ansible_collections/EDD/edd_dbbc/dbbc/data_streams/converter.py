
import numpy as np
from mpikat.core.logger import getLogger
from mpikat.utils.data_streams import RedisSpectrumStreamFormat, serialize_array
from mpikat.utils.spead_capture import SpeadPacket
from dbbc.data_streams import DDCDataStreamFormat

_LOG = getLogger("mpikat.dbbc.data_streams.converter")

class Converter():
    """The Converter-class converts a stream of the format DDCDataStreamFormat to
        a RedisSpectrumStreamFormat.
    """

    def __init__(self, sname: str= "", conf: dict = None, naccumulate: int=1024):
        """Constructor

        Args:
            sname (str, optional): Stream name. Defaults to "".
            conf (dict, optional): Configuration of the DDC stream. Defaults to None.
            naccumulate (int, optional): number of FFT-accumulation. Defaults to 1024.
        """
        if isinstance(conf, dict):
            self._conf = {}
            for key, val in conf.items():
                self._conf[key] = val
        else:
            self._conf = dict(DDCDataStreamFormat.stream_meta)
        self._sname: str = sname
        self._naccumulate:int = naccumulate
        self._call_cnt:int = 0
        self._first:int = 0
        self._last:int = 0
        self._trash:int = 0
        self._ostream = RedisSpectrumStreamFormat()
        self._nchannel:int = DDCDataStreamFormat.samples_per_heap // 2
        self._spectrum: np.ndarray = np.zeros(self._nchannel, dtype=self._conf["dtype"])
        # Set static data items
        self._ostream.data_items["f_min"] = self._conf["center_freq"] - self._conf["sample_rate"] / 4
        self._ostream.data_items["f_max"] = self._conf["center_freq"] + self._conf["sample_rate"] / 4
        self._ostream.data_items["interval"] = self._naccumulate * self._nchannel * 2 / self._conf["sample_rate"]
        self._ostream.data_items["name"] = self._sname

    def convert(self, heap: SpeadPacket) -> dict:
        """Converts the data by applying a FFT

        Args:
            heap (SpeadPacket): the heap

        Returns:
            dict: returns initialized data_items of the RedisSpectrumStreamFormat
        """
        self._call_cnt += 1
        current = heap.timestamp
        if self._call_cnt == 1:
            # Only start a new spectrum if the current timestamp is greater
            # than the last timestamp from the previous spectrum
            # Check if commented statement handles timestamp overroll
            #   (e.g. 'and not (self._last > 2**32 and current < 2**16)')
            if current < self._last:
                self._call_cnt -= 1; return None
            self._first = current
            self._last = current + self._naccumulate * DDCDataStreamFormat.samples_per_heap
        if current >= self._first and current <= self._last:
            self._spectrum += np.abs(np.fft.rfft(heap.data)[1:])**2
        else:
            self._trash += 1
        if self._call_cnt == self._naccumulate:
            spectrum = 10*np.log10(self._spectrum / (self._call_cnt - self._trash))
            self._ostream.data_items["timestamp"] = self._conf["sync_time"] + self._first / self._conf["sample_rate"]
            self._ostream.data_items["data"] = serialize_array(spectrum.astype(self._conf["dtype"]))
            self._spectrum.fill(0)
            self._call_cnt = 0
            self._trash = 0
            return self._ostream.data_items
        return None
