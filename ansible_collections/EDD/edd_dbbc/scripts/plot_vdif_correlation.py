"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

This script reads VDIF files and correlates them.
"""
import argparse
from argparse import RawTextHelpFormatter

import math
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as ss

from dbbc import vdif

def parse_comma_seperated(s: str, size: int):
    l = []
    if s != "":
        l = [int(ts) for ts in s.split(",")]
        if len(l) == 1:
            return int(l[0])
        assert(len(l) == size), "The number of list items does not match the expected size"
        return l
    return s


def normalize(arr: np.ndarray) -> np.ndarray:
    """_summary_

    Args:
        arr (np.ndarray): _description_

    Returns:
        np.ndarray: _description_
    """
    mean = np.mean(arr)
    arr = (arr - mean)
    sigma = np.std(arr)
    return arr / sigma

def main_cli():
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('--input_files', '-i', action='store', dest="ifiles",
                        help="The input files containing VDIF data. Files are seperated by ','. "
                        + "Add '+offset' after the file name for reading after offset bytes. "
                        + "If a single file is passed, channels are correlated against each other")
    parser.add_argument('--nframes', '-n', action='store', default=-1, dest="frames",
                        help="Number of frames to read")
    parser.add_argument('--center_freq', '-f', action='store', default=-1, dest="fc",
                        help="Center freuquency of the channel")
    parser.add_argument('--title', '-t', action='store', default="No Title", dest="title",
                        help="Title of the plot")
    parser.add_argument('--start', '-ts', action='store', default="", dest="start",
                        help="Start processing after seconds since epoch is reached. "
                        + "Seperate with ',' to start file reading with different epochs.")
    parser.add_argument('--channel', '-c', action='store', default="", dest="channel",
                        help="The channels to correlate. Either a single integer value or a ','-"
                        + "seperated string which maps to the number of channels. "
                        + "If not set all channels are correlated against each other")
    parser.add_argument('--auto', '-a', action='store_true', dest="autocorr",
                        help="If set, also applies the auto correlation")
    # Assign arguments to variables
    ifiles = str(parser.parse_args().ifiles).split(',')
    frames = int(parser.parse_args().frames)
    fc = int(parser.parse_args().fc)
    autocorr = parser.parse_args().autocorr
    title = parser.parse_args().title

    start = parse_comma_seperated(str(parser.parse_args().start), len(ifiles))
    channel = parse_comma_seperated(str(parser.parse_args().channel), len(ifiles))
    # Read in the data
    time_data = []
    invalid_frames = np.zeros(len(ifiles), dtype=np.int64)
    for i, fname in enumerate(ifiles):
        # Get the offset
        if "+" in fname:
            fname, offset = fname.split('+')
        else:
            offset = 0
        # Get the epoch start value
        if isinstance(start, list):
            epoch = start[i]
        elif isinstance(start, str):
            epoch = 0
        else:
            epoch = start
        # Process the given file
        with open(fname, mode="rb") as file:
            reader = vdif.Reader(file, offset, frames)
            if epoch != 0:
                reader.seek_frame(epoch)
            print(f"Start timestamp {reader.header.timestamp}\n")
            nchan = reader.header.nchannel
            reader.frames *= nchan
            bat_nsamples = reader.header.nsample
            tot_nsamples = reader.frames * bat_nsamples
            time_domain = np.zeros((nchan, tot_nsamples), dtype=np.float32)

            for ii, (header, payload) in enumerate(reader):

                # Do not process invalid heaps
                if header.invalid:
                    invalid_frames[i] += 1
                    continue

                time_domain[:,ii*bat_nsamples:(ii+1)*bat_nsamples] = reader.payload.data
                # Inform the user about the progress
                if payload.update_cnt % 100 == 0:
                    print(f"Processed {payload.update_cnt}/{frames} frames\r", end="")
            time_data.append(time_domain)

    if channel != "":
        corr = []
        for i in range(len(time_data)):
            for ii in range(len(time_data)):
                if isinstance(channel, list):
                    lchan = channel[i]
                    rchan = channel[ii]
                else:
                    lchan = channel
                    rchan = channel
                if (not autocorr and i >= ii) or i > ii:
                    continue
                print(f"Correlating channels {lchan} and {rchan} of files {ifiles[i]} and {ifiles[ii]}")
                left = normalize(time_data[i])
                right = normalize(time_data[ii])
                corr.append(ss.correlate(left, right))
    else:
        corr = np.zeros((math.factorial(len(ifiles)) - int(~autocorr), tot_nsamples*2), dtype=np.float32)
        corr = []
        plot_titles = []
        for i in range(len(time_data)):
            for ii in range(len(time_data)):
                if (i == ii and i > 0) or i > ii:
                    continue
                for ichan in range(len(time_data[i])):
                    for iichan in range(len(time_data[ii])):
                        if (i == ii) and ichan < iichan:
                            continue
                        if len(time_data[i][ichan]) != len(time_data[ii][iichan]):
                            print("Signal length not equal, wont correlate")
                            continue
                        print(f"Correlating channels {ichan} and {iichan} of files {ifiles[i]} and {ifiles[ii]}")
                        left = normalize(time_data[i][ichan])
                        right = normalize(time_data[ii][iichan])
                        corr.append(ss.correlate(left, right))
                        plot_titles.append(f"Channels {ichan} and {iichan}; Files {i} and {ii}")
                        # plot_titles.append(f"Channels {ichan} and {iichan}; Files {ifiles[i].split('/')[-1]} and {ifiles[ii].split('/')[-1]}")

    fig = plt.figure(len(corr))
    for i in range(len(corr)):
        ax = fig.add_subplot(len(corr)//2, 2, i+1)
        ax.plot(corr[i])
        ax.set_title(plot_titles[i])
    # fig.tight_layout()
    plt.show()

if __name__ == "__main__":
    main_cli()
