#include <gtest/gtest.h>

#include "edd_dbbc/vdif/test/test_header.h"
#include "edd_dbbc/vdif/test/test_packer.cuh"
#include "edd_dbbc/vdif/test/test_pipeline.cuh"

int main(int argc, char **argv) {
 ::testing::InitGoogleTest(&argc, argv);

 return RUN_ALL_TESTS();
}
