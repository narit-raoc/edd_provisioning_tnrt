
"""
The data_streams module provides data stream descriptions which are added to the EDD system by
the edd_dbbc plugin.
"""

from mpikat.core.data_stream import DataStreamRegistry
from dbbc.data_streams.vdif_stream import VDIFDataStreamFormat
from dbbc.data_streams.ddc_stream import DDCDataStreamFormat, DDCSpeadHandler, ddc_mkrecv_header, ddc_mkrecv_cmd
from dbbc.data_streams.converter import Converter

__all__ = [
    "VDIFDataStreamFormat",
    "DDCDataStreamFormat",
    "ddc_mkrecv_header",
    "ddc_mkrecv_cmd",
    "DDCSpeadHandler",
    "Converter"
]

# Register data streams
DataStreamRegistry.register(VDIFDataStreamFormat)
DataStreamRegistry.register(DDCDataStreamFormat)
