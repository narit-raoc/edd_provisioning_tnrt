/**
 * @file    processing.h
 * @author  Niclas Esser (nesser@mpifr-bonn.mpg.de)
 * @brief   The processing.h file contains algorithms used to run digital downconversion.
 *          Device code is implemented in src/signal.cu
 * @version 0.1
 * @date    2024-04-25
 *
 */
#pragma once

#include <cuda.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include "edd_dbbc/cuerror.cuh"

namespace edd_dbbc {
namespace ddc {
namespace processing {

// GPU kernel defintion
namespace kernel {

__global__ void convolve(const float* idata, const float* lfilter, float* odata, unsigned filter_size, unsigned width);
__global__ void convolve(const float2* idata, const float* lfilter, float2* odata, unsigned filter_size, unsigned width);
__global__ void convolve_hilbert(const float2* idata, const float* lfilter, float* odata, unsigned filter_size, unsigned width);
template<typename T>__global__ void upsample(const T* idata, T* odata, unsigned up, unsigned width);
template<typename T>__global__ void downsample(const T* idata, T* odata, unsigned down, unsigned width);
template<typename T>__global__ void poly_resample(const T* idata, const T* lfilter, T* odata, unsigned up, unsigned down, unsigned width, unsigned ntaps);
template<typename T> __global__ void mat_vec_mult(T* matrix, const T* vector, unsigned width, unsigned height);
__global__ void mat_vec_mult(const float2* matrix, const float* vector, float2* odata, float2* alpha, unsigned width, unsigned height);
__global__ void poly_resample(const float2* idata, const float* lfilter, float2* odata, unsigned up, unsigned down, unsigned width, unsigned ntaps);
__global__ void mix_poly_resample(const float* idata, const float* filter, const float* f_lo, const float2* oscillations,
    float2* odata, const int up, const int down, const int width, const int height, const int half_fir, const int ntaps_pad, const float t0);

} // namespace kernel
// Constants
const unsigned N_SM = 512;
const unsigned N_THREADS = 1024;
const unsigned WARP_SIZE = 32;
const unsigned N_WARPS = 32;
const unsigned MAX_SMEM_SIZE = 49152;

__host__ __device__ float2 cmult(float2 a, float2 b, float c){
    float2 res;
    res.x = (a.x * b.x - a.y * b.y) * c;
    res.y = (a.x * b.y + a.y * b.x) * c;
    return res;
}


/*------------------------*/
/*  Convolution functions */
/*------------------------*/
/**
 * @brief Convolves input with filter of the same type using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional
 * @param filter The second signal - 1 dimension
 * @param output The convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve(
    const thrust::device_vector<float>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float>& output,
    std::size_t n_signals=1,
    cudaStream_t stream=NULL)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = filter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    kernel::convolve<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(filter.data()),
        thrust::raw_pointer_cast(output.data()),
        filter_length, signal_length
    );
}


/**
 * @brief Convolves input with filter of the same type using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional
 * @param filter The second signal - 1 dimension
 * @param output The convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve(
    const thrust::host_vector<float>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float>& output,
    std::size_t n_signals=1)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = lfilter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    for (std::size_t n = 0; n < n_signals; n++){
        for (std::size_t oidx = 0; oidx < signal_length; oidx++){
            output[n * signal_length + oidx] = 0;
            for(std::size_t fidx = 0; fidx < lfilter.size(); fidx++){
                int iidx = oidx + fidx - (lfilter.size()-1) / 2;
                if(iidx >= 0 && iidx < (int)signal_length){
                    output[n * signal_length + oidx] += input[n * signal_length + iidx] * lfilter[fidx];
                }
            }
        }
    }
}

/**
 * @brief Complex-real convolution using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param filter The second signal - 1 dimension of real type
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float2>& output,
    std::size_t n_signals=1,
    cudaStream_t stream=NULL)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = filter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    kernel::convolve<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(filter.data()),
        thrust::raw_pointer_cast(output.data()),
        filter.size(), signal_length
    );
}


/**
 * @brief Complex-real convolution using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param filter The second signal - 1 dimension of real type
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    std::size_t n_signals=1)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = lfilter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    for (std::size_t n = 0; n < n_signals; n++){
        for (std::size_t oidx = 0; oidx < signal_length; oidx++){
            output[n * signal_length + oidx] = {0,0};
            for(std::size_t fidx = 0; fidx < lfilter.size(); fidx++){
                int iidx = oidx + fidx - (lfilter.size()-1) / 2;
                if(iidx >= 0 && iidx < (int)(signal_length)){
                    output[n * signal_length + oidx].x += input[n * signal_length + iidx].x * lfilter[fidx];
                    output[n * signal_length + oidx].y += input[n * signal_length + iidx].y * lfilter[fidx];
                }
            }
        }
    }
}


/**
 * @brief Complex-real convolution using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param bfilter The hilbert bandpass filter
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve_hilbert(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& bfilter,
    thrust::device_vector<float>& output,
    std::size_t n_signals=1,
    cudaStream_t stream=NULL)
{
    assert((input.size() / n_signals) % 1 == 0);
    // assert(input.size() * 2 == output.size());

    std::size_t signal_length = input.size() / n_signals;
    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    unsigned s_memory = bfilter.size() * sizeof(float);

    kernel::convolve_hilbert<<<grid, 1024, s_memory, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(bfilter.data()),
        thrust::raw_pointer_cast(output.data()),
        bfilter.size(), signal_length
    );
}

/**
 * @brief Complex-real convolution using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param bfilter The hilbert bandpass filter
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve_hilbert(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& bfilter,
    thrust::host_vector<float>& output,
    std::size_t n_signals=1)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = bfilter.size();

    assert(signal_length >= filter_length);
    assert(input.size() * 2 == output.size());

    for(std::size_t n = 0; n < n_signals; n++){
        for (std::size_t i = 0; i < signal_length; i++){
            float2 tmp = {.0,.0};
            for (std::size_t j = 0; j < filter_length; j++){
                int iidx = i - j + (filter_length - 1) / 2;
                if (iidx >= 0 && iidx < (int)(signal_length)){
                    tmp.x += input[n * signal_length + iidx].x * bfilter[j];
                    tmp.y += input[n * signal_length + iidx].y * bfilter[filter_length - 1 - j];
                }
            }
            output[n * signal_length * 2 + i] = tmp.x + tmp.y;
            output[n * signal_length * 2 + i + signal_length] = tmp.x - tmp.y;
        }
    }
}


/*--------------------------*/
/*  Up/Downsample functions */
/*--------------------------*/
/**
 * @brief Upsamples a signal by zero filling on the GPU
 *
 * @param input The input signal to upsample
 * @param output The upsampled output signal
 * @param up The upsample factor
 */
template<typename T>
void upsample(
    const thrust::device_vector<T>& input,
    thrust::device_vector<T>& output,
    std::size_t up,
    std::size_t n_signals = 1,
    cudaStream_t stream=NULL)
{
    assert(input.size() * up == output.size());
    kernel::upsample<<<input.size() / 1024 + 1, 1024, 0>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(output.data()),
        up, input.size()
    );
}


/**
 * @brief Upsamples a signal by zero filling on the CPU
 *
 * @param input The input signal to upsample
 * @param output The upsampled output signal
 * @param up The upsample factor
 */
template<typename T>
void upsample(
    const thrust::host_vector<T>& input,
    thrust::host_vector<T>& output,
    std::size_t up,
    std::size_t n_signals = 1)
{
    assert(input.size() * up == output.size());
    std::size_t isignal_length = input.size() / n_signals;
    std::size_t osignal_length = output.size() / n_signals;
    for(std::size_t n = 0; n < n_signals; n++){
        for(std::size_t i = 0; i < isignal_length; i++){
            output[n * osignal_length + i * up] = input[n * osignal_length + i];
        }
    }
}


/**
 * @brief Downsamples a signal by skipping on the GPU
 *
 * @param input The input signal to downsample
 * @param output The downsampled output signal
 * @param up The downsample factor
 */
template<typename T>
void downsample(
    const thrust::device_vector<T>& input,
    thrust::device_vector<T>& output,
    std::size_t down,
    std::size_t n_signals = 1,
    cudaStream_t stream=NULL)
{
    assert(input.size() / down == output.size());
    kernel::downsample<<<output.size() / 1024 + 1, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(output.data()),
        down, output.size()
    );
}


/**
 * @brief Downsamples a signal by skipping on the GPU
 *
 * @param input The input signal to downsample
 * @param output The downsampled output signal
 * @param up The downsample factor
 */
template<typename T>
void downsample(
    const thrust::host_vector<T>& input,
    thrust::host_vector<T>& output,
    std::size_t down,
    std::size_t n_signals = 1)
{
    assert(input.size() / down == output.size());
    std::size_t isignal_length = input.size() / n_signals;
    std::size_t osignal_length = output.size() / n_signals;
    for(std::size_t n = 0; n < n_signals; n++){
        for(std::size_t i = 0; i < osignal_length; i++){
            output[n * osignal_length + i] = input[n * isignal_length + i * down];
        }
    }
}


/*---------------------*/
/*  Resample functions */
/*---------------------*/
/**
 * @brief Naive rational resampling
 *
 * @note This is a very naive and slow resampling approach. It will allocate temporary buffers
 *
 * @param input The complex input signal
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
template<typename VectorType, typename FilterType>
void resample(
    const VectorType& input,
    const FilterType& lfilter,
    VectorType& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1
)
{
    assert(output.size() == input.size() * up / down);
    VectorType upsampled(up*input.size());
    VectorType filtered(upsampled.size());
    processing::upsample(input, upsampled, up, n_signal);
    processing::convolve(upsampled, lfilter, filtered, n_signal);
    processing::downsample(filtered, output, down, n_signal);
}


/**
 * @brief Rational resampling using a polyphase filter on the CPU - faster and efficent
 *
 * @param input The complex input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
void poly_resample(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1)
{
    assert(output.size() == input.size() * up / down);
    std::size_t i_length =  input.size() / n_signal;
    std::size_t o_length = output.size() / n_signal;
    std::size_t half_fir = (lfilter.size() - 1) / 2;
    std::size_t rotate = up - down + up * down;
    for(std::size_t n = 0; n < n_signal; n++)
    {
        std::size_t o_offset = n * o_length;
        std::size_t i_offset = n * i_length;
        for(std::size_t oidx = 0; oidx < o_length; oidx++)
        {
            std::size_t poly_branch = (half_fir + rotate * oidx) % up;
            for(std::size_t fidx = poly_branch; fidx < lfilter.size(); fidx += up)
            {
                int iidx = (oidx * down + fidx - half_fir) / up;
                if (iidx >= 0 && iidx < (int)i_length){
                    output[o_offset + oidx].x += input[i_offset + iidx].x * lfilter[fidx];
                    output[o_offset + oidx].y += input[i_offset + iidx].y * lfilter[fidx];
                }
            }
        }
    }
}


/**
 * @brief Rational resampling using a polyphase filter on the CPU - faster and efficent
 *
 * @param input The real input signal - 1 or 2 dimensional
 * @param filter The real FIR filter
 * @param output The real output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
template<typename T>
void poly_resample(
    const thrust::host_vector<T>& input,
    const thrust::host_vector<T>& lfilter,
    thrust::host_vector<T>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1)
{
    assert(output.size() == input.size() * up / down);
    std::size_t i_length =  input.size() / n_signal;
    std::size_t o_length = output.size() / n_signal;
    std::size_t half_fir = (lfilter.size() - 1) / 2;
    std::size_t rotate = up - down + up * down;
    for(std::size_t n = 0; n < n_signal; n++){
        std::size_t o_offset = n * o_length;
        std::size_t i_offset = n * i_length;
        for(std::size_t oidx = 0; oidx < o_length; oidx++){
            std::size_t poly_branch = (half_fir + rotate * oidx) % up;
            for(std::size_t fidx = poly_branch; fidx < lfilter.size(); fidx += up){
                int iidx = (oidx * down + fidx - half_fir) / up;
                if (iidx >= 0 && iidx < (int)i_length){
                    output[o_offset + oidx] += input[i_offset + iidx] * lfilter[fidx];
                }
            }
        }
    }
}

/**
 * @brief Rational resampling using a polyphase filter on the GPU - faster and efficent
 *
 * @param input The complex input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
void poly_resample(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& lfilter,
    thrust::device_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1,
    cudaStream_t stream=NULL)
{
    dim3 grid;
    if(output.size() > (std::size_t)(N_SM / n_signal * N_THREADS)){
        grid.x = N_SM / n_signal;
    }else{
        grid.x = output.size() / N_THREADS + 1;
    }
    grid.y = n_signal;
    unsigned s_memory = lfilter.size() * sizeof(float);
    kernel::poly_resample<<<grid, N_THREADS, s_memory, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(lfilter.data()),
        thrust::raw_pointer_cast(output.data()),
        up, down, input.size() / n_signal, lfilter.size()
    );
}


/**
 * @brief Rational resampling using a polyphase filter on the GPU - faster and efficent
 *
 * @param input The real input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The real output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
template<typename T>
void poly_resample(
    const thrust::device_vector<T>& input,
    const thrust::device_vector<T>& lfilter,
    thrust::device_vector<T>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1,
    cudaStream_t stream=NULL)
{
    dim3 grid;
    grid.x = (N_SM * N_THREADS < input.size()) ? N_SM / n_signal : input.size() / N_THREADS + 1;
    grid.y = n_signal;
    unsigned s_memory = lfilter.size() * sizeof(T);
    kernel::poly_resample<<<grid, N_THREADS, s_memory, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(lfilter.data()),
        thrust::raw_pointer_cast(output.data()),
        up, down, input.size() / n_signal, lfilter.size()
    );
}


/**
 * @brief Rational resampling and mixing using a polyphase filter on the GPU - faster and efficent
 *
 * @param input The real input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The real output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
void mix_poly_resample(
    const thrust::device_vector<float>& input,
    const thrust::device_vector<float>& lfilter,
    const thrust::device_vector<float>& f_lo,
    const thrust::device_vector<float2>& oscillations,
    thrust::device_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t ntaps,
    double t0 = .0,
    cudaStream_t stream=NULL)
{
    int ntaps_pad = lfilter.size();
    int half_fir = (ntaps - 1) / 2;
    int height = f_lo.size();
    int nwarps = (ntaps_pad / (up * WARP_SIZE) < N_WARPS) ?
        ntaps_pad / (up * WARP_SIZE) : N_WARPS;
    unsigned s_memory =
        ntaps_pad * 2 * sizeof(float) // Input and filter data
        + WARP_SIZE * height * sizeof(float2) // Output data
        + WARP_SIZE * nwarps * sizeof(float2);// Store intermediate results
    dim3 grid, block;
    grid.x = ceil(output.size() / static_cast<float>(WARP_SIZE*height));
    block.x = WARP_SIZE * nwarps;

    assert_msg(ntaps_pad % WARP_SIZE == 0, "filter size is not aligned to WARP_SIZE (=32)");
    assert_msg((s_memory + WARP_SIZE * sizeof(float2)) < MAX_SMEM_SIZE, "Not enough requested shared memory");
    kernel::mix_poly_resample<<<grid, block, s_memory, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(lfilter.data()),
        thrust::raw_pointer_cast(f_lo.data()),
        thrust::raw_pointer_cast(oscillations.data()),
        thrust::raw_pointer_cast(output.data()),
        up, down, input.size(), height, half_fir, ntaps_pad, t0);
}


/**
 * @brief Elementwise multiplication of matrix and vector on the GPU
 *
 * @note Overloaded function: Either runs on host or GPU - depending on the thrust vectors
 *
 * @param matrix the input matrix
 * @param vector the input vector
 * @param output the output matrix
 * @param alpha  the scalar to multiply element with
 * @param stream the cudaStream_t used to process. Defaults to NULL.
 */
void mat_vec_mult(
    thrust::device_vector<float2>& matrix,
    thrust::device_vector<float>& vector,
    thrust::device_vector<float2>& output,
    thrust::device_vector<float2>& alpha,
    cudaStream_t stream=NULL)
{
    assert(matrix.size() % vector.size() == 0);
    assert(output.size() == matrix.size());
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    dim3 grid(cols / 1024 + 1, 1, 1);
    kernel::mat_vec_mult<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(matrix.data()),
        thrust::raw_pointer_cast(vector.data()),
        thrust::raw_pointer_cast(output.data()),
        thrust::raw_pointer_cast(alpha.data()), cols, rows
    );
}

/**
 * @brief elementwise multiplication of matrix and vector (in-place) on the GPU
 *
 * @tparam T the type of items in matrix and vector
 * @param matrix the input matrix
 * @param vector the input vector
 * @param stream the cudaStream_t used to process. Defaults to NULL.
 */
template<typename T>
void mat_vec_mult(
    thrust::device_vector<T>& matrix,
    thrust::device_vector<T>& vector,
    cudaStream_t stream=NULL)
{
    assert(matrix.size() % vector.size() == 0);
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    dim3 grid(cols / 1024 + 1, 1, 1);
    kernel::mat_vec_mult<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(matrix.data()),
        thrust::raw_pointer_cast(vector.data()),
        cols, rows
    );
}

/**
 * @brief Elementwise multiplication of matrix and vector on the CPU
 *
 * @note Overloaded function: Either runs on host or GPU - depending on the thrust vectors
 *
 * @param matrix the input matrix
 * @param vector the input vector
 * @param output the output matrix
 * @param alpha  the scalar to multiply element with
 * @param stream the cudaStream_t used to process. Defaults to NULL.
 */
void mat_vec_mult(
    thrust::host_vector<float2>& matrix,
    thrust::host_vector<float>& vector,
    thrust::host_vector<float2>& output,
    thrust::host_vector<float2>& alpha)
{
    assert(matrix.size() % vector.size() == 0);
    assert(output.size() == matrix.size());
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    assert(rows == alpha.size());
    for(std::size_t i = 0; i < rows; i++){
        for(std::size_t ii = 0; ii < cols; ii++){
            output[i * cols + ii] = cmult(alpha[i], matrix[i * cols + ii], vector[ii]);
        }
    }
}

/**
 * @brief elementwise multiplication of matrix and vector (in-place) on the CPU
 *
 * @tparam T the type of items in matrix and vector
 * @param matrix the input matrix
 * @param vector the input vector
 */
void mat_vec_mult(
    thrust::host_vector<float2>& matrix,
    thrust::host_vector<float>& vector)
{
    assert(matrix.size() % vector.size() == 0);
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    for(std::size_t i = 0; i < rows; i++){
        for(std::size_t ii = 0; ii < cols; ii++){
            matrix[i * cols + ii].x = vector[ii] * matrix[i * cols + ii].x;
            matrix[i * cols + ii].y = vector[ii] * matrix[i * cols + ii].y;
        }
    }
}

}
}
}

#include "src/processing.cu"