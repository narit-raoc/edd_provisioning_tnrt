import unittest
import unittest.mock
import asyncio
import aiokatcp
from aiokatcp.client import Client

from mpikat.utils import get_port
import mpikat.utils.dada_tools as dada
from mpikat.utils import testing

from edd_pfb.pipelines import EDDPFBAnalysis



HOST = "127.0.0.1"
class Test_PFBAnalysisPipeline(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        self.pipeline_port = get_port()
        self.pipeline_thread = testing.launch_server_in_thread(EDDPFBAnalysis, HOST, self.pipeline_port)
        self.pipeline = self.pipeline_thread.server



    async def asyncSetUp(self):
        self.client = await Client.connect(HOST, self.pipeline_port)

    def tearDown(self) -> None:
        self.client.close()
        for p in self.pipeline._subprocesses:
            p.terminate()
        for wd in self.pipeline.watchdogs:
            wd.stop_event.set()
        self.pipeline_thread.stop().result()
        self.pipeline_thread.join(timeout=5)


    async def test_pipeline_start_up(self):
        for i in range(30):
            if self.client.is_connected:
                break
            await asyncio.sleep(0.1)
        self.assertTrue(self.client.is_connected)

    @unittest.mock.patch('mpikat.utils.numa.getInfo', return_value=[])
    async def test_fail_if_no_numa_nodes_available(self, _):
        with self.assertRaises(aiokatcp.FailReply):
            await self.pipeline.configure()

    async def test_configure_should_fail_on_more_than_one_input(self):
        await self.pipeline.set({'input_data_streams': [{}, {}]})
        with self.assertRaises(aiokatcp.FailReply):
            await self.pipeline.configure()

    async def test_configure(self):
        """After configure, the pipeline should be in state idle"""

        self.assertEqual(self.pipeline.state, "idle")
        await self.pipeline.set({'numa_node': '0'})
        await self.pipeline.configure()
        self.assertEqual(self.pipeline.state, "configured")

        # After configure, input and output dada buffers should be allocated
        self.assertTrue(isinstance(self.pipeline.dada_buffers['input'], dada.DadaBuffer))
        self.assertTrue(isinstance(self.pipeline.dada_buffers['output'], dada.DadaBuffer))

        # there should be two managed processes, processing and output
        self.assertEqual(len(self.pipeline._subprocesses), 2)


    async def test_capture_start(self):

        await self.pipeline.set({'numa_node': '0'})
        await self.pipeline.configure()
        N = len(self.pipeline._subprocesses)
        await self.pipeline.capture_start()
        self.assertEqual(self.pipeline.state, "streaming")
        # capture start should start a subprocess to capture the data
        self.assertEqual(len(self.pipeline._subprocesses), N+1)

if __name__ == '__main__':
    unittest.main()
