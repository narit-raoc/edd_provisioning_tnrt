#pragma once

#include <cuda.h>

namespace edd_dbbc {
namespace ddc {
namespace processing {
namespace kernel {


__global__ void convolve(
    const float* idata,
    const float* filter,
    float* odata,
    unsigned filter_size,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned tidy = blockIdx.y;
    float tmp = 0;
    if(tidx < width){
        for(unsigned j = 0; j < filter_size; j++)
        {
            unsigned i_idx = tidy * width + tidx + j - (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp += idata[i_idx] * filter[j];
            }
        }
        odata[tidy * width + tidx] = tmp;
    }
}


__global__ void convolve(
    const float2* idata,
    const float* filter,
    float2* odata,
    unsigned filter_size,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned tidy = blockIdx.y;
    float2 tmp = {.0f,.0f};
    if(tidx < width){
        for(unsigned j = 0; j < filter_size; j++)
        {
            unsigned i_idx = tidy * width + tidx + j - (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp.x += idata[i_idx].x * filter[j];
                tmp.y += idata[i_idx].y * filter[j];
            }
        }
        odata[tidy * width + tidx] = tmp;
    }
}


__global__ void convolve_hilbert(
    const float2* idata,
    const float* filter,
    float* odata,
    unsigned filter_size,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned tidy = blockIdx.y;
    float2 tmp = {.0f,.0f};

    extern __shared__ float s_filter[];

    for(int i = threadIdx.x; i < filter_size; i+=blockDim.x){
        s_filter[i] = filter[i];
    }
__syncthreads();

    if(tidx < width){
        for(unsigned j = 0; j < filter_size; j++)
        {
            unsigned i_idx = tidy * width + tidx - j + (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp.x += idata[i_idx].x * s_filter[j];
                tmp.y += idata[i_idx].y * s_filter[filter_size - 1 - j];
            }
        }
        odata[tidy * width * 2 + tidx] = tmp.x + tmp.y;
        odata[tidy * width * 2 + tidx + width] = tmp.x - tmp.y;
    }
}


template<typename T>
__global__ void upsample(
    const T* idata,
    T* odata,
    unsigned up,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    if(tidx < width){
        odata[tidx * up] = idata[tidx];
    }
}


template<typename T>
__global__ void downsample(
    const T* idata,
    T* odata,
    unsigned down,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    if(tidx < width){
        odata[tidx] = idata[tidx * down];
    }
}


__global__ void poly_resample(const float2* idata,
    const float* filter,
    float2* odata,
    unsigned up,
    unsigned down,
    unsigned width,
    unsigned ntaps)
{
    unsigned tidx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned tidy = blockIdx.y;
    unsigned o_width = width * up / down;
    unsigned half_fir = (ntaps - 1) / 2;
    unsigned rotate = up - down + up * down;
    unsigned iidx = 0;
    extern __shared__ float s_filter[];

    for(int i = threadIdx.x; i < ntaps; i+=blockDim.x){
        s_filter[i] = filter[i];
    }
__syncthreads();
    for(unsigned oidx = tidx; oidx < o_width; oidx+=blockDim.x*gridDim.x){
        float2 tmp = {0,0};
        unsigned offset = (oidx * down);
        unsigned poly_branch = (half_fir + rotate * oidx) % up;
        for(unsigned fidx = poly_branch; fidx < ntaps; fidx+=up){
            iidx = tidy * width + (offset + fidx - half_fir) / up;
            if(iidx >= tidy * width && iidx < (tidy+1) * width){
                tmp.x += idata[iidx].x * s_filter[fidx];
                tmp.y += idata[iidx].y * s_filter[fidx];
            }
        }
        odata[tidy * o_width + oidx] = tmp;
    }
}


template<typename T>
__global__ void poly_resample(const T* idata,
    const T* filter,
    T* odata,
    unsigned up,
    unsigned down,
    unsigned width,
    unsigned ntaps)
{
    unsigned tidx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned tidy = blockIdx.y;
    unsigned o_width = width * up / down;
    unsigned half_fir = (ntaps - 1) / 2;
    unsigned rotate = up - down + up * down;
    unsigned iidx = 0;
    extern __shared__ float s_filter[];

    for(unsigned i = threadIdx.x; i < ntaps; i+=blockDim.x){
        s_filter[i] = filter[i];
    }
__syncthreads();

    for(unsigned oidx = tidx; oidx < o_width; oidx+=blockDim.x*gridDim.x){
        float tmp = 0;
        unsigned poly_branch = (half_fir + rotate * oidx) % up;
        for(unsigned fidx = poly_branch; fidx < ntaps; fidx+=up){
            iidx = tidy * width + (oidx * down + fidx - half_fir) / up;
            if(iidx >= tidy * width && iidx < (tidy+1) * width){
                tmp += idata[iidx] * s_filter[fidx];
            }
        }
        odata[tidy * o_width + oidx] = tmp;
    }

}


__global__ void mat_vec_mult(
    const float2* matrix,
    const float* vector,
    float2* odata,
    float2* alpha,
    unsigned width,
    unsigned height)
{
    __shared__ float s_vec[1024];
    unsigned glob_idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(glob_idx < width){
        s_vec[threadIdx.x] = vector[glob_idx];
        __syncthreads();
        for(unsigned i = glob_idx; i < height*width; i+=width){
            odata[i] = cmult(matrix[i], alpha[i/width], s_vec[threadIdx.x]);
        }
    }
}

template<typename T>
__global__ void mat_vec_mult(
    T* matrix,
    const T* vector,
    unsigned width,
    unsigned height
)
{
    __shared__ T s_vec[1024];
    unsigned glob_idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(glob_idx < width){
        s_vec[threadIdx.x] = vector[glob_idx];
    }
    __syncthreads();
    for(unsigned i = glob_idx; i < height*width; i+=width){
        matrix[i].x = matrix[i].x * s_vec[threadIdx.x].x - matrix[i].y * s_vec[threadIdx.x].y;
        matrix[i].y = matrix[i].x * s_vec[threadIdx.x].y + matrix[i].y * s_vec[threadIdx.x].x;
    }
}



const unsigned N_OSAMPLES = 32;

/**
 * @brief
 *
 * @param idata
 * @param filter
 * @param odata
 * @param up
 * @param down
 * @param width
 * @param ntaps
 */
__global__ void mix_poly_resample(
    const float* idata,
    const float* filter,
    const float* f_lo,
    const float2* oscillations,
    float2* odata,
    const int up,
    const int down,
    const int width,
    const int height,
    const int half_fir,
    const int ntaps_pad,
    const float t0)
{
    // The block-wide thread index
    const int tid = threadIdx.x;
    // The grid offset with respect to the output data
    const int offset = blockIdx.x * N_OSAMPLES;
    // The output index - only 32 threads of the first warp are used, rest is ignored
    const int ooidx = offset + threadIdx.x;
    // Number of warps
    const int n_warp = blockDim.x / WARP_SIZE;
    // The number of output items (per band)
    const int o_width = width * up / down;
    // The length of the padded poly branch -> always padded to a multiple of WARP_SIZE
    const int poly_branch_len_pad = ntaps_pad / up; // Pad poly branch length - a multiple of 32
    // The floored sampling ratio
    const int ratio_int = down / up;
    // The rotation is the number of branch rotations per polyphase cycle
    // const int rotate = down * up + up - down;
    const int rotate = ratio_int * up + up - down;
    // Used to compare if the used poly_branch is smaller than the comprator
    const int comp = static_cast<float>(up) * ((down / static_cast<float>(up)) - ratio_int) + 0.5;
    // const int comp = up * fdividef(down, up) - ratio_int + 0.5;
    // The shift width after each output iteration
    int shift = ratio_int;
    // The input index used to load data from the global memory
    int iidx = 0;
    // Temp variable to store and load input data
    float tmpf = 0;
    // Temp variable used for computitation stroing the real part
    float tmp_real = .0;
    // Temp variable used for computitation storing the imaginary part
    float tmp_imag = .0;
    // Temp variable the filtered product
    float filtered;
    // Temp variable to store the real part of the mixing product
    float mixed_real;
    // Temp variable to store the imaginary part of the mixing product
    float mixed_imag;
    // temp variable to store the osclaltion frequency
    float2 osci;


    // ------------- //
    // Shared memory //
    // ------------- //
    // Static shared memory array to store intermediate results. This array is warp_reduced to get a single output sample
    __shared__ float2 s_alpha[WARP_SIZE];
    // Holder for the dynamically allocated shared memory
    extern __shared__ char dyn_smem[];
    // The dynamic array containing the filter taps of the polyphase filter
    float* s_filter = (float*) (&dyn_smem[0]);
    // The dynamic array containing the input data - temporary
    float* s_input  = (float*) (&dyn_smem[ntaps_pad * 4]);
    // The dynamic array containing 32 x height output samples
    float2* s_odata = (float2*)(&dyn_smem[ntaps_pad * 8]);
    // The dyanamic array to store intermediate results -> gets warp reduced
    float2* s_inter = (float2*)(&dyn_smem[ntaps_pad * 8 + WARP_SIZE * height * 8]);

    // -------------------- //
    // Initial data loading //
    // -------------------- //
    if(tid < height)
    {
        float phase = -2.0 * f_lo[tid] * static_cast<float>(t0);
        s_alpha[tid].x = cospif(phase);
        s_alpha[tid].y = sinpif(phase);
    }
__syncthreads();
    // Load filter into shared memory - always padded to multiple of 32
    for(int i = tid; i < ntaps_pad; i+=blockDim.x)
    {
        s_filter[i] = filter[i];
    }
    // Load input into shared memory - always padded to multiple of 32
__syncthreads();
    int k;
    for(k = 0; k < ntaps_pad; k+=blockDim.x)
    {
        iidx = (offset * down + (half_fir + rotate * offset) % up + (k+tid) * up - half_fir) / up;
        if(iidx >= 0 && iidx < width)
        {
            s_input[k+tid] = idata[iidx];
        }
        else
        {
            s_input[k+tid] = 0;
        }
    }
__syncthreads(); // Ensure data are loaded
    iidx -= (k - blockDim.x); // Later used for pipelined loading of input data

    // --------------- //
    // Processing part //
    // --------------- //
    // Every warp computes a single output sample per iteration.
    // In total 32 output sample per mixing frequency and warp are calculated
#pragma unroll
    for(int oidx = 0; oidx < N_OSAMPLES && oidx + offset < o_width; oidx++)
    {
        // Compute the currently polyphase branch
        int poly_branch = (half_fir + rotate * (oidx + offset)) % up;
        // Iterate over mixing frequencies
        for(int i = 0; i < height; i++)
        {
__syncthreads();
            float2 tmp_alpha = s_alpha[i];
            tmp_real = 0;
            tmp_imag = 0;
            for(int ii = 0; ((ii + tid) < poly_branch_len_pad) && ((iidx + ii) < width) && ((iidx + ii) >= 0); ii+=blockDim.x)
            {
                osci = oscillations[i * width + iidx + ii];
                filtered = s_input[ii + tid] * s_filter[poly_branch * poly_branch_len_pad + ii + tid];
                mixed_real = osci.x * tmp_alpha.x;
                mixed_imag = osci.x * tmp_alpha.y;
                mixed_real = fmaf(osci.y, tmp_alpha.y*-1, mixed_real);
                mixed_imag = fmaf(osci.y, tmp_alpha.x, mixed_imag);
                tmp_real = fmaf(mixed_real, filtered, tmp_real);
                tmp_imag = fmaf(mixed_imag, filtered, tmp_imag);
            }
            // warp wide reduction
#pragma unroll
            for(int offset = 16; offset > 0; offset /=2)
            {
                tmp_real += __shfl_down_sync(0xffffffff, tmp_real, offset);
                tmp_imag += __shfl_down_sync(0xffffffff, tmp_imag, offset);
            }
            s_inter[tid] = {tmp_real, tmp_imag};
__syncthreads();
            // Reduce the block by using only the first thread
            if(tid == 0)
            {
                for(int ii = WARP_SIZE; ii < n_warp * WARP_SIZE; ii+=WARP_SIZE)
                {
                    tmp_real += s_inter[ii].x;
                    tmp_imag += s_inter[ii].y;
                }
                s_odata[i * N_OSAMPLES + oidx] = {tmp_real, tmp_imag};
            }
        }

        // ---------------------- //
        // Pipelined data loading //
        // ---------------------- //
        // Shift input data and load next input data
        // Compute the shift range.
        if(poly_branch < comp)
        {
            // If down / up has decimal points it is compensated here
            shift = ratio_int + 1;
        }
        else
        {
            shift = ratio_int;
        }
        // Shift the data in the shared memory by value of shift
        for(int i = 0; i < ntaps_pad - shift; i+=blockDim.x)
        {
            // If thread is used for the shift, first load the sample into register
            int hidx = i + tid;
            if(hidx < (ntaps_pad - shift))
            {
                tmpf = s_input[hidx + shift];
            }
__syncthreads();
            // If the threads are synced -> all participating threads have loaded the sample into register
            // write the sample back to the corresponding location in the shared memory
            if(hidx < (ntaps_pad - shift))
            {
                s_input[hidx] = tmpf;
            }
__syncthreads();
        }

        // Load new input data
        for(int i = 0; i < shift; i+=blockDim.x)
        {
            int s_iidx = ntaps_pad - shift + i + tid;
            int glob_iidx = iidx + i + ntaps_pad;
            if((glob_iidx >= 0) && (glob_iidx < width) && (s_iidx < ntaps_pad))
            {
                s_input[s_iidx] = idata[glob_iidx];
            }
        }
        iidx += shift; // Increment the input index for all threads
__syncthreads(); // Ensure data are loaded
    }
    // ------------------- //
    // Offload output data //
    // ------------------- //

    if(ooidx < o_width && tid < N_OSAMPLES)
    {
#pragma unroll
        for(int i = 0; i < height; i++)
        {
            odata[o_width * i + ooidx] = s_odata[i * N_OSAMPLES + tid];
        }
    }
}


}
}
}
}