---
# Repository Settings
#####################
#
# This version tag is used to tag all containers build + run with the
# provisioning systems. The default "latest" is set in the common role and
# overriden here for the production environment.

version_tag: "testing"
# Tag name used for loading pipelines in the master controller. All created
# products will be tagegd with version and deploy tag.Thus, changing a single
# pipeline in production becomes easy:  just tag the container
# Repository and branch of the site specific provisioning system (roles and
# provision descriptions) used by the master controller
deploy_tag: "testing"

# Repository and branch of the provisioning system (roles and provision descriptions) used by the master controller
provision_repository: "https://gitlab.com/narit-raoc/edd_provisioning_tnrt.git"
provision_branch: "feature/upgrade_EDD_from_docker"

# These settings specify the repositories used to build the EDD
# In particular useful to be tweaked in a local custom mysite_dev inventory to develop the provisioning system.
# Repository and branch used to check out mpikat
mpikat_repository: "https://gitlab.mpcdf.mpg.de/mpifr-bdg/mpikat.git"
mpikat_branch: "dev"

# Inventory file / directory in this repository to be used.
# ToDo: replace with look up of the current inventory
edd_inventory_folder: "TNRT_testing"

# Data path for the docker registry:
# docker_registry_data_path: "/beegfsEDD/edd_docker_registry"  NO REGSITRY IN DEV SETUP

# Data path for the influx db database
docker_registry_data_path: "/hdd/mpifr_deploy/docker_registry"

# Data path for the influx db database
influx_db_path: "/hdd/mpifr_deploy/edd_influx_data"
imflux_memory_limit: 8g

influx_retention: 30w
loki_retention: 30w


# Architecture for gcc optimizations
gcc_architecture: "broadwell"   # to use same container on pacifixes as on edds

# Tweak to use the correct python version depending on the used ansible version.
ansible_python_interpreter: auto_legacy_silent

# use sudo
#ansible_become: no

# If true, packetizers will be modified during roll-out
enable_packetizer_management: False

# In production, all versions are specified, so we can safely use docker cache
use_docker_cache: True

# Network configuration
#######################
#
# Parameters of the network configuration of the site

# The subnet from which the katcp server accept control connections
edd_subnet: "0.0.0.0"

# Subnet used for highspeed data connections
high_speed_data_subnet: "192.168.91.0"
high_speed_data_subnetmask: "255.255.255.0"

# Address range used by the master cotnroller to automatically assign data streams
multicast_network: '225.0.1.0/16'


# Port used for the ssh connections of the master controller tot he ansible interface
edd_ansible_port: 2222

enable_dhcpd: False # False because production already run this docker

# Variables for Effelsberg specific services
##############################################
#
# These variables are only used for the effelsberg setup.
# log_server: "effmonitor1:12204"

# fits_interface_port: 5002

# DEBMIRROR: http://debcache.mpifr-bonn.mpg.de/ubuntu/

# File System Settings
#########################
#
# Settings of the shared file system / common file system layout of the nodes.

# Data base path to be used for pipelines. The pipelines will create
# data_base_path/container_name as directory and see this internally as /mnt
# This can be overriden on per role basis by setting data_path variable
# for the role
data_base_path: "/hdd/mpifr_deploy/testing/"

# base path as used for additional data definitions
base_path: "{{ playbook_dir }}"


# Service variables
#####################
#
# These variables are used to specify the host and port on which a specific
# service is running. Typically, the first available host in a service group is
# used. Configuring the same role on multiple service machines thus automatically
# provides a simple redundancy automatically.

redis_storage: "{{ groups['redis'][0] }}" # only for old EDD version will remove next
redis_host: "{{ groups['redis'][0] }}"
# redis_port: 6333 change to be same as efflesberg parameter
redis_port: 6379

docker_registry: "{{ groups['registry'][0] }}"
docker_registry_port: "5000"
docker_registry_webui_port: "8083"

# master controller port should not be part of the katcp port range to avoid
# conflicts for sure
master_controller_port: 7147

interface_host:  "{{ groups['interface'][0] }}"

influx_host: "{{ groups['influx'][0] }}"
influxdb_port: "8086"

grafana_host: "{{ groups['grafana'][0] }}"
grafana_port: "3000"

loki_host: "{{ groups['loki'][0] }}"
loki_port: 3100

# Non-PC Devices
################
#
# Any device which are present in the ansible configuration, in particular
# devices with entwork interface, but to which ansible cannot log on to control
# them.
npc_devices:
  skarab_00:
    interfaces:
      0:
        mac: 06:50:02:0e:01:01
        ip: 192.168.91.121
      1:
        mac: 06:50:02:0e:01:02
        ip: 192.168.91.122
    control_port: 7147

  # alveo_00: #fpga000 -> to be moved to alveo_host group
  #   interfaces:
  #     0:
  #       mac: 00:0A:35:06:9E:EA
  #       ip: 10.10.1.32
  #     1:
  #       mac: 00:0A:35:06:9E:EB
  #       ip: 10.10.1.33
  #   device: 0
  #   control_port: 7180
