#include <gtest/gtest.h>

#include "edd_dbbc/test/test_testing_tools.cuh"
#include "edd_dbbc/test/test_math_tools.cuh"

int main(int argc, char **argv) {
 ::testing::InitGoogleTest(&argc, argv);

 return RUN_ALL_TESTS();
}
