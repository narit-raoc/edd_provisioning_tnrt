import asyncio
import json
import redis
import matplotlib.pyplot as plt

from mpikat.core import logger
from mpikat.utils.testing.provision_tests import register, Stage, EDDTestContext
from mpikat.utils.data_streams import RedisSpectrumStreamFormat, deserialize_array
from mpikat.pipelines.gated_spectrometer.plotter import encodeb64_figure
from dbbc.data_streams import DDCDataStreamFormat

_LOG = logger.getLogger('mpikat.dbbc.testing.provision_tests.ddc_plotter')

@register(stage=Stage.capture_start, class_name='DDCPlotter', tbc=True)
async def test_is_streaming_data(context: EDDTestContext):

    """
    The pipeline should send data.
    """
    _LOG.debug('Checking output data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))
    sleep = cfg["n_accumulate"]* DDCDataStreamFormat.samples_per_heap / cfg["input_data_streams"][0]["sample_rate"] * 4
    await asyncio.sleep(sleep)

    streams = set([val["name"] for __, val in cfg['output_data_streams'].items()])
    client = redis.StrictRedis(cfg["data_store"]["ip"], cfg["data_store"]["port"], 2).json()
    # Proof if all streams are online
    for stream in streams:
        res = client.get(stream)
        for key in RedisSpectrumStreamFormat.data_items.keys():
            assert key in res, f"Key {key} not in streamed dicitonary"
        data = deserialize_array(res["data"])
        fig = plt.figure(figsize=(8,4))
        plt.plot(data)
        fig.suptitle(f"{stream}")
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        context.output.append(encodeb64_figure(fig).decode('ascii'))


