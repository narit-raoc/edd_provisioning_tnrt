namespace edd_dbbc {
namespace vdif {

Packer::Packer(float digitizer_threshold)
    :
    _digitizer_threshold(digitizer_threshold)
{

}


Packer::~Packer()
{

}

void Packer::process(
    thrust::device_vector<float>* idata,
    thrust::device_vector<uint32_t>* odata,
    cudaStream_t stream)
{
    float mean = edd_dbbc::tools::mean(*idata);
    BOOST_LOG_TRIVIAL(debug)
        << "\t- Calculated mean " << std::to_string(mean);
    float sigma2 = edd_dbbc::tools::variance(*idata, mean);
    BOOST_LOG_TRIVIAL(debug)
        << "\t- Calculated sigma2 " << std::to_string(sigma2);

    // non linear packing
    BOOST_LOG_TRIVIAL(debug)
        << "\t- Packing data with non linear 2-bit packaging"
        << " using levels -v*sigma, 0, v*sigma with v = " << _digitizer_threshold;
    pack2bit_non_linear<<<n_blocks, n_threads, 0, stream>>>(
      thrust::raw_pointer_cast(idata->data()),
      thrust::raw_pointer_cast(odata->data()),
      idata->size(), _digitizer_threshold, sigma2, mean
    );
}

}
}