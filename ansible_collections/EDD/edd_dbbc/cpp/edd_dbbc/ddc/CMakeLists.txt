set(DEPENDENCY_LIBRARIES
${CMAKE_PROJECT_NAME}
${DEPENDENCY_LIBRARIES}
${CUDA_CUBLAS_LIBRARIES}
${CUDA_CUFFT_LIBRARIES}
-lpsrdada_cpp_common
)

include_directories(${CMAKE_SOURCE_DIR})

set(edd_dbbc_src_ddc
  src/signal.cu
  src/filter.cu
  src/processing.cu
  src/ddc.cu
  src/pipeline.cu
)

set(edd_dbbc_inc_ddc
  signal.h
  filter.h
  processing.h
  ddc.cuh
  pipeline.cuh
)

set(DEPENDENCY_LIBRARIES ${DEPENDENCY_LIBRARIES} ${CMAKE_PROJECT_NAME})

set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES LINKER_LANGUAGE CXX)

# cuda_add_executable(ddc_example cli/example.cu)
# target_link_libraries(ddc_example ${DEPENDENCY_LIBRARIES} )
# install(TARGETS ddc_example DESTINATION bin)

cuda_add_executable(ddc_processor cli/ddc_cli.cu)
target_link_libraries(ddc_processor ${DEPENDENCY_LIBRARIES} )
install(TARGETS ddc_processor DESTINATION bin)

if (GTest_FOUND)
  add_subdirectory(test)
else()
  message(WARNING "Google Test framework not found, not able to test")
endif()

if(ENABLE_BENCHMARK)
    include_directories(${BENCHMARK_INCLUDE_DIR})
    link_directories(${BENCHMARK_LIBRARY_DIR})
    cuda_add_executable(ddc_benchmark cli/ddc_benchmark.cu)
    add_dependencies(ddc_benchmark googlebenchmark)
    target_link_libraries(ddc_benchmark ${BENCHMARK_LIBRARIES} ${DEPENDENCY_LIBRARIES})
endif(ENABLE_BENCHMARK)

install(FILES ${edd_dbbc_inc_vdif} DESTINATION include/edd_dbbc/ddc)
