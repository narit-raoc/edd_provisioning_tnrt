#pragma once

#include <cuda.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform_reduce.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>

#include <functional>
#include <cmath>

namespace edd_dbbc {
namespace tools {

template<typename T>
T mean(thrust::device_vector<T> vec, T val=0)
{
    T acc = thrust::reduce(vec.begin(), vec.end(), val, thrust::plus<T>());
    return acc / vec.size();
}

template<typename T>
struct variance_unary : std::unary_function<T, T>
{
    const T mean;
    variance_unary(T m) : mean(m){}
    __device__ T operator()(const T &data) const
    {
        return (data - mean) * (data - mean);
    }
};

template<typename T>
T variance(thrust::device_vector<T> vec, T mean)
{
    T var = thrust::transform_reduce(
        vec.begin(), vec.end(),
        variance_unary<T>(mean), 0.0f,
        thrust::plus<T>()
    );
    return var / vec.size();
}


}
}