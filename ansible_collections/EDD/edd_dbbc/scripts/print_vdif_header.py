"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

This script reads a vdif file and prints the header.
"""
import os
import argparse
from argparse import RawTextHelpFormatter

from dbbc import vdif

def main_cli():
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('--input_file', '-i', action='store',
                        default="VDIFdata/n22l3_ef_no0001.m5a", dest="ifile",
                        help="The input file containing VDIF data")
    parser.add_argument('--psize', '-b', action='store', default=8000, dest="psize",
                        help="Number of bytes in payload / data array")
    parser.add_argument('--hsize', '-s', action='store', default=32, dest="hsize",
                        help="Number of bytes in data header")
    parser.add_argument('--nframes', '-n', action='store', default=-1, dest="frame",
                        help="Number of frames to read")
    parser.add_argument('--offset', '-o', action='store', default=0, dest="offset",
                        help="Offset in bytes where to start reading the data")
    ifile = str(parser.parse_args().ifile)
    psize = int(parser.parse_args().psize)
    hsize = int(parser.parse_args().hsize)
    frame = int(parser.parse_args().frame)
    offset = int(parser.parse_args().offset)
    cnt = 0
    if frame == -1:
        frame = int(os.stat(ifile).st_size) // (psize+hsize)
    with open(ifile, mode="rb") as file:
        vdif_file = vdif.Reader(file, offset)
        while True:
            header = vdif_file.get_header()
            if cnt > frame or header is False:
                break
            print(header)
            cnt += 1

if __name__ == "__main__":
    main_cli()
