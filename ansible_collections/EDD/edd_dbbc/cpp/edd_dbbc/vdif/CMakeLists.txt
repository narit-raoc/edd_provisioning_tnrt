set(DEPENDENCY_LIBRARIES ${DEPENDENCY_LIBRARIES}
    -lpsrdada_cpp_common
)

set(edd_dbbc_inc_vdif
    header.h
    sender.h
    packer.cuh
    pipeline.cuh
)

set(DEPENDENCY_LIBRARIES ${DEPENDENCY_LIBRARIES} ${CMAKE_PROJECT_NAME})

add_executable(vdif_sender cli/send_cli.cpp)
target_link_libraries(vdif_sender ${DEPENDENCY_LIBRARIES})
install(TARGETS vdif_sender DESTINATION bin)

cuda_add_executable(vdif_packer cli/packer_cli.cu)
target_link_libraries(vdif_packer ${DEPENDENCY_LIBRARIES})
install(TARGETS vdif_packer DESTINATION bin)

if (GTest_FOUND)
  add_subdirectory(test)
else()
  message(WARNING "Google Test framework not found, not able to test")
endif()

install(FILES ${edd_dbbc_inc_vdif} DESTINATION include/edd_dbbc/vdif)
