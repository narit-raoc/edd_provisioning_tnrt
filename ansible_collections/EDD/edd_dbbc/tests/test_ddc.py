import unittest
from dbbc import ddc

# ------------------------------------------ #
# Test non-class functions of the ddc-module #
# ------------------------------------------ #

class Test_ddc_functions(unittest.TestCase):

    def setUp(self) -> None:
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def get_start_stop_frequencies_baseband(self):
        fc = ddc.get_center_frequency(100e6, 50e6)
        self.assertEqual(fc[0], (75e6,  100e6))
        self.assertEqual(fc[1], (100e6, 125e6))

    def get_start_stop_frequencies_nyquist4(self):
        fc = ddc.get_center_frequency(100e6, 50e6, 1.4e9, 8e8)
        self.assertEqual(fc[0], (1.275e9, 1.3e9))
        self.assertEqual(fc[1], (1.3e9, 1.325e9))

    def test_get_center_frequency_baseband(self):
        fc_low, fc_up = ddc.get_center_frequency(100e6, 50e6)
        self.assertEqual(fc_low, 87.5e6)
        self.assertEqual(fc_up, 112.5e6)

    def test_get_center_frequency_nyquist4(self):
        fc_low, fc_up = ddc.get_center_frequency(100e6, 50e6, 1.4e9, 8e8)
        self.assertEqual(fc_low, 1.2875e9)
        self.assertEqual(fc_up,  1.3125e9)

    def test_get_nyquist_zone(self):
        nz = ddc.get_nyquist_zone(8e8, 1.4e9)
        self.assertEqual(nz, 4)

    def test_get_oscillation_frequencies_upper(self):
        lo = ddc.get_oscillation_frequencies([2e6, 4e6], 1e6)
        self.assertTrue(isinstance(lo, list))
        self.assertEqual(lo, [1.5e6, 3.5e6])

        lo = ddc.get_oscillation_frequencies(4e6, 1e6)
        self.assertTrue(isinstance(lo, list))
        self.assertEqual(lo, [3.5e6])

    def test_get_oscillation_frequencies_lower(self):
        lo = ddc.get_oscillation_frequencies([2e6, 4e6], 1e6, False)
        self.assertTrue(isinstance(lo, list))
        self.assertEqual(lo, [2.5e6, 4.5e6])

        lo = ddc.get_oscillation_frequencies(4e6, 1e6, False)
        self.assertTrue(isinstance(lo, list))
        self.assertEqual(lo, [4.5e6])

    def test_get_nsamples(self):
        # Input rate is multiple of output
        nsamples = ddc.get_nsamples(8e8, 2e6, 4096)
        dw = ddc.downsample(8e8, 2e6)
        up = ddc.upsample(8e8, 2e6)
        self.assertEqual(nsamples % 4096, 0)
        self.assertEqual(nsamples % dw, 0)
        self.assertEqual(nsamples % up, 0)
        self.assertGreaterEqual(nsamples, 2**20)
        # Input rate is not divisble by output
        nsamples = ddc.get_nsamples(7.6e6, 2e6, 4096, 2**24)
        dw = ddc.downsample(7.6e6, 2e6)
        up = ddc.upsample(7.6e6, 2e6)
        self.assertEqual(nsamples % 4096, 0)
        self.assertEqual(nsamples % dw, 0)
        self.assertEqual(nsamples % up, 0)
        self.assertGreaterEqual(nsamples, 2**24)

if __name__ == "__main__":
    unittest.main()
