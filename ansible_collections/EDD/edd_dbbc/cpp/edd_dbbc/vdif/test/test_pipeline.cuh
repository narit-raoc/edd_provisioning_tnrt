#pragma once

#include <gtest/gtest.h>
#include <ctime>
#include <random>
#include <string>

#include <psrdada_cpp/testing_tools/streams.hpp>
#include <psrdada_cpp/testing_tools/tools.cuh>
#include "edd_dbbc/testing_tools.cuh"
#include "edd_dbbc/vdif/pipeline.cuh"
#include "edd_dbbc/vdif/test/test_packer.cuh"


namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace vdif {
namespace test {

template<typename Handler, typename InputType>
class Test_Pipeline
{
public:
    Test_Pipeline(pipe_config &config, Handler &handle) : _config(config) {
        test_object.reset(
            new Pipeline<Handler, InputType>(_config, vdif_header, handle)
        );
    }
    ~Test_Pipeline(){};

    void init(psrdada_cpp::RawBytes &header)
    {
        test_object->init(header);
    }

    void init()
    {
        // Create the DADA header and pass it to the pipeline under test
        char header[header_size] = {0};
        std::string hstring = "SAMPLE_CLOCK_START 0\nSYNC_TIME "
            + std::to_string(std::time(nullptr)) + "\n";
        std::copy(hstring.cbegin(), hstring.cend(), header);
        psrdada_cpp::RawBytes bytes(header, header_size, header_size);
        // Initalize the pipeline under test with the DADA header
        test_object->init(bytes);
    }

    psrdada_cpp::RawBytes output_block()
    {
        return psrdada_cpp::RawBytes(
            reinterpret_cast<char *>(odata.data()),
            odata.size() * sizeof(uint32_t),
            odata.size() * sizeof(uint32_t)
        );
    }

    psrdada_cpp::RawBytes input_block()
    {
        return psrdada_cpp::RawBytes(
            reinterpret_cast<char *>(idata.data()),
            idata.size() * sizeof(InputType),
            idata.size() * sizeof(InputType)
        );
    }
    void stream_until_first_output_block(std::size_t nsamples)
    {
        // For the reference processing we use the Test_Packer object, which implements a CPU reference packing method
        Test_Packer test_packer(nsamples, _config.digitizer_threshold);
        this->init();
        // If the InputType is float, simply use the same input
        if(std::is_same<InputType, float>::value)
        {
            std::cout << "Shifting by " << test_object->get_spillover() << " samples " <<std::endl;
            idata = edd_dbbc::tools::shift(test_packer.get_input(), test_object->get_spillover(), false);
        }
        // Otherwise we need to create a random int8_t array and cast it to a uint64_t array.
        else
        {
            thrust::host_vector<int8_t> vec = psr_testing::random_vector<int8_t>(0, 32, nsamples);
            thrust::host_vector<float> input = edd_dbbc::tools::shift(
                edd_dbbc::tools::conversion<int8_t, float>(vec), test_object->get_spillover(), true
            );
            test_packer.set_input(input);
            // reinterpret the data as uint64_t
            idata = thrust::host_vector<uint64_t>(
                reinterpret_cast<uint64_t*>(thrust::raw_pointer_cast(vec.data())),
                reinterpret_cast<uint64_t*>(thrust::raw_pointer_cast(vec.data())) + nsamples / 8
            );
        }
        odata = test_packer.get_output();
        psrdada_cpp::RawBytes input_block = this->input_block();
        // Iterate until count count is 5 to get the first
        while(test_object->call_count() != 5){
            ASSERT_FALSE((*test_object)(input_block));
        }
    }

    void compare_payload(char* actual_ptr)
    {
        char expected, actual;
        char* reference = output_block().ptr();
        int cnt = 0;
        for(std::size_t i = 0; i < output_block().used_bytes() / _config.n_bytes_payload; i++)
        {
            for(std::size_t ii = 0; ii < _config.n_bytes_payload; ii++)
            {
                expected = reference[i * _config.n_bytes_payload + ii];
                actual = actual_ptr[i * _config.n_bytes_payload + ii + (i+1) * vdif_header_size];
                if(expected != actual){cnt++;}
                EXPECT_EQ(expected, actual)
                    << "Expected " << std::to_string(expected) << ", got " << std::to_string(actual)
                    << " at position " << std::to_string(i + ii) << std::endl;
            }
        }
        std::cout << "Unequal elements " << cnt << "/"
            << output_block().used_bytes() << " "
            << ((float)cnt)/output_block().used_bytes()*100 << "%" << std::endl;
    }

private:
    thrust::host_vector<InputType> idata;
    thrust::host_vector<uint32_t> odata;
    std::unique_ptr<Pipeline<Handler, InputType>> test_object;
    Header vdif_header;
    pipe_config _config;
};


// TEST(Test_Pipeline, test_asserts)
// {
//     pipe_config config;
//     std::size_t nsamples = 8192*256;
//     config.bit_depth = 8;
//     config.buffer_bytes = nsamples * sizeof(int8_t);
//     psr_testing::DummyStream<char> dummy;
//     EXPECT_THROW(Pipeline<psr_testing::DummyStream<char>, InputType>(_config, vdif_header, dummy), std::runtime_error);
//     config.bit_depth = 32;
//     config.buffer_bytes = nsamples+1 * sizeof(float);
//     EXPECT_THROW(Pipeline<psr_testing::DummyStream<char>, InputType>(_config, vdif_header, dummy), std::runtime_error);
// }


TEST(Test_Pipeline, test_init_method)
{
    char header[header_size] = {0};
    std::string hstring = "SAMPLE_CLOCK_START 0\nSYNC_TIME 0\n";
    std::copy(hstring.cbegin(), hstring.cend(), header);
    psrdada_cpp::RawBytes bytes(header, header_size, header_size);

    pipe_config config;
    psr_testing::DummyStream<char> consumer;
    Test_Pipeline<psr_testing::DummyStream<char>, float> tester(config, consumer);
    tester.init(bytes);
    ASSERT_TRUE(hstring == std::string(consumer.header_ptr(), hstring.size()));
}


TEST(Test_Pipeline, test_operator_method_float_no_spillover)
{
    pipe_config config;
    std::size_t nsamples = 32000000;
    config.bit_depth = 32;
    config.buffer_bytes = nsamples * sizeof(float);
    psr_testing::DummyStream<char> consumer; // Create a consumer -> output handler
    Test_Pipeline<psr_testing::DummyStream<char>, float> tester(config, consumer);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload(consumer.data_ptr());
}

TEST(Test_Pipeline, test_operator_method__float_spillover_buffer_greater_1_second)
{
    pipe_config config;
    std::size_t nsamples = 32000000*3;
    config.bit_depth = 32;
    config.buffer_bytes = nsamples * sizeof(float);
    psr_testing::DummyStream<char> consumer; // Create a consumer -> output handler
    Test_Pipeline<psr_testing::DummyStream<char>, float> tester(config, consumer);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload(consumer.data_ptr());
}

TEST(Test_Pipeline, test_operator_method_float_spillover_buffer_no_full_second_align)
{
    pipe_config config;
    std::size_t nsamples = 8000*666;
    config.bit_depth = 32;
    config.buffer_bytes = nsamples * sizeof(float);
    psr_testing::DummyStream<char> consumer; // Create a consumer -> output handler
    Test_Pipeline<psr_testing::DummyStream<char>, float> tester(config, consumer);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload(consumer.data_ptr());
}

TEST(Test_Pipeline, test_operator_method_int8_16000)
{
    pipe_config config;
    std::size_t nsamples = 8000*333;
    config.bit_depth = 8;
    config.buffer_bytes = nsamples * sizeof(int8_t);
    psr_testing::DummyStream<char> consumer; // Create a consumer -> output handler
    Test_Pipeline<psr_testing::DummyStream<char>, uint64_t> tester(config, consumer);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload(consumer.data_ptr());
}

}
}
}