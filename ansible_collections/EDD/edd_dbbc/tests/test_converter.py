import unittest
import unittest.mock as mock
import redis
import time
import numpy as np
from mpikat.core.logger import getLogger
from mpikat.core.data_stream import convert64_48
from mpikat.utils.data_streams.converter import serialize_array, deserialize_array

import dbbc.testing as testing
import dbbc.data_streams as ds


LOG = getLogger("mpikat.dbbc.spectrum_stream")

class Test_SpectrumStreamHandler(unittest.TestCase):
    """Testing the class dbbc.redis_sender.RedisSender

    Args:
        unittest (_type_): _description_
    """
    def setUp(self) -> None:
        """
        Setup the test environment.
        Note:
        The test highly depend on an existing Redis server with JSONRedis module loaded.
        If there is no Redis server available the tests are just skipped (sorry).
        """
        self.tobj = ds.Converter("dummy_converter")

    def test_callback_best_case(self):
        """
        Should test that the callback returns the expected data,
            assuming all heaps are in continous order.
        """
        data = np.zeros((self.tobj._naccumulate, self.tobj._conf["samples_per_heap"]),
                        dtype=self.tobj._conf["dtype"])
        handler = ds.DDCSpeadHandler()
        handler.item_group["channel"].value = convert64_48(0) # timestamp item
        handler.item_group["polarization"].value = convert64_48(0) # timestamp item
        for cnt in range(self.tobj._naccumulate):
            data[cnt] = np.asarray(np.random.rand(self.tobj._conf["samples_per_heap"]), dtype=self.tobj._conf["dtype"])
            handler.item_group["timestamp"].value = convert64_48(cnt * self.tobj._conf["samples_per_heap"]) # timestamp item
            handler.item_group["data"].value = data[cnt]
            res = self.tobj.convert(handler.build_packet(handler.item_group))
            if res is not None:
                res_arr = deserialize_array(res["data"], dtype=self.tobj._conf["dtype"])
                expected = 10*np.log10(np.mean(np.abs(np.fft.rfft(data))**2, axis=0)[1:])
                # Check if the FFTs have same length
                self.assertEqual(len(expected), len(res_arr))
                # Check if the FFTs have nearly same results
                self.assertTrue(np.allclose(expected, res_arr))
                # substract interval because _mid has already been updated
                self.assertEqual(
                    res["timestamp"], self.tobj._first / self.tobj._conf["sample_rate"])
                self.assertEqual(
                    res["interval"], self.tobj._naccumulate * self.tobj._nchannel * 2 / self.tobj._conf["sample_rate"])
                self.assertEqual(
                    res["f_min"], self.tobj._conf["center_freq"] - self.tobj._conf["sample_rate"] /4)
                self.assertEqual(
                    res["f_max"], self.tobj._conf["center_freq"] + self.tobj._conf["sample_rate"] /4)

    def test_callback_behavior(self):
        """
        Should test if the consumer gets the correct data from the publisher (capture-thread)
        """
        items = [{"call":0},{"call":1},{"call":2},{"call":4}]
        self.tobj.convert = mock.Mock(return_value=None)
        dummy = testing.DummyCapture(self.tobj.convert, items)
        dummy.start()
        time.sleep(1)
        for i, arg in enumerate(self.tobj.convert.call_args_list):
            self.assertEqual(items[i], arg.args[0])

if __name__ == '__main__':
    # ToDo: start a redis server
    unittest.main()