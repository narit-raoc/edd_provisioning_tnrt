import numpy as np
from dbbc import vdif

def vdif_dummy_file(frames: int, fname: str="/tmp/vdif_dummy.dat", fs: float=1e6):
    """Generates a dummy VDIF file

    Args:
        frames (int): number of frames to generate_
        fname (str, optional): name of the file to write to. Defaults to "/tmp/vdif_dummy.dat".
        fs (float, optional): the sampple frequency used for the generation. Defaults to 32e6.
    """
    psize = vdif.payload_size(fs)
    header = vdif.Header()
    header.data_frame_len = int((psize + vdif.HEADER_SIZE) // 8)
    with open(fname, "wb") as file:
        fps = vdif.frame_size(fs)[1][-1] * header.nchannel
        for __ in range(frames):
            header.frame_id += 1
            if fps == header.frame_id:
                header.frame_id = 0
                header.seconds_from_epoch += 1
            payload = np.random.randint(-127,128, psize, dtype=np.int8)
            frame_bytes = header.to_bytes()
            frame_bytes += payload.tobytes()
            file.write(frame_bytes)


TEST_HEADERS = {
    "set1" : {
        "bytes" : b'\x93\xc6\x96\x00\x00\x00\x00.\xec\x03\x00\x02AN\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',
        "expected" : {
            "word0": {"i": 0,"l": 0,"seconds_from_epoch": 9881235},
            "word1": {"unassigned": 0,"ref_epoch": 46,"frame_id": 0},
            "word2": {"v": 0,"log2chns": 2,"data_frame_len": 1004},
            "word3": {"c1": 0,"bit_sample": 1,"thread_id": 0,"station_id": 20033},
            "word4": {"edv": 0, "user": 0},
            "word5": {"user": 0},
            "word6": {"user": 0},
            "word7": {"user": 0}
        }
    },
    "set2" : {
        "bytes" : b'\x93\xc6\x96\x00\x9f\x0f\x00.\xec\x03\x00\x02AN\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xFF\xFF\x00\x00',
        "expected" : {
            "word0": {"i": 0,"l": 0,"seconds_from_epoch": 9881235},
            "word1": {"unassigned": 0,"ref_epoch": 46,"frame_id": 3999},
            "word2": {"v": 0,"log2chns": 2,"data_frame_len": 1004},
            "word3": {"c1": 0,"bit_sample": 1,"thread_id": 0,"station_id": 20033},
            "word4": {"edv": 0, "user": 0},
            "word5": {"user": 0},
            "word6": {"user": 0},
            "word7": {"user": 65535}
        }
    },
    "set3" : {
        "bytes" : b'\x94\xc6\x96\x00\x00\x00\x00.\xec\x03\x00\x02AN\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\xFF\xFF\x00\x00\x00\x00\x00\x00',
        "expected" : {
            "word0": {"i": 0,"l": 0,"seconds_from_epoch": 9881236},
            "word1": {"unassigned": 0,"ref_epoch": 46,"frame_id": 0},
            "word2": {"v": 0,"log2chns": 2,"data_frame_len": 1004},
            "word3": {"c1": 0,"bit_sample": 1,"thread_id": 0,"station_id": 20033},
            "word4": {"edv": 0, "user": 0},
            "word5": {"user": 0},
            "word6": {"user": 65535},
            "word7": {"user": 0}
        }
    }
}
