
"""
Run all default provision tests from mpikat on a specified provision / invenory combination
"""
import asyncio
import argparse
import os
import datetime
from mpikat.utils.testing.provision_tests import register, Stage, run_tests, TerminalOutput, load_inventory, HTMLOutput, MultiOutput
from mpikat.utils.testing.provision_tests.registry import instance

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('-i', '--inventory', help='Inventory to use', default='effelsberg_devel')
parser.add_argument('-o', '--output', help='Default output directory', default='/beegfsEDD/edd_test_results')
parser.add_argument('provisions', nargs='*', help='Provisions to be used.')

args = parser.parse_args()

import pickle


provisions = set([os.path.splitext(os.path.basename(f))[0] for f in args.provisions])


print(f'Testing {len(provisions)} provisions')

now = datetime.datetime.now().isoformat()

for i, p in enumerate(provisions):
    instance().clear()
    output_prefix = os.path.join(args.output, args.inventory, *now.split('T')[0].split('-'))

    os.umask(0)
    os.makedirs(output_prefix, exist_ok=True, mode=0o755)

    print(f"Running on {i+1} / {len(provisions)}: {p}")
    stage_parameters = {Stage.provision: p,
            }

    # Define outputs
    html = HTMLOutput()
    output = MultiOutput(TerminalOutput(), html )

    # Run tests
    res = asyncio.run(
            run_tests(output=output, inventory_data=load_inventory(args.inventory), stage_parameters=stage_parameters)
            )

    # Save HTML Report
    ofile = os.path.join(output_prefix, f'EDD_TEST_REPORT_{now}_{p}.html')
    print(f'Writing output to {ofile}\n')
    with open(ofile, 'w') as f:
        f.write(html.to_string())
    os.chmod(ofile, 0o744)


