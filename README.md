# Effelsberg provision repository

This repository contains the configuration and provision descriptions specific
to the the EFFELSBERG deployment of the EDD.

## Cloning the repository
The setup uses git submodules to retrieve the core edd_provisioning repository. To
get a working copy, thus:

```
git clone https://gitlab.com/narit-raoc/edd_provisioning_tnrt.git
git submodule init
git submodule update
```

Changes to the the core repository are not included automatically, but need to
be pulled explicitly:

```
cd ansible_collections/EDD/core
git pull --rebase
```

And then the update to a new version of the core needs to be commited.

## Deploying using the edd_tool (Recommended)
The edd_tool helps with clean build and deployments of the EDD.

Requirements
  ansible (pip install ansible)
  coloredlogs (pip install coloredlogs)


Example Usage:

1) List all available versions
  `./edd_tool -i TNRT_devel list-versions`

2) Active deploy enviroment
  `source ~/venv-edd/bin/activate`

3) Deploy a specific version
  `./edd_tool -i ./TNRT_devel deploy --version=230307.0`

4) Build all containers of a version [necessary only on new releases]
  `./edd_tool buildall -i TNRT_devel --version=VERSION buildall`

5) Limit Re-deploy to a specific host, e.g. a single packetizer
  `./edd_tool -i ./TNRT_devel deploy --version=230307.0`

For more information check the help
`./edd_tool --help`



## Manual deployment (not recommended)
System can stil be deployed manyually. However, care must be taken to checkout
/ install the correct versions of the base system

Assuming a checkout with submodules
```
$ cd ansible_collections/EDD/core
$ git co CORRECT_TAG
$ cd ../../../
$ git co CORRECT_TAG
$ ansible-playbook -i TNRT_devel tnrt_config.yml -K -v --ask-vault-pass -b --tags=buildbase,build

$ ansible-playbook -i TNRT_devel tnrt_config.yml -K -v --ask-vault-pass -b
```


# Retagging only
To retag the current 'latest' containers as production verions, you can use

`$ ansible-playbook -i effelsberg_production effelsberg_config.yml --tags retag_latest --ask-vault-password`

NOTE THAT AFTER DEPLOYING, THE REPOSITORY INSIDE THE MASTER CONTROLLER NEEDS TO
BE UPDATED ALSO IF A PRODUCTION VERSION IS CREATED LIKE THAT.

However, it is probably best to always quick-build the master
controller conventionally after retagging.




