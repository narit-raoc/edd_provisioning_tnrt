import asyncio
import unittest
import os
import time
import json
import sys
import numpy as np
from psrdada import Writer, Reader
from mpikat.utils import dada_tools as dada
from mpikat.utils.process_tools import ManagedProcess
from mpikat.core.logger import getLogger

from dbbc import vdif
from dbbc.pipelines.vdif_packer import get_vdifpacker_command

LOG = getLogger("mpikat.dbbc.tests.test_ddc_algorithm")
NBUFFER = 16 # used for stream test

# ---------------- #
# Helper functions #
# ---------------- #

def get_atol(dtype):
    """
    Acceptable absolute tolerance for sample comparsion
    f32 lower tolerance as f64
    """
    if dtype=="float32":
        return 1e-2
    elif dtype=="float64":
        return 1e-6

def get_rtol(dtype):
    """
    Acceptable absolute tolerance for sample comparsion
    f32 lower tolerance as f64
    """
    if dtype=="float32":
        return 1e-1
    elif dtype=="float64":
        return 1e-4

def get_vdifpacker_command_old(desc, physcpu=None):
    """
    Creates the command for the old VDIF packer
    Note:
        This function is deprecated
    """
    cmd = []
    if physcpu:
        cmd.append(f'taskset -c {physcpu} VLBI')
    else:
        cmd.append('VLBI')
    cmd.append(f'--input_key {desc["input_key"]}')
    cmd.append(f'--output_key {desc["output_key"]}')
    cmd.append('--output_type dada')
    cmd.append('--nbits 8')
    cmd.append(f'--speadheap_size {int(desc["samples_per_heap"])}')
    cmd.append(f'--thread_id {int(desc["thread_id"])}')
    cmd.append(f'--station_id {int(desc["station_id"])}')
    cmd.append(f'--payload_size {int(desc["payload_size"])}')
    cmd.append(f'--sample_rate {int(desc["sample_rate"])}')
    LOG.debug("VDIF Packer command (old) %s", ' '.join(cmd))
    return ' '.join(cmd)


class Test_VDIF_Packer(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        """
        Setup the base envoroment for the tests
        """
        super().setUp()
        self._config = {
            "samples_per_heap": 2048,
            "thread_id": 3,
            "station_id": 4,
            "sample_rate": int(2e6),
            "channels":1
        }
        self._config["payload_size"] = vdif.payload_size(self._config["sample_rate"])
        self._config["samples_per_block"] = self._config["payload_size"] * self._config["samples_per_heap"]
        self._8bit_data_dir = "/tmp/vdif_packer_test/8bit"
        self._32bit_data_dir = "/tmp/vdif_packer_test/32bit"
        os.makedirs(self._8bit_data_dir, exist_ok=True)
        os.makedirs(self._32bit_data_dir, exist_ok=True)
        self._o_proc = None
        self._n_proc = None


    async def asyncSetUp(self) -> None:
        """
        asynchronous setUp function creates dada buffer and Reader/Writer
        """
        await self.__create_test_env()

    async def asyncTearDown(self) -> None:
        """
        asynchronous tearDown function destroys dada buffer and Reader/Writer
        """
        super().tearDown()
        await self.__destroy_test_env()

    def test_old_against_new_packer(self):
        data_8bit = (np.random.rand(self._config["samples_per_block"]) * 64).astype(dtype="int8")
        data_32bit = data_8bit.astype(dtype="float32")
        # First second is skipped. Calculate the number of slots when data is written to dada buffer
        n_pack = int(self.o_buffer_8bit.n_bytes // (self._config["payload_size"] + vdif.HEADER_SIZE))
        header = vdif.Header()
        for i in range(32):

            mem_8bit = self.i_writer8bit.getNextPage()
            mem_32bit = self.i_writer32bit.getNextPage()
            mem_8bit[:] = data_8bit.tobytes()
            mem_32bit[:] = data_32bit.tobytes()

            self.i_writer8bit.markFilled()
            self.i_writer32bit.markFilled()

            if i >= 5:

                odata_8bit = self.o_reader8bit.getNextPage()
                odata_32bit = self.o_reader32bit.getNextPage()
                for packet in range(n_pack-1):

                    h_start = int(packet * (self._config["payload_size"] + vdif.HEADER_SIZE))
                    p_start = int(packet * (self._config["payload_size"] + vdif.HEADER_SIZE) + vdif.HEADER_SIZE)

                    header.update(bytes(odata_8bit[h_start : h_start + vdif.HEADER_SIZE]))
                    oheader_8bit = header.to_dict()
                    header.update(bytes(odata_32bit[h_start : h_start + vdif.HEADER_SIZE]))
                    oheader_32bit = header.to_dict()
                    if oheader_8bit["word0"]["i"] != 0 and oheader_32bit["word0"]["i"] != 0:
                        self.assertDictEqual(oheader_8bit, oheader_32bit, f"Unequal header in packet {packet}")

                    opayload_8bit = bytes(odata_8bit[p_start : p_start + int(self._config["payload_size"])])
                    opayload_32bit = bytes(odata_32bit[p_start : p_start + int(self._config["payload_size"])])
                    if opayload_8bit != opayload_32bit and packet < 2:
                        a1 = np.frombuffer(opayload_8bit, dtype="int8")
                        a2 = np.frombuffer(opayload_32bit, dtype="int8")
                        print(packet, i, a1, a2)
                    # self.assertEqual(
                    #     bytes(odata_8bit[p_start : p_start + int(self._config["payload_size"])]),
                    #     bytes(odata_32bit[p_start : p_start + int(self._config["payload_size"])])
                    # )
                self.o_reader8bit.markCleared()
                self.o_reader32bit.markCleared()

    def _launch_vdif_packers(self):
        o_conf = dict(self._config)
        n_conf = dict(self._config)
        o_conf["input_key"] = self.i_buffer_8bit.key
        n_conf["input_key"] = self.i_buffer_32bit.key
        o_conf["output_key"] = self.o_buffer_8bit.key
        n_conf["output_key"] = self.o_buffer_32bit.key
        self._o_proc = ManagedProcess(get_vdifpacker_command_old(o_conf) + " --log_level debug", stdout_handler=sys.stdout)
        self._n_proc = ManagedProcess(get_vdifpacker_command(n_conf) + " --log_level debug", stdout_handler=sys.stdout)

    async def __create_test_env(self):
        """
        Create all buffers for the streaming mode
        """
        frame_size = self._config["payload_size"] + vdif.HEADER_SIZE
        nsample_packet = int(self._config["payload_size"] * 8 // 2)
        hdr_size = int(vdif.HEADER_SIZE * self._config["samples_per_block"] // nsample_packet)
        i_byte = int(self._config["samples_per_block"])
        o_byte = int(self._config["samples_per_block"] * 2 // 8 + hdr_size)
        print(self._config["samples_per_block"], i_byte, o_byte, self._config["samples_per_block"] * 2 // 8, hdr_size)
        # Create the buffers for the streaming test. Needs to be async!
        self.i_buffer_8bit = dada.DadaBuffer(int(i_byte), n_slots=4, pin=True)
        self.i_buffer_32bit = dada.DadaBuffer(int(i_byte*4), n_slots=4, pin=True)
        self.o_buffer_8bit = dada.DadaBuffer(int(o_byte+frame_size), n_slots=4, pin=True)
        self.o_buffer_32bit = dada.DadaBuffer(int(o_byte), n_slots=4, pin=True)


        tasks = []
        tasks.append(asyncio.create_task(self.i_buffer_8bit.create()))
        tasks.append(asyncio.create_task(self.i_buffer_32bit.create()))
        tasks.append(asyncio.create_task(self.o_buffer_8bit.create()))
        tasks.append(asyncio.create_task(self.o_buffer_32bit.create()))
        await asyncio.gather(*tasks)

        # Instantiate
        self.i_writer8bit = Writer(int(self.i_buffer_8bit.key, 16))
        self.i_writer32bit = Writer(int(self.i_buffer_32bit.key, 16))
        self.o_reader8bit = Reader(int(self.o_buffer_8bit.key, 16))
        self.o_reader32bit = Reader(int(self.o_buffer_32bit.key, 16))

        self.i_writer8bit.pinned()
        self.i_writer32bit.pinned()
        self.o_reader8bit.pinned()
        self.o_reader32bit.pinned()

        # self.disk_writer8bit = dada.DbDisk(self.o_buffer_8bit.key, directory=self._8bit_data_dir).start()
        # self.disk_writer32bit = dada.DbDisk(self.o_buffer_32bit.key, directory=self._32bit_data_dir).start()
        ts = str(int(time.time()))
        header = {
            'HEADER':'DADA',
            'HDR_VERSION':'1.0',
            'HDR_SIZE':'4096',
            'DADA_VERSION':'1.0',
            'FILE_SIZE':f"{o_byte}",
            'UTC_START':ts,
            'NBIT':'8',
            "OBS_OFFSET":"0",
            'RESOLUTION':'8',
            "BYTES_PER_SECOND":f"{self._config['sample_rate']}",
            'SYNC_TIME':ts,
            'SAMPLE_CLOCK_START':'0'
        }
        self.i_writer8bit.setHeader(header)
        self.i_writer32bit.setHeader(header)

        self._launch_vdif_packers()

        await asyncio.sleep(2)

    async def __destroy_test_env(self):
        """
        Destroy all buffers for the streaming mode
        """
        self.i_writer8bit.disconnect()
        self.i_writer32bit.disconnect()
        self.o_reader8bit.disconnect()
        self.o_reader32bit.disconnect()

        if self._o_proc:
            self._o_proc.terminate()
        if self._n_proc:
            self._n_proc.terminate()

        # self.disk_writer8bit.terminate()
        # self.disk_writer32bit.terminate()

        tasks = []
        tasks.append(asyncio.create_task(self.i_buffer_8bit.destroy()))
        tasks.append(asyncio.create_task(self.i_buffer_32bit.destroy()))
        tasks.append(asyncio.create_task(self.o_buffer_8bit.destroy()))
        tasks.append(asyncio.create_task(self.o_buffer_32bit.destroy()))
        await asyncio.gather(*tasks)


if __name__ == "__main__":
    unittest.main()
