"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description: Test the VDIF utils
"""
import unittest
import os
import numpy as np
from dbbc import vdif
from dbbc.testing import vdif_dummy_file, TEST_HEADERS

class Test_VDIF_utils(unittest.TestCase):

    def test_packing_unpacking_payload_1chan(self):
        data = (np.random.rand(8000, 1) * 4).astype(np.uint8)
        packed = vdif.pack2bit(data)
        unpacked = vdif.unpack2bit(packed, 1).transpose(1,0)
        self.assertTrue(np.all(data == unpacked))

    def test_packing_unpacking_payload_2chan(self):
        data = (np.random.rand(8000, 2) * 4).astype(np.uint8)
        packed = vdif.pack2bit(data)
        unpacked = vdif.unpack2bit(packed, 2).transpose(1,0)
        self.assertTrue(np.all(data == unpacked))

    def test_packing_unpacking_payload_4chan(self):
        data = (np.random.rand(8000, 4) * 4).astype(np.uint8)
        packed = vdif.pack2bit(data)
        unpacked = vdif.unpack2bit(packed, 4).transpose(1,0)
        self.assertTrue(np.all(data == unpacked))

    def test_packing_unpacking_payload_8chan(self):
        data = (np.random.rand(8000, 8) * 4).astype(np.uint8)
        packed = vdif.pack2bit(data)
        unpacked = vdif.unpack2bit(packed, 8).transpose(1,0)
        self.assertTrue(np.all(data == unpacked))

    def test_payload_size(self):
        """Should test if the payload size is correctly calculated
        ToDo: Test different configurations
        """
        self.assertEqual(8000, vdif.payload_size(32e6))
        self.assertEqual(5000, vdif.payload_size(1e6))

    def test_frames_per_second(self):
        """Should test if the payload size is correctly calculated
        ToDo: Test different configurations
        """
        self.assertEqual(1000, vdif.frames_per_second(32e6))
        self.assertEqual(4000, vdif.frames_per_second(32e6, 4))

    def test_frame_time_ns(self):
        """Should test if the payload size is correctly calculated
        ToDo: Test different configurations
        """
        self.assertEqual(1000000, vdif.frame_time_ns(32e6))
        self.assertEqual(20000000, vdif.frame_time_ns(1e6))

    def test_volume_size(self):
        """Should test if the payload size is correctly calculated
        ToDo: Test different configurations
        """
        self.assertEqual(8032000, vdif.volume(32e6, 1))
        self.assertEqual(10064000, vdif.volume(1e6, 10, 4))



class Test_VDIF_Header(unittest.TestCase):

    def setUp(self) -> None:
        self.tobj = vdif.Header()
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_to_dict_is_dict(self):
        """Should test if __dict__ returns a dictionary
        """
        self.assertTrue(isinstance(self.tobj.to_dict(), dict))

    def test_attr_after_update(self):
        """Should test if all attributes are correct assigned
        """
        for test_set in TEST_HEADERS.values():
            self.tobj.update(test_set["bytes"])
            self.assertDictEqual(self.tobj.to_dict(), test_set["expected"])
            self.assertEqual(self.tobj.to_bytes(), test_set["bytes"])

    def test_nchannel(self):
        """Should test if nchannel is correct assigned
        """
        for test_set in TEST_HEADERS.values():
            self.tobj.update(test_set["bytes"])
            self.assertEqual(self.tobj.nchannel, 4)

    def test_psize(self):
        """Should test if nsamples is correct assigned
        """
        for test_set in TEST_HEADERS.values():
            self.tobj.update(test_set["bytes"])
            self.assertEqual(self.tobj.psize, 8000)

    def test_nsamples(self):
        """Should test if nsamples is correct assigned
        """
        for test_set in TEST_HEADERS.values():
            self.tobj.update(test_set["bytes"])
            self.assertEqual(self.tobj.nsample, 8000)

    def test_timestamp(self):
        """Should test if timestamp is correct assigned
        """
        self.tobj.update(TEST_HEADERS["set1"]["bytes"])
        self.assertEqual(self.tobj.timestamp, 1682412435)
        self.tobj.update(TEST_HEADERS["set2"]["bytes"])
        self.assertEqual(self.tobj.timestamp, 1682412435)
        self.tobj.update(TEST_HEADERS["set3"]["bytes"])
        self.assertEqual(self.tobj.timestamp, 1682412436)



class Test_VDIF_Payload(unittest.TestCase):

    def setUp(self) -> None:
        self.tobj = vdif.Payload(8000, 4)
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_data_shape(self):
        """Should test if the shape of the data is correct
        """
        self.assertEqual(self.tobj.data.shape, (4, 8000))

    def test_update(self):
        """Should test if the passed byte array is correctly unpacked
        """
        data = (np.random.rand(8000, 4) * 4).astype(np.uint8)
        self.tobj.update(vdif.pack2bit(data))
        self.assertTrue(np.all(data == self.tobj.data.transpose(1,0)))

    def test_update(self):
        """Should test if the passed byte array is correctly unpacked
        """
        data = (np.random.rand(8000, 4) * 4).astype(np.uint8)
        packed = vdif.pack2bit(data)
        self.tobj.update(packed)
        self.assertTrue(np.all(data == self.tobj.data.transpose(1,0)))
        self.assertEqual(packed, self.tobj.to_bytes())
        self.assertEqual(8000, self.tobj.samples)

    def test_histogram(self):
        """Should test if the historgram of the payload is correct
        """
        data = np.ones((self.tobj.nsample, self.tobj.nchannel), dtype=np.uint8)
        expt = np.asarray([[0, self.tobj.nsample, 0, 0] for __ in range(self.tobj.nchannel)])
        self.tobj.update(vdif.pack2bit(data))
        self._proof_histogram(self.tobj.histogram, expt)

        data = np.zeros((self.tobj.nsample, self.tobj.nchannel), dtype=np.uint8)
        expt = np.asarray([[self.tobj.nsample, 0, 0, 0] for __ in range(self.tobj.nchannel)])
        self.tobj.update(vdif.pack2bit(data))
        self._proof_histogram(self.tobj.histogram, expt)

        data = np.zeros((self.tobj.nsample, self.tobj.nchannel), dtype=np.uint8)
        expt = np.asarray([[self.tobj.nsample // 2, self.tobj.nsample // 2, 0, 0]
                for __ in range(self.tobj.nchannel)])
        data[:4000] = 1
        self.tobj.update(vdif.pack2bit(data))
        self._proof_histogram(self.tobj.histogram, expt)


    def _proof_histogram(self, histogram: np.ndarray, expected: np.ndarray) -> bool:
        for i in range(histogram.shape[0]):
            for ii in range(histogram.shape[1]):
                self.assertEqual(self.tobj.histogram[i, ii], expected[i, ii])



class Test_VDIF_Reader(unittest.TestCase):

    DUMMY_FILE = "/tmp/vdif_dummy.dat"
    SAMPLE_RATE = 1e6
    FRAMES=101
    @classmethod
    def setUpClass(cls) -> None:
        print("Creating dummy file")
        vdif_dummy_file(cls.FRAMES, fname=cls.DUMMY_FILE, fs=cls.SAMPLE_RATE)
        return super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        print("Removing dummy file")
        os.remove(cls.DUMMY_FILE)
        return super().tearDownClass()

    def setUp(self) -> None:
        self.fstream = open(self.DUMMY_FILE, "rb")
        self.tobj = vdif.Reader(self.fstream)
        return super().setUp()

    def tearDown(self) -> None:
        self.fstream.close()
        return super().tearDown()

    def test_fps_estimation(self):
        """Should test if the frames per seconds is correct
        """
        ptr = self.tobj.tell()
        expt =  vdif.frames_per_second(self.SAMPLE_RATE)
        self.assertEqual(self.tobj.frames_per_second, expt)
        self.assertEqual(ptr, self.tobj.tell()) # Also check that the stream pointer is at the original position

    def test_sample_rate_estimation(self):
        """Should test if the sample rate is correct
        """
        ptr = self.tobj.tell()
        self.assertEqual(self.tobj.sample_rate, self.SAMPLE_RATE)
        self.assertEqual(ptr, self.tobj.tell()) # Also check that the stream pointer is at the original position

    def test_seek_frame(self):
        """Should test if a frame is successfully found
        """
        self.tobj.seek_frame(1, 43)
        header = self.tobj.get_header()
        self.assertEqual(1, header.seconds_from_epoch)
        self.assertEqual(43, header.frame_id)

    # def test_seek_frame_stream_pointer_beyond(self):
    #     """Should test if a frame is successfully found also when the pointer is beyond the
    #     the frame to be seeked
    #     """
    #     self.tobj.seek_frame(1, 43)
    #     self.tobj.seek_frame(0, 15)
    #     header = self.tobj.get_header()
    #     self.assertEqual(0, header.seconds_from_epoch)
    #     self.assertEqual(15, header.frame_id)

    def test_read_data_against_iteration(self):
        """_summary_
        """
        it_data = np.zeros((self.tobj.nchannel, self.tobj.nsample * self.FRAMES), dtype=np.int8)
        for i, (__, pay) in enumerate(self.tobj):
            it_data[:, i*self.tobj.nsample : (i+1)*self.tobj.nsample] = pay.data
        self.tobj.reset()
        al_data = self.tobj.read_data()
        self.assertTrue(np.all(al_data == it_data))

    def test_seek_frame_non_existing_frame(self):
        """Should test if an RuntimeError is raised when a frame is not in the stream
        """
        with self.assertRaises(RuntimeError):
            self.tobj.seek_frame(2, 43)

    def test___next__(self):
        """Should test if the iteration function returns expected objects and stops after FRAMES
        """
        for header, payload in self.tobj:
            self.assertIsInstance(header, vdif.Header)
            self.assertIsInstance(payload, vdif.Payload)
        self.assertEqual(self.FRAMES, self.tobj.call_cnt)

    def test_read_payload_stream_pointer(self):
        """Should test if the stream pointer is at the expected position after reading the payload
        """
        self.assertIsInstance(self.tobj.get_payload(), vdif.Payload)
        self.assertEqual(self.tobj.header.data_frame_len*8, self.tobj.tell())
        self.assertIsInstance(self.tobj.get_payload(), vdif.Payload)
        self.assertEqual(self.tobj.header.data_frame_len*8*2, self.tobj.tell())

    def test_read_header_stream_pointer(self):
        """Should test if the stream pointer is at the expected position after reading the header
        """
        self.assertIsInstance(self.tobj.get_header(), vdif.Header)
        self.assertEqual(vdif.HEADER_SIZE, self.tobj.tell())
        self.assertIsInstance(self.tobj.get_header(), vdif.Header)
        self.assertEqual(self.tobj.header.data_frame_len*8 + vdif.HEADER_SIZE, self.tobj.tell())

    def test_read_payload_eof(self):
        """Should test if False is returned when EOF is reached
        """
        self.tobj.seek(0, 2)
        self.assertFalse(self.tobj.get_payload())

    def test_read_header_eof(self):
        """Should test if False is returned when EOF is reached
        """
        self.tobj.seek(0, 2)
        self.assertFalse(self.tobj.get_header())



if __name__ == '__main__':
    unittest.main()
