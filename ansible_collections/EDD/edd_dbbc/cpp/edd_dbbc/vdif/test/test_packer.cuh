#ifndef TEST_VDIF_PACKER_CU
#define TEST_VDIF_PACKER_CU


#include <gtest/gtest.h>
#include <numeric>

#include <psrdada_cpp/testing_tools/tools.cuh>

#include "edd_dbbc/cuerror.cuh"
#include "edd_dbbc/vdif/packer.cuh"
#include "edd_dbbc/testing_tools.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace vdif {
namespace test {

class Test_Packer
{
public:
    Test_Packer(std::size_t size, float threshold)
    : _size(size), _threshold(threshold)
    {
        h_idata = psr_testing::random_vector<float>(0, 32, _size);
        d_idata = h_idata;
        h_odata.resize(_size / 16);
        d_odata.resize(_size / 16);
        CUDA_ERROR_CHECK(cudaStreamCreate(&_stream));
    }
    ~Test_Packer()
    {
        CUDA_ERROR_CHECK(cudaStreamDestroy(_stream));
    }

    void set_input(thrust::host_vector<float> &vec)
    {
        h_idata = vec;
        d_idata = h_idata;
    }

    void cpu_process()
    {
        float mean = tools::mean(d_idata);
        float sigma2 = tools::variance(d_idata, mean);
        float vsigma = _threshold * sqrt(sigma2);
        uint32_t clip;uint8_t var; // The 2bit container
        std::size_t npack = 16; // A single uint32_t value contains 16x2bit values
        // toDo: assert(data % 16 == 0)
        for(std::size_t i = 0; i < h_idata.size(); i+=npack)
        {
            clip = 0; //
            for(std::size_t j = 0; j < npack; j++)
            {
                h_idata[i + j] -= mean;
                var = 0;
                // Compute the two bits                                      -> 0 0
                var += (h_idata[i + j] >= (-1. * vsigma)); // greater than -std    -> 0 1
                var += (h_idata[i + j] >= (0.)); // greater than mean              -> 1 0
                var += (h_idata[i + j] >= (vsigma)); // greater than + std         -> 1 1 highest value
                // h_idata[j=15] gets the 2 MSB; h_idata[j=0] gets the 2 LSB
                clip += (var << (j * 2)); //
            }
            h_odata[i / npack] = clip;
        }
        tested = true;
    }

    void gpu_process()
    {
        vdif::Packer packer(_threshold);
        packer.process(&d_idata, &d_odata, _stream);
    }

    void test()
    {
        cpu_process();
        gpu_process();
        CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream));
        thrust::host_vector<uint32_t> gpu_out = d_odata;
        for(std::size_t i=0; i < gpu_out.size(); i++){
            // Compare results
            ASSERT_TRUE(h_odata[i] == gpu_out[i])
                << "CPU and GPU results for index " << i << " unequal. CPU = "
                << h_odata[i] << "; GPU = " << gpu_out[i];// << " GPU old = " << gpu_out2[i];
        }
    }

    thrust::host_vector<uint32_t> get_output()
    {
        if(!tested){cpu_process();}
        return h_odata;
    }
    thrust::host_vector<float> get_input()
    {
        return h_idata;
    }


private:
    const std::size_t _size;
    float _threshold;
    cudaStream_t _stream;
    bool tested = false;
    thrust::host_vector<float> h_idata;
    thrust::host_vector<uint32_t> h_odata;
    thrust::device_vector<float> d_idata;
    thrust::device_vector<uint32_t> d_odata;
};

TEST(Packer, test_against_reference_32768)
{
    Test_Packer tester = Test_Packer(32768, 0.5);
    tester.test();
};
TEST(Packer, test_against_reference_65536)
{
    Test_Packer tester = Test_Packer(65536, 5.9);
    tester.test();
};
TEST(Packer, test_against_reference_131072)
{
    Test_Packer tester = Test_Packer(131072, 1.2);
    tester.test();
};

}
}
}

#endif //TEST_VDIF_PACKER_CU