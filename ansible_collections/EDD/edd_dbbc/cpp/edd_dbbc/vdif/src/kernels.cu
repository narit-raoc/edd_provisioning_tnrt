namespace edd_dbbc {
namespace vdif {


__global__
void pack2bit_non_linear(
  const float *__restrict__ input,
  uint32_t *__restrict__ output,
  size_t width,
  float v,
  float sigma2,
  float mean)
{
  // number of values to pack into one output element, use 32 bit here to
  // maximize number of threads
  const uint8_t NPACK = 32 / 2;

  __shared__ uint32_t tmp[1024];
  const float vsigma = v * sqrt(sigma2);

  for (uint32_t i = NPACK * blockIdx.x * blockDim.x + threadIdx.x;
       (i < width); i += blockDim.x * gridDim.x * NPACK)
  {
    tmp[threadIdx.x] = 0;

    #pragma unroll
    for (uint8_t j = 0; j < NPACK; j++)
    {
      // Load new input value, clip and convert to Nbit integer
      const float inp = input[i + j * blockDim.x] - mean;

      uint32_t p = 0;
      p += (inp >= (-1. * vsigma));
      p += (inp >= (0));
      p += (inp >= (1. * vsigma));
      // this is more efficient than fmin, fmax for clamp and cast.

      // store in shared memory with linear access
      tmp[threadIdx.x] += p << (2 * j);
    }
    __syncthreads();

    // load value from shared memory and rearange to output - the read value is
    // reused per warp
    uint32_t out = 0;

    // bit mask: Thread 0 always first input_bit_depth bits, thread 1 always
    // second input_bit_depth bits, ...
    const uint32_t mask = ((1 << 2) - 1) << (2 * (threadIdx.x % NPACK));

    #pragma unroll
    for (uint8_t j = 0; j < NPACK; j++)
    {
      uint32_t v = tmp[(threadIdx.x / NPACK) * NPACK + j] & mask;
      // retrieve correct bits
      v = v >> (2 * (threadIdx.x % NPACK));
      v = v << (2 * j);
      out += v;
    }

    std::size_t oidx = threadIdx.x / NPACK
      + (threadIdx.x % NPACK) * (blockDim.x / NPACK)
      + (i - threadIdx.x) / NPACK;
    output[oidx] = out;

    __syncthreads();

  }
}

}
}
