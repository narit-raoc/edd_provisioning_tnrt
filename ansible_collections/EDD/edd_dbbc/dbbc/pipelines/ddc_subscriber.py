"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

Example pipeline on how to subscribe to a DigitalDownConverter. The pipeline expectes data
in the DigitalDownConverter:1-format.
"""
# pylint: disable=duplicate-code
# python natives
import asyncio
# third party
from aiokatcp.sensor import Sensor
# EDD libs
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.utils import numa
from mpikat.utils.spead_capture import SpeadCapture, SpeadPacket
import mpikat.core.logger as logging

from dbbc import data_streams as ds

_LOG = logging.getLogger("mpikat.dbbc.pipelines.ddc_subscriber")

"""
Define a default configuration in a python dicitionary.
The dicitionary should provide all required information to operate the pipeline
"""
_DEFAULT_CONFIG = {

    "id": "DDCSubscriber",
    "type": "DDCSubscriber",
    "n_heaps_per_block": 16384,
    "input_data_streams":
    [
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.123",
            "port": "8125"
        },
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.124",
            "port": "8125"
        },
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.127",
            "port": "8125"
        },
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.128",
            "port": "8125"
        }
    ],
    "spead_config": SpeadCapture.DEFAULT_CONFIG,
    # User params
    "output_type": 'disk',
    "file_size":0,
    "output_directory": "/mnt/",
    # Debug / expert params
    "log_level": "debug"
}

def on_heaps_received(p: SpeadPacket):
    """ Function which is called by SpeadCapture when a heap is received

    Args:
        p (SpeadPacket): the heap as instance of a SpeadPacket
    """
    _LOG.debug("Received heap with timestamp %d and channel %d", p.timestamp, p.channel)

class DDCSubscriber(EDDPipeline):
    """
    Description:
        The DDCSubscriber pipeline is an example pipeline on how to subscribe to
        DigitalDownconverter Streams. Hence the only accepted DATA_FORMATS is the
        DDCDataStreamFormat.
    """

    DATA_FORMATS = [ds.DDCDataStreamFormat.stream_meta]

    def __init__(self, ip: str, port: int, loop: asyncio.AbstractEventLoop=None):
        """
        Description:
            Construct the DDCSubscriber pipeline.

        Args:
            - ip (str): The ip of the KATCP server
            - port (int): The port of the KATCP server
            - loop (asyncio.AbstractEventLoop, optinal): Defaults to None
        """
        # Call to the base class for constructing the pipeline
        super().__init__(ip, port, default_config=dict(_DEFAULT_CONFIG), loop=loop)
        self._numa_node = None
        self._capture = None
        _LOG.info("DDCSubscriber pipeline instantiated")

    def setup_sensors(self):
        """Setups KATCP sensors. You can define as many sensor as you like
        """
        # Call to the base class for setting up common sensors
        super().setup_sensors()
        # The aiokatcp.DeviceServer owns a set() of sensors (self.sensors)
        self.sensors.add(Sensor(str, "example-name", description="I am an example sensor"))

    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """Configure the EDD VLBi pipeline
        """
        _LOG.info("Configuring DDCSubscriber processing")
        # Initialize the spead capturing
        # We use mpikat.utils.numa to get the IP address of the fastest
        # network interface card / network device
        nic_name, nic_desc = numa.getFastestNic()
        _LOG.info(
            "Capturing on interface %s, ip %s, speed %s Mbit/s",
            nic_name, nic_desc["ip"], nic_desc["speed"]
        )
        # The mpikat.utils.speadcapture.SpeadCapture class requires the NIC IP
        # and the number of CPU cores to ensure threaded capturing
        self._config["spead_config"]["interface_address"] = nic_desc["ip"]
        self._config["spead_config"]["numa_affinity"] = numa.getInfo()[nic_desc['node']]['cores']
        # Instantiate the a DDCSpeadHandler object to provide spead2 the necessary information
        # about the used spead format. Then we can instantiate a SpeadCapture object to handle
        # the spead heaps. The passed function 'on_heaps_received' is used to access the spead data.
        # This function is the 'user-space'-function and called whenever a spead heap is received
        spead_handler = ds.DDCSpeadHandler()
        self._capture = SpeadCapture(self._config["input_data_streams"], spead_handler,
                                     on_heaps_received, self._config["spead_config"])
        _LOG.debug("Pipeline is configured")


    @state_change(target="streaming", allowed=["configured"], intermediate="configured")
    async def capture_start(self):
        """start streaming output
        """
        # Here we start the actual netowrk capturing of the spead heaps
        if isinstance(self._capture, SpeadCapture):
            self._capture.start()
        _LOG.debug("Started capturing")


    @state_change(target="idle", allowed=["streaming"], intermediate="streaming")
    async def capture_stop(self):
        """stop streaming of data
        """
        self.deconfigure()

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """deconfigure the pipeline.
        """
        self._destroy_capture()

    def _destroy_capture(self):
        """Destroys the capture threads
        """
        _LOG.debug("Cleaning up capture thread")
        if isinstance(self._capture, SpeadCapture):
            if self._capture.is_alive():
                self._capture.stop()
                self._capture.join(10)
                self._capture = None
        _LOG.debug("Capture thread cleaned")

def main_cli():
    """The main function wrapper used to build a CLI
    """
    asyncio.run(launchPipelineServer(DDCSubscriber))

if __name__ == "__main__":
    main_cli()
