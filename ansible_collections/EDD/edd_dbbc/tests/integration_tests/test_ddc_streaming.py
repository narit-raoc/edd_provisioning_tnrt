import asyncio
import unittest

import numpy as np
import scipy.signal as ss
from psrdada import Reader, Writer
from mpikat.utils import dada_tools as dada
from mpikat.core.logger import getLogger

from dbbc import ddc
from dbbc import filter

LOG = getLogger("mpikat.dbbc.tests.test_ddc_algorithm")
NBUFFER = 16 # used for stream test

# ---------------- #
# Helper functions #
# ---------------- #

def get_atol(dtype):
    """
    Acceptable absolute tolerance for sample comparsion
    f32 lower tolerance as f64
    """
    if dtype=="float32":
        return 1e-2
    elif dtype=="float64":
        return 1e-6

def get_rtol(dtype):
    """
    Acceptable absolute tolerance for sample comparsion
    f32 lower tolerance as f64
    """
    if dtype=="float32":
        return 1e-1
    elif dtype=="float64":
        return 1e-4

# # ------------------------------------------------------------- #
# # Setup a common base class (used for non-stream and streaming) #
# # ------------------------------------------------------------- #
class Test_DDC_Stream(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        """
        Setup the base envoroment for the tests
        """
        self.conf = self.get_config()

        self.lcm = np.lcm(self.conf['f_in'], self.conf['f_out'])
        self.up = int(self.lcm // self.conf['f_in'])
        self.dw = int(self.lcm // self.conf['f_out'])
        self.duration = self.conf['nsamples'] / self.conf['f_in']
        self.hilbert_fir = self.conf['hilbert']
        self.input_type = np.dtype(self.conf['input_dtype'])
        self.output_type = np.dtype(self.conf['output_dtype'])
        self.h_coeff_r = filter.hilbert_fir(**self.hilbert_fir)
        self.h_coeff_i = filter.hilbert_fir(**self.hilbert_fir, reverse=True)

    def get_config(self):
        """
        Overwrite function for test with different configs
        """
        return {
            "f_in":int(1.024e9),
            "f_out":int(64e6),
            "f_lo":[201e6, 312e6, 421e6],
            "nsamples":1048576*8,
            "window":('kaiser', 5.0),
            "hilbert": filter.default_hilbert,
            "input_dtype":"int8",
            "output_dtype":"float32"
        }

    def _reference_test(self):
        """
        CPU based reference test
        """
        # 1. Generate the time vector
        # 2. Generate input signal
        # 3. Generate local oscillations
        # 4. Do the actual DDC processing on CPU
        time = np.arange(self.conf['nsamples']) / self.conf["f_in"]

        # Generating input signal with chirp()
        # input = np.asarray(ss.chirp(time, 1, self.duration,
        #     self.conf['f_in']//2)*127,dtype=self.conf["input_dtype"])
        input = np.random.randint(-127, 127, len(time))

        osci = np.zeros((len(self.conf['f_lo']), len(time)),
            dtype=ddc.get_complex_type(self.output_type))
        for i, lo in enumerate(self.conf['f_lo']):
            osci[i] = np.exp(-2j * np.pi * lo * time)

        mx = input * osci
        rs = ss.resample_poly(mx, self.up, self.dw,
            window=self.conf['window'], axis=1)
        re = ss.lfilter(self.h_coeff_r, 1, rs.real)
        im = ss.lfilter(self.h_coeff_i, 1, rs.imag)
        ls = re + im
        us = re - im
        output = np.asarray([ls, us], dtype=self.output_type).transpose(1,0,2)
        return output, input

    async def asyncSetUp(self) -> None:
        """
        asynchronous setUp function creates dada buffer and Reader/Writer
        """
        super().setUp()
        LOG.info(f"Testing with class {self.__class__.__name__}")
        self.i_sample = int(self.conf['nsamples'] // NBUFFER)
        self.o_sample = int(self.duration / NBUFFER * self.conf['f_out'])

        self.processor = ddc.DDC(**self.conf)
        self.processor.set('_nsamples', self.i_sample)
        self.processor.setup()

        await self.__create_buffers()

    async def asyncTearDown(self) -> None:
        """
        asynchronous tearDown function destroys dada buffer and Reader/Writer
        """
        super().tearDown()
        await self.__destroy_buffers()

    def _do_stream_processing(self, input: np.ndarray, output: np.ndarray):
        """
        Functions that executes the the DDC pipeline in a streaming environment

        Args:
            input (np.ndarray):
            output (np.ndarray):
        """
        mem_out = np.asarray(self.o_writer.getNextPage())
        # Iterate over the buffers to simulate a streaming behavior
        for run in range(NBUFFER):
            # 1. Get memory location of input buffers
            # 2. Write input signal into the input dada buffer
            # 3. mark the block as filled, so that the reader can access it
            # 4. process the block with the DDC instance and mark the block as cleared
            mem_in = np.asarray(self.i_writer.getNextPage()).view(self.input_type)
            mem_in[:] = input[self.i_sample*run:self.i_sample*(run+1)]
            self.i_writer.markFilled()
            raw_data = np.asarray(self.i_reader.getNextPage()).view(self.input_type)
            result = self.processor.process(raw_data.ctypes.data, mem_out.ctypes.data)
            self.i_reader.markCleared()

            # Handle the first dada block -> The first block is processed as soon
            # as the second block is available, because of padding.
            # Continue with the next iteration step. the DDC.process()-function
            # returns "None" if there are no output data.
            if result == False:
                if run == 0:
                    header = self.i_reader.getHeader()
                    self.o_writer.setHeader(header)
                continue
            # For block 2 and beyond we get GPU results
            # Now we take the data from the output buffer by a seond reader
            # 1. Mark the buffer as filles
            # 2. Get the data from the marked DADA block
            # 3. Read the rawbytes and append it to the 'gpu'-array
            # 4. Mark the current block as cleared
            # 5. Acquire the next DADA block of the output buffer
            # start = (run-3)*self.o_sample
            # stop = (run-2)*self.o_sample
            start = (run-2)*self.o_sample
            stop = (run-1)*self.o_sample

            self.o_writer.markFilled()
            output[..., start:stop] = np.frombuffer(
                self.o_reader.getNextPage(), dtype=self.output_type
            ).reshape((len(self.conf['f_lo']),2,self.o_sample))

            self.o_reader.markCleared()
            mem_out = np.asarray(self.o_writer.getNextPage())

            # If we reached the last iteration of the loop we will squeeze the
            # last blocks out of the GPU by passing a zero-array to the DDC.proces-
            # function.
            # 1. Get the memory location of the last DADA block
            # 2. Pass the zero array to the processor and get the last GPU result
            # 3. Copy the GPU results to this location and mark it as filled
            # 4. Read the last DADA blocks from the output buffer
            # 5. Assign it to the 'gpu'-array
            # 6. mark as cleared
            # 7. Do it twice
            if run < NBUFFER - 1:
                continue

            pad = np.zeros(self.i_sample, dtype=self.input_type)
            for i in range(-1,1):
                self.processor.process(pad.ctypes.data, mem_out.ctypes.data)
                self.o_writer.markFilled()
                output[..., (run+i)*self.o_sample:(run+1+i) *self.o_sample] = \
                    np.frombuffer(self.o_reader.getNextPage(),
                        dtype=self.output_type)\
                        .reshape((len(self.conf['f_lo']),2,self.o_sample))
                self.o_reader.markCleared()
                mem_out = np.asarray(self.o_writer.getNextPage())

        self.o_reader.markCleared()

    def test_ddc_stream_against_reference(self):
        """
        Testing the DDC algorithm in streaming mode against a CPU reference implementation
        """
        # Create the arrays which are compared; one for the CPU and one for GPU
        cpu, input = super()._reference_test()
        gpu = np.zeros(cpu.shape, cpu.dtype)
        self._do_stream_processing(input, gpu)

        # plot(cpu, gpu, self.__class__.__name__, show=True)
        # The first samples (N= FIR-taps) are excluded
        self.assertTrue(np.allclose(
            cpu[...,self.hilbert_fir['ntaps']*2:],
            gpu[..., self.hilbert_fir['ntaps']*2:],
            atol=get_atol(self.output_type),
            rtol=get_rtol(self.output_type)
        ), "Output is not equal")

    async def __create_buffers(self):
        """
        Create all buffers for the streaming mode
        """
        i_byte = int(self.i_sample * self.input_type.itemsize)
        o_byte = int(self.o_sample * self.output_type.itemsize * 2 * len(self.conf['f_lo']))

        # Create the buffers for the streaming test. Needs to be async!
        self.i_buffer = dada.DadaBuffer(i_byte, key="dada", n_slots=4)
        await self.i_buffer.create()
        self.o_buffer = dada.DadaBuffer(o_byte, key="dadc", n_slots=4)
        await self.o_buffer.create()

        # Instantiate
        self.i_writer = Writer(int(self.i_buffer.key, 16))
        self.i_reader = Reader(int(self.i_buffer.key, 16))
        self.o_writer = Writer(int(self.o_buffer.key, 16))
        self.o_reader = Reader(int(self.o_buffer.key, 16))
        header = {
            'HEADER':'DADA',
            'HDR_VERSION':'1.0',
            'HDR_SIZE':'4096',
            'DADA_VERSION':'1.0',
            'FILE_SIZE':'unset',
            'UTC_START':'unset',
            'NBIT':'8',
        }
        self.i_writer.setHeader(header)

    async def __destroy_buffers(self):
        """
        Destroy all buffers for the streaming mode
        """
        self.i_writer.disconnect()
        self.i_reader.disconnect()
        self.o_writer.disconnect()
        self.o_reader.disconnect()

        tasks = []
        tasks.append(asyncio.create_task(self.i_buffer.destroy()))
        tasks.append(asyncio.create_task(self.o_buffer.destroy()))
        await asyncio.gather(*tasks)


if __name__ == "__main__":
    unittest.main()
