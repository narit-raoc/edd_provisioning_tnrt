#pragma once

#include <psrdada_cpp/cli_utils.hpp>
#include "edd_dbbc/ddc/ddc.cuh"

namespace edd_dbbc {
namespace ddc {

/**
 * @brief Pipeline configuration contains interface information (e.g. dada buffer),
 *        the DDC configuration and its substructs.
 *        Attributes are private to capsulate the data
 */
class pipe_config
{

private:
    // The input DADA key
    key_t i_key = psrdada_cpp::string_to_key("dada");
    // The output DADA key
    key_t o_key = psrdada_cpp::string_to_key("dadc");
    // Bit size of the input data for 8,10,12 or 32 for int or float, respectivly
    std::size_t _nbit = 32;
    // The byte size of the input buffer
    std::size_t _isize = 128000;
    // The byte size of the output buffer
    std::size_t _osize = 32000;
    // The number of samples per channel batch. Set to zero for continious time dimension
    std::size_t _batch_size = 0;
public:
    /**
     * @brief Set the key of the input buffer
     *
     * @param key_t
     */
    void input_key(key_t val){i_key = val;}
    /**
     * @brief Set the key of the output buffer
     *
     * @param key_t
     */
    void output_key(key_t val){o_key = val;}
    /**
     * @brief Set the bit size of an input sample
     *
     * @param key_t
     */
    void nbit(std::size_t val){_nbit = val;}
    /**
     * @brief Set the size of the input buffer
     *
     * @param std::size:t
     */
    void input_size(std::size_t val){_isize = val;}
    /**
     * @brief Set the size of the output buffer
     *
     * @param std::size:t
     */
    void output_size(std::size_t val){_osize = val;}
    /**
     * @brief Set the number of samples per channel batch
     *
     * @param std::size:t
     */
    void batch_size(std::size_t val){_batch_size = val;}
    /**
     * @brief Get the key of the input buffer
     *
     * @param key_t
     */
    key_t input_key(){return i_key;}
    /**
     * @brief Get the key of the output buffer
     *
     * @param key_t
     */
    key_t output_key(){return o_key;}
    /**
     * @brief Get the bit size of an input sample
     *
     * @param key_t
     */
    size_t nbit(){return _nbit;}
    /**
     * @brief Get the size of the input buffer
     *
     * @param std::size:t
     */
    size_t input_size(){return _isize;}
    /**
     * @brief Get the size of the output buffer
     *
     * @param std::size:t
     */
    size_t output_size(){return _osize;}
    /**
     * @brief Get the number of samples per channel batch
     *
     * @param std::size:t
     */
    size_t batch_size(){return _batch_size;}
    /**
     * @brief Get the number of output samples per buffer
     *
     * @return size_t
     */
    size_t osamples(){return _osize / sizeof(float);}
    /**
     * @brief Get the number of input samples per buffer
     *
     * @return size_t
     */
    size_t isamples(){return _isize * 8 / nbit();}
    /**
     * @brief Construct a new pipe config object
     *
     * @param ddc_conf the DDC configuration
     * @param nbit bit size of input samples
     * @param isize size of the input buffer in bytes
     * @param osize size of the output buffer in bytes
     * @param batch batch size
     */
    pipe_config(ddc_t ddc_conf={0, 32000, 4000, "8000", true, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},
      std::size_t nbit=32,
      std::size_t isize=128000,
      std::size_t osize=32000,
      std::size_t batch=0)
      :
      ddc(ddc_conf),
      _nbit(nbit),
      _isize(isize),
      _osize(osize),
      _batch_size(batch){};

    // The DDC configuration
    ddc_t ddc;
};

}
}