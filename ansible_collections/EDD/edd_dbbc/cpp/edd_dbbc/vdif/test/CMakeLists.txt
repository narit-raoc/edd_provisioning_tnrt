include_directories(${GTEST_INCLUDE_DIR})

set(GTEST_LIBRARIES gtest_main gtest)

set(gtest_vdif_src
    gtest_vdif.cu
)

set(gtest_vdif_inc
    test_header.h
    test_packer.cuh
    test_pipeline.cuh
)

cuda_add_executable(gtest_vdif ${gtest_vdif_src})
target_link_libraries(gtest_vdif ${DEPENDENCY_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_vdif gtest_vdif --test_data "${CMAKE_CURRENT_LIST_DIR}/data")

set_tests_properties(gtest_vdif PROPERTIES TIMEOUT 120)
add_custom_target(vdif_tests COMMAND ${CMAKE_CTEST_COMMAND} -V)