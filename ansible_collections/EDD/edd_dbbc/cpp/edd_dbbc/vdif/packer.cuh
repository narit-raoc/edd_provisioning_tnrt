#pragma once

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/raw_bytes.hpp>
// CUDA dependencies
#include <cuda.h>
#include <thrust/device_vector.h>

#include "edd_dbbc/math_tools.cuh"
#include "edd_dbbc/cuerror.cuh"

namespace edd_dbbc {
namespace vdif {

const std::size_t n_threads = 1024;
const std::size_t n_blocks = 128;

/**
 * CUDA Kernel for conversion from float to 2-bit data
*/
__global__ void pack2bit_non_linear(
  const float *__restrict__ input,
  uint32_t *__restrict__ output,
  std::size_t width,
  float v,
  float sigma2,
  float mean
);

class Packer
{
public:
    Packer(float digitizer_threshold);
    ~Packer();

    void process(
        thrust::device_vector<float>* idata,
        thrust::device_vector<uint32_t>* odata,
        cudaStream_t stream
    );
private:
    float _digitizer_threshold = 1;
};


}
}
#include "edd_dbbc/vdif/src/packer.cu"
#include "edd_dbbc/vdif/src/kernels.cu"