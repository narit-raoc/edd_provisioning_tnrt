import numpy as np
from mpikat.core.logger import getLogger

LOG = getLogger("mpikat.dbbc.utils.ddc")

def get_complex_type(dtype):
    """
    Returns a the complex correspondend of the passed dtype
    """
    if dtype == "float64":
        return "complex128"
    return "complex64"

def get_nyquist_zone(fs: float, fc: float) -> float:
    """
    Description:
    ------------
        Returns the nyquist zone

    Parameters:
    -----------
        fs (float): The sample frequency
        fc (float); The center frequency of the band
    """
    return np.ceil(2 * fc / fs)

def get_center_frequency(f_lo: float, fs_out: float, f_center: float=0, fs_in: float=0) -> tuple:
    """
    Description:
    ------------
        Returns the center frequencies for lower and upper sidebands

    Parameters:
    -----------
        f_lo (float): Frequency / Tone of the local oscillator
        bw_out (float): Output bandwidth of the DDC
        f_center (float): (optional) Center/Sky frequency of the received signal
        fs_in (float): (optional) sampling rate of the input signal

    Return:
        returns a tuple where the first item is the lower and the second the upper
        center frequency of the sidebands
    """
    if f_center != 0 and fs_in != 0:
        fc_low = fs_in / 2 * np.floor(2 * f_center / fs_in) + f_lo - fs_out / 4
        fc_up = fs_in / 2 * np.floor(2 * f_center / fs_in) + f_lo + fs_out / 4
        return fc_low, fc_up
    return f_lo - fs_out / 4, f_lo + fs_out / 4

def get_start_stop_frequencies(f_lo: float, fs_out: float, f_center: float=0, fs_in: float=0) -> list:
    """
    Description:
    ------------
        Returns the center frequencies for lower and upper sidebands

    Parameters:
    -----------
        f_lo (float): Frequency / Tone of the local oscillator
        bw_out (float): Output bandwidth of the DDC
        f_center (float, optional): Center/Sky frequency of the received signal
        fs_in (float, optional): sampling rate of the input signal

    Return:
        returns a list of tuples the first list item contains the start and stop
        of the LSB and second item the start and stop of the USB
    """
    fc_low, fc_up = get_center_frequency(f_lo, fs_out, f_center, fs_in)
    return [(fc_low - fs_out / 4, fc_low + fs_out / 4), (fc_up - fs_out / 4, fc_up + fs_out / 4)]

def get_oscillation_frequencies(f_center: list, bandwidth: float, upper: bool=True) -> list:
    """
    Description:
    ------------
        Returns a list of local oscillation corresponding to the desired center
        frequency and bandwidth

    Parameters:
    -----------
        f_center (list): desired center frequency of the baseband (!)
        bandwidth (float): bandwidth of the channel
        upper (bool, optional): If True aligns the USB, otherwise the LSB. Defaults to True.

    Returns:
        list: _description_
    """
    if not isinstance(f_center, list):
        f_center = [f_center]
    lo = []
    for fc in f_center:
        if upper:
            lo.append(fc - bandwidth / 2)
        else:
            lo.append(fc + bandwidth / 2)
    return lo

def downsample(f_in: int, f_out: int) -> int:
    return int(np.lcm(int(f_in), int(f_out)) // f_out)

def upsample(f_in: int, f_out: int) -> int:
    return int(np.lcm(int(f_in), int(f_out)) // f_in)

def get_nsamples(f_in: int, f_out: int, heap_size: int=2048, mini: int=2**20) -> int:
    """_summary_

    Args:
        f_in (int): The input sampling rate
        f_out (int): The output sampling rate
        heap_size (int, optional): The heap size / batch size of incoming heaps. Defaults to 2048.
        mini (int, optional): Minimum number of sampls per buffer slot. Defaults to 2**20.

    Returns:
        _type_: _description_
    """
    up = upsample(f_in, f_out)
    dw = downsample(f_in, f_out)
    nsamples = np.lcm(np.lcm(up, dw), int(heap_size))
    while nsamples < mini:
        nsamples *= 2
    return int(nsamples)


def volume(sample_rate: int, duration: int, nchannel: int=1, nbit: int=32, packet_size: int=0) -> int:
    """Computes the the data volume in bytes for a given duration

    Args:
        sample_rate (int): Sample rate
        duration (int): Duration in seconds
        nchannel (int, optional): Number of channels. Defaults to 1.
        nbit (int, optional): Bit depth of a sample. Defaults to 32.
    """
    if packet_size:
        size = int(duration * sample_rate * nchannel * nbit / 8)
        return ((size + packet_size - 1) // packet_size) * packet_size
    return int(duration * sample_rate * nchannel * nbit / 8)
