from traitlets.config import Config
import nbformat as nbf
from nbconvert.exporters import PDFExporter
from nbconvert.preprocessors import TagRemovePreprocessor
import os

import argparse
from argparse import RawTextHelpFormatter


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='options', formatter_class=RawTextHelpFormatter)
    parser.add_argument('--notebook_file', '-i',
        action = "store",
        default = "dbbc.ipynb",
        dest = "ifname",
        help = "Filename of the notebook")

    ifname = parser.parse_args().ifname
    ofname =  "{}.pdf".format(ifname.split(".")[0])
    this_file_path = "/".join(__file__.split("/")[:-1]) + "/"

    # Setup config
    c = Config()
    # Configure tag removal - be sure to tag your cells to remove  using the
    # words remove_cell to remove cells. You can also modify the code to use
    # a different tag word
    c.TagRemovePreprocessor.remove_cell_tags = ("remove_cell",)
    c.TagRemovePreprocessor.remove_all_outputs_tags = ('remove_output',)
    c.TagRemovePreprocessor.remove_input_tags = ('remove_input',)
    c.TagRemovePreprocessor.enabled = True

    # Configure and run out exporter
    c.PDFExporter.preprocessors = ["nbconvert.preprocessors.TagRemovePreprocessor"]

    exporter = PDFExporter(config=c)
    exporter.register_preprocessor(TagRemovePreprocessor(config=c),True)

    os.chdir(this_file_path)
    output = PDFExporter(config=c).from_filename(ifname)
    os.chdir("../doc/")

    print("PDF file gets written to " + os.getcwd() + "/" + ofname)
    # Write to output html file
    with open(ofname,  "wb") as f:
        f.write(output[0])
