#pragma once

namespace edd_dbbc {
namespace ddc {
namespace filter {
namespace kernel {

const float ROOT2 = 1.414213562f;

__global__ void firhilbert(
    float* data,
    int length,
    float a,
    float w1,
    float w2)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    float t;
    if(tid < length){
        t = 2 * M_PI * (tid - (length - 1) / 2);
        if(t == 0){
            data[tid] = ROOT2 * (w2 - w1);
        }else if(t == M_PI/2*a){
            data[tid] = a * (sinf(M_PI/4 * ( (a+2*w2) / a)) - sinf( M_PI/4 *( (a+2*w1) / a )));
        }else if(t == -M_PI/2*a){
            data[tid] = a * (sinf(M_PI/4 * ( (a-2*w1) / a)) - sinf( M_PI/4 *( (a-2*w2) / a )));
        }else{
            data[tid] = 2 * M_PI * M_PI * cosf(a * t)
                / (t * (4 * a * a * t * t - M_PI * M_PI))
                * (sinf(w1 * t + M_PI / 4)
                - sinf(w2 * t + M_PI / 4));
        }
    }
}


template<typename T>
__global__ void firwin(
    T* data,
    T* window,
    std::size_t length,
    float f_cutoff)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    if(tid < length){
        data[tid] = f_cutoff * signal::sinc(((T)tid-length/2)*f_cutoff) * window[tid];
    }
}

template<typename T>
__global__ void window_hamming(T* idata, int width)
{
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	if (tidx < width)
	{
		idata[tidx] = 0.54 - 0.46 * cosf(2*tidx*M_PI / (width - 1));
	}
}

template<typename T>
__global__ void window_hann(T* idata, int width)
{
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	if (tidx < width)
	{
		idata[tidx] = 0.5*(1 + cosf(2*tidx*M_PI / (width - 1)));
	}
}

template<typename T>
__global__ void window_bartlett(T* idata, int width)
{
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	if (tidx < width)
	{
		idata[tidx] = 0;
	}
}

template<typename T>
__global__ void window_blackman(T* idata, int width)
{
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	if (tidx < width)
	{
		idata[tidx] = 0.74 / 2 * -0.5 * cosf(2 * M_PI*tidx / (width - 1)) + 0.16 / 2 * sinf(4 * M_PI*tidx / (width - 1));
	}
}


}
}
}
}