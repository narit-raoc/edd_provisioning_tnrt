// FUNCTIONS
function parse_data(series)
{
    result = {}
    series.fields.forEach((object) => {
      result[object["name"]] = object["values"].buffer[0]
    });
    return result;
}

function remove_old_items(name, max_length)
{
    var keys = Object.keys(window.sessionStorage)
        .filter(key => key.includes(name))
        .sort();
    for(let i = 0; i < keys.length - max_length; i++)
    {
        window.sessionStorage.removeItem(keys[i]);
    }
}

function initialize_array(n_spectra, nfft)
{
    var data_array = new Array(n_spectra);
    for(var i = 0; i < n_spectra; i++)
    {
      data_array[i] = new Array(nfft);
    }
    return data_array;
}

function seconds2timestamp(seconds) {
    return new Date(parseInt(seconds)).toISOString();
}

function compute(n_spectra, f_min, f_max, name)
{
    var sorted = Object.keys(window.sessionStorage).sort();
    var encoded = window.sessionStorage.getItem(sorted[0]);
    var uint8Array = new Uint8Array(atob(encoded).split('').map(char => char.charCodeAt(0)));
    var dataView = new DataView(uint8Array.buffer);
    var nfft = dataView.byteLength / 4;
    var bw = f_min - f_max
    var res = bw / nfft;
    var x = new Array(nfft);
    var y = new Array(nfft);
    var z = initialize_array(n_spectra, nfft);
    for (var i = 0; i < nfft; i++) {
        x[i] = f_min + i * res;
        z[0][i] = dataView.getFloat32(i * 4, true); // true specifies little-endian byte order
    }
    var cnt = 0;
    for(var i = 1; i < sorted.length; i++)
    {
        if(sorted[i].includes(name))
        {
            cnt = cnt + 1;
            if(cnt >= n_spectra){
                continue;
            }
            encoded = window.sessionStorage.getItem(sorted[i]);
            uint8Array = new Uint8Array(atob(encoded).split('').map(char => char.charCodeAt(0)));
            dataView = new DataView(uint8Array.buffer);
            //y[i] = seconds2timestamp(sorted[i]);
            y[cnt] = sorted[i].replace(name + "/","").split(".")[0].replace(".");
            for (var ii = 0; ii < nfft; ii++) {
                z[cnt][ii] = dataView.getFloat32(ii * 4, true); // true specifies little-endian byte order
            }
        }
    }
    return {"x":x, "y":y, "z":z};
}
// VARIBALES
var n_spectra = parseInt(document.getElementById("var-n_spectra").value);
var stream_name = data.request.scopedVars.data_streams.value.split(":")[1];
var res_dict = parse_data(data.series[0]);
window.sessionStorage.setItem(
    stream_name + "/" + res_dict["timestamp"], res_dict["data"]
);
remove_old_items(stream_name, n_spectra);
var result = compute(n_spectra, res_dict["f_min"], res_dict["f_max"], stream_name);

var trace = [{
  x: result["x"],
  y: result["y"],
  z: result["z"],
  type: 'heatmap'
}];

return {data:trace};