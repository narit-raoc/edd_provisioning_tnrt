cmake_minimum_required(VERSION 3.10)

project(pfb LANGUAGES CUDA CXX)

include_directories(${CUDA_TOOLKIT_INCLUDE})

#list(APPEND CUDA_NVCC_FLAGS -DENABLE_CUDA --std c++14 -Wno-deprecated-gpu-targets --ptxas-options=-v  --generate-line-info)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


# default installtion locations (note these will be prefixed by ${CMAKE_INSTALL_PREFIX})
if(NOT LIBRARY_INSTALL_DIR)
    set(LIBRARY_INSTALL_DIR "lib")
endif(NOT LIBRARY_INSTALL_DIR)

if(NOT INCLUDE_INSTALL_DIR)
    set(INCLUDE_INSTALL_DIR "include/${CMAKE_PROJECT_NAME}")
endif(NOT INCLUDE_INSTALL_DIR)

if(NOT MODULES_INSTALL_DIR)
    set (MODULES_INSTALL_DIR "share/${CMAKE_PROJECT_NAME}")
endif(NOT MODULES_INSTALL_DIR)

if(NOT BINARY_INSTALL_DIR)
    set(BINARY_INSTALL_DIR "bin/")
endif(NOT BINARY_INSTALL_DIR)

find_package(CUDA)
########################################################################
#PSRDADA
option(${PSRDADA_CPP_DIR} "Location of psrdada directory" "")
include(FindPackageHandleStandardArgs)

find_path(
    PSRDADA_INCLUDE_DIR
    NAMES "multilog.h"
    PATHS "${CMAKE_FRAMEWORK_PATH}/include" 
    "${CMAKE_FRAMEWORK_PATH}/include/psrdada" 
    "/usr/local/include/psrdada"
    ${PSRDADA_INCLUDE_DIR}
)
find_library(PSRDADA_LIBRARY NAMES psrdada)
find_package_handle_standard_args(PSRDADA DEFAULT_MSG PSRDADA_LIBRARY PSRDADA_INCLUDE_DIR)
set(PSRDADA_INCLUDE_DIRS ${PSRDADA_INCLUDE_DIR})
set(PSRDADA_LIBRARIES ${PSRDADA_LIBRARY})


########################################################################
# OpenMP
find_package(OpenMP REQUIRED)
if(OPENMP_FOUND)
    message(STATUS "Found OpenMP" )
    set(DEPENDENCY_LIBRARIES ${DEPENDENCY_LIBRARIES} ${OpenMP_EXE_LINKER_FLAGS})
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

########################################################################
# Googletest

include(ExternalProject)
ExternalProject_Add(
    googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG v1.14.0
    INSTALL_COMMAND ""
    CMAKE_ARGS -DINSTALL_GTEST=OFF
    )
set(GTEST_LIBRARIES libgtest_main.a libgtest.a libgmock_main.a libgmock.a)
ExternalProject_Get_Property(googletest source_dir)
set(GTEST_INCLUDE_DIR ${source_dir}/googletest/include ${source_dir}/googlemock/include)

ExternalProject_Get_Property(googletest binary_dir)
set(GTEST_LIBRARY_DIR ${binary_dir}/${CMAKE_FIND_LIBRARY_PREFIXES})

message(STATUS "GTEST_INCLUDE_DIR: ${GTEST_INCLUDE_DIR}")
message(STATUS "GTEST_LIBRARY_DIR: ${GTEST_LIBRARY_DIR}")


########################################################################
# PSRDADACPP

find_path(
  PSRDADA_CPP_INCLUDE_DIR
    NAMES "psrdada_cpp/dada_client_base.hpp"
    PATHS "${CMAKE_FRAMEWORK_PATH}/include" ${PSRDADA_CPP_DIR} ${PSRDADA_CPP_DIR}/include
)
find_library(PSRDADA_CPP_LIBRARY NAMES psrdada_cpp HINTS ${PSRDADA_CPP_DIR} ${PSRDADA_CPP_DIR}/lib)
find_package_handle_standard_args(PSRDADA DEFAULT_MSG PSRDADA_LIBRARY PSRDADA_INCLUDE_DIR)
set(PSRDADA_CPP_INCLUDE_DIRS ${PSRDADA_CPP_INCLUDE_DIR})
set(PSRDADA_CPP_LIBRARIES
	${PSRDADA_CPP_LIBRARY}
	)

########################################################################
# Boost
include_directories(${PSRDADA_INCLUDE_DIRS} ${PSRDADA_CPP_INCLUDE_DIRS})

find_package(Boost COMPONENTS log program_options REQUIRED)
include_directories(${Boost_INCLUDE_DIR})




message(STATUS " BOOST include directory: "  ${Boost_INCLUDE_DIR})
message(STATUS " PSRDADA include directory: "  ${PSRDADA_INCLUDE_DIRS})
message(STATUS " PSRDADACPP include directory: "  ${PSRDADA_CPP_INCLUDE_DIRS} )
message(STATUS " PSRDADACPP_LIBRARIES : "  ${PSRDADA_CPP_LIBRARIES} )
########################################################################
include_directories(
  ${CMAKE_SOURCE_DIR}/edd_pfb
  ${GTEST_INCLUDE_DIR}
)


add_executable(pfb edd_pfb/main.cu)
target_link_libraries(pfb ${CUDA_CUFFT_LIBRARIES}  ${PSRDADA_CPP_LIBRARIES} ${PSRDADA_LIBRARIES} ${Boost_LIBRARIES} psrdada_cpp_common )
install(TARGETS pfb DESTINATION bin)



link_directories(${GTEST_LIBRARY_DIR})

enable_testing()
add_executable(pfb_test test/test.cu)
add_dependencies(pfb_test googletest)
target_link_libraries(pfb_test gtest gtest_main pthread  ${CUDA_CUFFT_LIBRARIES} ${Boost_LIBRARIES}  ${PSRDADA_CPP_LIBRARIES} ${PSRDADA_LIBRARIES})
add_test(PolyphaseFilterBank pfb_test)


