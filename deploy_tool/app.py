#!/usr/bin/python3
__doc__ = "Deploy the EDD using a clean environment"

import argparse
import os
import sys
import tempfile
import shlex
import time
import shutil
import subprocess
import getpass
import pathlib
import logging
import datetime
import dataclasses
import git
import coloredlogs


_log = logging.getLogger("EDD")

coloredlogs.install(
    fmt=("[ %(levelname)s - %(asctime)s - %(name)s "
         "- %(filename)s:%(lineno)s] %(message)s"),
    level=1,            # We manage the log level via the logger, not the handler
    logger=_log)

env_level = os.getenv("LOG_LEVEL")
if env_level:
    _log.setLevel(env_level.upper())
else:
    _log.setLevel("INFO")

CFG_FILE = 'tnrt_config.yml'


class TempRepository:
    """
    Checkout a repository into a temporary folder for version management with EDD versions.
    """
    def __init__(self, origin):
        self.origin = origin
        self._tmpdir = tempfile.TemporaryDirectory()
        self.repo = None

    def __del__(self):
        self._tmpdir.cleanup()

    def init(self):
        """Initialize repository"""
        _log.debug('Initializing tmp repo')
        self.repo = git.Repo.clone_from(self.origin, self._tmpdir.name)


    def get_next_version(self, dto=None):
        """Return next verson in EDD versioning scheme"""
        _log.debug('getting next version')
        if dto is None:
            _log.debug('No date provided, using today')
            dto = datetime.datetime.now()

        base = dto.strftime("%y%m%d")
        today_tags = [t.name for t in self.repo.tags if t.name.startswith(base)]
        _log.debug('Found %i tags for given date', len(today_tags))

        return f"{base}.{len(today_tags)}"


    def release(self, dto=None):
        """
        Create a new tag in the repository and push it to the origin
        """
        nt = self.get_next_version(dto)
        _log.debug('Creating release %s', nt)
        self.repo.create_tag(nt)
        self.repo.remote().push(tags=True)


@dataclasses.dataclass
class BuildContext:
    """Any informatino needed by the build command"""
    build_directory: str
    env: dict
    inventory: str
    no_act: bool
    limit: str
    config_file: str
    additional_args: str


class CleanEnvironment:
    """Contextmanager for checkouts in a clean build environment"""
    def __init__(self, inventory, config_file, version, no_act, limit, keep_tempdir, role, additional_args):

        self.inventory = inventory
        self.config_file = config_file
        self.version = version
        self.no_act = no_act
        self.keep_tempdir = keep_tempdir
        self.role = role
        self.additional_args = additional_args
        self.starttime = None

        if limit:
            self.limit = f"--limit={limit}"
        else:
            self.limit = ""

        self.cwd = None
        self.build_directory = None


    def __enter__(self):
        self.cwd = os.getcwd()
        # Resolve origin before changing directory
        origin = os.path.abspath(os.path.dirname(self.inventory))

        _log.debug('Creating temp directory')
        self.build_directory = tempfile.mkdtemp()
        os.chdir(self.build_directory)

        _log.debug('Cloning %s -> %s', origin, self.build_directory)
        repo = git.Git()

        repo.clone(origin, self.build_directory)

        if self.version:
            _log.debug('Checking out version %s', self.version)
            try:
                repo.checkout(self.version)
            except git.exc.GitCommandError as E:
                _log.exception(E)
                _log.error("Version '%s' not known to git", self.version)
                sys.exit(-1)

        vault_password = getpass.getpass('Please enter vault password: ')
        self.starttime = time.time()# ONly here to avoid bias by vault password search

        pwdfile = os.path.join(self.build_directory, 'vault_pwd')
        pathlib.Path(pwdfile).touch(mode=0o600)
        with open(pwdfile, 'w', encoding='utf8') as f:
            f.write(vault_password)

        env = {}
        _log.debug('Remove all ansible variables from environemtn')
        for k, v in os.environ.items():
          if k.startswith('ANSIBLE'):
            _log.warning('variable %s=%s set in user environment. This is ignored by the edd_tool!', k, v)
            continue
          env[k] = v

        env['ANSIBLE_COLLECTIONS_PATH'] = self.build_directory
        env['ANSIBLE_NOCOWS'] = "True"

        requirements_file = os.path.join(self.build_directory, os.path.basename(self.inventory), 'requirements.yml')
        print('Install collections:')
        with open(requirements_file) as f:
            print(f.read())

        try:
            subprocess.run(shlex.split(f'ansible-galaxy collection install -r {requirements_file} -p {self.build_directory}'), check=True)
        except subprocess.CalledProcessError as E:
            _log.error('ERROR during installation of collection')
            raise RuntimeError from E


        if self.role:
            _log.debug('Role defined - limit action to single role: %s', self.role)
            self.config_file = os.path.join(self.build_directory, 'build_role.yml')
            with open(self.config_file, 'w') as f:
                f.write(f"""---
- hosts: dev_server[0]
  gather_facts: no
  tasks:
  - name: Print path variable
    debug:
      var: base_path
    tags: always

  roles:
    - {self.role}
    """)

        return BuildContext(build_directory=self.build_directory,
                            env=env, inventory=self.inventory,
                            no_act=self.no_act,
                            limit=self.limit,
                            config_file=self.config_file,
                            additional_args=self.additional_args)


    def __exit__(self, *args, **kwargs):
        _log.debug("Change back to initial working dir")
        os.chdir(self.cwd)
        if self.keep_tempdir:
            _log.info('Temporary directory %s was not deleted as requested', self.build_directory)
        else:
            _log.debug("Cleaning up temporary directory")
            shutil.rmtree(self.build_directory)
        duration = time.time() - self.starttime
        print(f'Duration: {duration:.1f} s')


def get_version_list(remote):
    """Get list of tags without cloning repository"""
    R = subprocess.run(shlex.split(f"git -c 'versionsort.suffix=-' ls-remote --tags --sort='v:refname' {remote}"), capture_output=True, check=True)
    _log.debug('Got reply from remote:\n%s', R.stdout.decode())
    result = [s.split('/')[-1] for s in  R.stdout.decode().split('\n') if s]
    return result


def list_versions(inventory, **kwargs):
    """List all versions in the provision_repository"""
    origin = os.path.abspath(os.path.dirname(inventory))
    R = get_version_list(origin)
#    R.sort()
    print(f"Versions in {origin}:\n")
    print("\n".join(R))


def buildbase(build_context):
    """Execute ansible buildbase command"""
    print('--> Building base in clean environment')
    if build_context.no_act:
        no_act = '-C'
    else:
        no_act = ''
    try:
        subprocess.run(shlex.split(f'ansible-playbook {no_act} -i {build_context.inventory} {build_context.config_file} {build_context.additional_args} --tags=buildbase --vault-password-file=vault_pwd {build_context.limit}'), env=build_context.env, check=True)
    except subprocess.CalledProcessError as E:
        print('!!! Error during buildbase. Deployment unsuccessfull !')
        raise RuntimeError from E


def build(build_context):
    """Execute ansible build command"""
    print('--> Building in clean environment')
    if build_context.no_act:
        no_act = '-C'
    else:
        no_act = ''
    try:
        subprocess.run(shlex.split(f'ansible-playbook {no_act} -i {build_context.inventory} {build_context.config_file} {build_context.additional_args} --tags=build --vault-password-file=vault_pwd {build_context.limit}'), env=build_context.env, check=True)
    except subprocess.CalledProcessError as E:
        print('!!! Error during build. Deployment unsuccessfull !')
        raise RuntimeError from E


def deploy(build_context):
    """Execute ansible deploy command"""
    print('--> Deploying from clean build')
    if build_context.no_act:
        no_act = '-C'
    else:
        no_act = ''
    try:
        subprocess.run(shlex.split(f'ansible-playbook {no_act} -i {build_context.inventory} {build_context.config_file} {build_context.additional_args} --vault-password-file=vault_pwd {build_context.limit}'), env=build_context.env, check=True)
    except subprocess.CalledProcessError as E:
        print('!!! Error during deployment. deployment unsuccessfull !')
        raise RuntimeError from E


def buildall_action(**kwargs):
    """Build base images and images"""
    with CleanEnvironment(**kwargs) as ctx:
        buildbase(ctx)
        build(ctx)


def buildbase_action(**kwargs):
    """Build base images only"""
    with CleanEnvironment(**kwargs) as ctx:
        buildbase(ctx)

def build_action(**kwargs):
    """Build all images except base images"""
    with CleanEnvironment(**kwargs) as ctx:
        build(ctx)

def deploy_action(**kwargs):
    """Deploy system"""
    with CleanEnvironment(**kwargs) as ctx:
        deploy(ctx)







actions = {}
actions['list-versions'] = list_versions
actions['build'] = build_action
actions['buildbase'] = buildbase_action
actions['buildall'] = buildall_action
actions['deploy'] = deploy_action


def action_help():
    """
    Compile help string from all actions
    """
    res = []
    for k, v in actions.items():
        res.append(f"{k:16}   - {v.__doc__}")
    return "\n".join(res)


def main():
    """edd_tool main"""
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-i', '--inventory', help='Inventory to deploy to', required=True)
    parser.add_argument('--version', help='Version to use. Defaults to head of branch', default=None)
    parser.add_argument('-n', '--no-act', help='Do not execute  ansible but add check parameter', action='store_true', default=False)
    parser.add_argument('--limit', help='Limit action to selected host group', metavar='HOSTGROUP')
    parser.add_argument('--keep-tempdir', help='Do not delete temporary build directory', action='store_true')
    parser.add_argument('--role', help='Limit build to a single role, e.g. EDD.core.gated_spectrometer')
    parser.add_argument('--additional_args', help='Further arguments passed to ansible-playbook', default="")
    parser.add_argument('action', help=f'Action to perform. Valid actions are:\n{action_help()}', default=None)

    args = parser.parse_args()

    #with open(os.path.join(args.inventory, 'group_vars', 'all.yml')) as f:
    #    inventory_settings = yaml.load(f, Loader=yaml.SafeLoader)
    actions[args.action](
            version=args.version, config_file=CFG_FILE, inventory=args.inventory, no_act=args.no_act, limit=args.limit, keep_tempdir=args.keep_tempdir, role=args.role, additional_args=args.additional_args)

if __name__ == "__main__":
    main()
