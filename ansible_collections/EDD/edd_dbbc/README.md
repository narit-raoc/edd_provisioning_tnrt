# VLBI for EDD
The EDD (Effelsberg Direct Digitization) is a modular concept for radioastronomy systems, including receivers, time distribution, digitization, packetization and data processing in the backend. 

This repository is a EDD backend plugin used for VLBI operations, but it also has proven in other usecases.

## Requirements
- CUDA 11.0 or greater (and cuBLAS)
- C++ libs - psrdada, psrdada_cpp, boost, Google Testframework
- python3 - numpy, matplotlib, scipy, cupy, cusignal


## Documentation
- Documentation can be found here: <http://mpifr-bdg.pages.mpcdf.de/edd_dbbc/>

## Contact
Author: Niclas Esser - <nesser@mpifr-bonn.mpg.de>
